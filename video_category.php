<?php 
include_once("session_check.php");
include_once("connect.php");
$id=$_REQUEST['id'];
?> 
 

<?php
if($id=='game') 
{
?>  


<div class="col-md-12 col-sm-12 col-xs-12 col-lg-12 event_padding" >
	<form action="" method="post"> 
		<div class="col-md-1 col-sm-12 col-xs-12 col-lg-1 stream_setgame" >
			<div class="form-group video_game">Game
			</div>
		</div>
		<div class="col-md-3 col-sm-12 col-xs-12 col-lg-4 video_game-txt">
			<div class="form-group stream_setgame pad_none" >
				<select name="game" class="form-control input-sm" id="game">
					<?php
							
							$cid=$_SESSION['loginid'];
							$team_id=$_SESSION['team_id'];

							if($team_id==0)
							{
								$gres = $conn->prepare("select * from games_info where home_customer_id='$cid' or visitor_customer_id='$cid'");
							}
							else
							{	$gres = $conn->prepare("select * from games_info where home_team_id='$team_id' or visitor_team_id='$team_id'");
								
							}
							$gres->execute();
						while($grow=$gres->fetch(PDO::FETCH_ASSOC))
						{

						?>
						<option value="<?php echo $grow['id']; ?>"><?php echo $grow['game_name'],"....", $grow['date']; ?></option>
						<?php }
					?>
				</select>
			</div>
		</div>
		<div class="col-md-2 col-sm-2 col-xs-12 col-lg-2" style="padding-top: 8px;">
			<div class="form-group">				 
				  <label for="practice" class="checkbox-inline"><input type="checkbox" class="form-control input-sm" id="practice" name="practice" value="practice"><span style='padding-top: 10px;display: block;'>Practice</span></label>
			</div>
		</div>	
		<div class="col-md-2 col-sm-12 col-xs-12 col-lg-1 stream_setgame">
			<div class="form-group">
				<input type="submit" value="Save" class="btn searchtogglebtn" style="width:85px;">
			</div>
		</div>
	</form>
</div>
<div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 video_border">
    <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 videospecification">
	  <p >805stats Streaming Specifications/Requirements</p>
    </div>
    <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12">
       <div class="col-md-6 col-sm-12 col-lg-6 col-xs-12">
        <p class="eve_videotxt">Supported Video Bitrate Range : <b>1000 to 3000 kbps</b></p>
       <p class="eve_videotxt">Video Codec : <b>H264 Only</b></p>
       </div>
       <div class="col-md-6 col-sm-12 col-lg-6 col-xs-12">
       <p class="eve_videotxt">Supported Resolutions: <b>1280x720 and 1920x1080 </b></p>
       <p class="eve_videotxt">Audio Codec : <b>AAC Only </b></p>
       </div>
    </div>
	
</div>
<?php
}
else
{
?>
	<form action="" method="post" id="eventform">
	    <div class="row center">
			<input type="hidden" name="category" value="<?php echo $id; ?>"/>
			<div class="col-md-12 col-sm-12 col-xs-12 event_game_txt event_padding">
				<div class="col-md-3 col-sm-3 col-xs-12 col-lg-3">
					<div class="form-group">
						<label for="gamename">Name of <?php echo $id; ?>
						<span class="required"> * </span>
						</label>
						<input type="text" class="form-control input-sm " id="ename" name="ename" placeholder="Name of <?php echo $id;?>" value=""> 
					</div>
				</div>
				<div class="col-md-2 col-sm-2 col-xs-12 col-lg-2">
					<div class="form-group">
						<label for="gamename">Date
						<span class="required"> * </span>
						</label>
						<input type="text" class="form-control input-sm" id="date" name="date" placeholder="Date" value="" > 
					</div>
				</div>					
				<div class="col-md-3 col-sm-2 col-xs-12 col-lg-2 eventtime">
					<div class="form-group">
						<label for="gamename">Time
						<span class="required"> * </span>
						</label>
						<input type="text" class="form-control input-sm" id="time" name="time" placeholder="Time" value="" > 
					</div>
				</div>	

				<div class="col-md-2 col-sm-2 col-xs-12 col-lg-2">
					<div class="form-group">
						 <p></p>
						  <label for="practice" class="checkbox-inline"><input type="checkbox" class="form-control input-sm" id="practice" name="practice" value="practice"><span style='padding-top: 10px;display: block;'>Practice</span></label>
					</div>
				</div>	
			</div>	
			<div class="col-md-12 col-sm-12 col-xs-12 event_game_txt event_padding">
				<div class="col-md-7 col-sm-10 col-xs-12 col-lg-7 eventdesc">
					<div class="form-group">
						<label for="gamename">Description
						<span class="required"> * </span>
						</label>
						<textarea name="descp" placeholder="Description" id="descp" class="form-control" rows="3" ></textarea> 
					</div>
				</div>
				<div class="col-md-1 col-sm-1 col-xs-12 col-lg-1 eventsavebtn">
					<div class="form-group">
						<input type="submit" value="Save" class="btn btn-danger searchtogglebtn_red" style="width:85px;">
					</div>
				</div>
			</div>
			
		</div>
	</form>
	<div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 video_border">
	    <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 videospecification">
		  <p>805stats Streaming Specifications/Requirements</p>
	    </div>
	    <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12">
	       <div class="col-md-6 col-sm-12 col-lg-6 col-xs-12">
	        <p class="eve_videotxt">Supported Video Bitrate Range : <b>1000 to 3000 kbps</b></p>
	       <p class="eve_videotxt">Video Codec : <b>H264 Only</b></p>
	       </div>
	       <div class="col-md-6 col-sm-12 col-lg-6 col-xs-12">
	       <p class="eve_videotxt">Supported Resolutions: <b>1280x720 and 1920x1080 </b></p>
	       <p class="eve_videotxt">Audio Codec : <b>AAC Only </b></p>
	       </div>
	    </div>
		
	</div>
<?php
}
?>
