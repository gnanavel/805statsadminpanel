<?php
include_once('session_check.php');
include_once('connect.php'); 


if(isset($_POST['teamid'])){
	
	$SeasonId      = $_POST['seasonid'];
	$DivisionId    = $_POST['divisionid'];
	$ConferenceId  = $_POST['conferenceid'];
	$TeamId		   = $_POST['teamid'];
	$PlayersId	   = array_filter($_POST['PlayersId']);
	$createdate	   = date('Y-m-d H:i:s');
	
	
	$Inc=1;
	$PlayerData = "";
	

	foreach($PlayersId as $PlayerId) {

		$QryExePlayer = $conn->prepare("select  player_id, status, player_image,isdelete from customer_team_player where division_id='$DivisionId' and conference_id='$ConferenceId' and customer_id='$MasterCustId' and season_id='$SeasonId' and team_id='$TeamId' and player_id=$PlayerId and isdelete=0");
		$QryExePlayer->execute();
		$QryCntPlayer = $QryExePlayer->rowCount();
		$PlayerData[$PlayerId]["player_image"]='';
		$PlayerData[$PlayerId]["status"] = (int) 1;
		if ($QryCntPlayer > 0) {
			$FetchPlayerData = $QryExePlayer->fetch(PDO::FETCH_ASSOC);
			$PlayerData[$FetchPlayerData["player_id"]] = $FetchPlayerData;
		} 
		
	}
	//print_r($PlayerData);exit;
	$delcustqry = $conn->prepare("delete from customer_team_player where division_id=:division_id and conference_id=:conference_id and customer_id=:customer_id and season_id=:season_id and team_id=:team_id");
	$QryArrCond   = array(':customer_id' => $MasterCustId, ':season_id' => $SeasonId, ':conference_id' => $ConferenceId, ':division_id' => $DivisionId,":team_id"=>$TeamId);
	$delcustqry->execute($QryArrCond);

	foreach($PlayersId as $PlayerId) {		

		$stmt   = $conn->prepare("INSERT INTO customer_team_player (customer_id, season_id, conference_id,division_id,team_id,player_id,player_order,status,player_image,created_date,modified_date) VALUES (:customer_id, :season_id,:conference_id,:division_id,:team_id,:player_id,:player_order,:status,:player_image,:created_date,:modified_date)");

		$stmt->execute(array(':customer_id' => $MasterCustId, ':season_id' => $SeasonId, ':conference_id' => $ConferenceId, ':division_id' => $DivisionId, ':team_id' =>$TeamId,':player_id' => $PlayerId, ':player_order' => $Inc, ':status' => $PlayerData[$PlayerId]["status"],':player_image' => $PlayerData[$PlayerId]["player_image"], ':created_date' => $createdate,':modified_date' => $createdate));	  

		$Inc++;
	}
	
	$QryExeDiv = $conn->prepare("select * from customer_team_player as seasonplayer LEFT JOIN player_info as playertbl ON  seasonplayer.player_id=playertbl.id where seasonplayer.conference_id=:conference_id and season_id=:season_id and division_id=:division_id and seasonplayer.team_id=:team_id and seasonplayer.isdelete=0 and playertbl.lastname!='TEAM' and seasonplayer.customer_id=:customer_id");
	$QryarrCon = array(":conference_id"=>$ConferenceId,":season_id"=>$SeasonId,":division_id"=>$DivisionId,":team_id"=>$TeamId ,':customer_id' => $MasterCustId);
	$QryExeDiv->execute($QryarrCon);
	$QryCntSeasonconf = $QryExeDiv->rowCount();
	$responseHtml = '';
	$AssignPlayerArr  = array();

	$responseHtml .= "<table class='table assignplayertbl'>";
	if($QryCntSeasonconf>0){
			
		while ($rowPlayer = $QryExeDiv->fetch(PDO::FETCH_ASSOC)){			
				$AssignPlayerArr  = $rowPlayer['id'];

				$InActiveBg='';
				$Status=$rowPlayer['status'];
				if($Status==0){							
					$InActiveBg = 'inactiveplayerbg';
				}
				$src =HTTP_ROOT.'/uploads/players/thumb/'.$rowPlayer['player_image'];

				$PlayerImg='';
				if (@getimagesize($src)) {
					$PlayerImg   = "<img src='$src' class='playerimageinlist'>";
				}else{
					$PlayerImg   = "<img src='images/defaultplayer.png' class='playerimageinlist'>";
				}
				$responseHtml .= "<tr id='player_".$rowPlayer['id']."' class='".$InActiveBg."'><td class='playerimgtbl'>".$PlayerImg."</td><td class='playernamevalgin'>".$rowPlayer['firstname']." ".$rowPlayer['lastname']."</td><td><p class='playeractionwrap'><span date-playerid='".$rowPlayer['id']."' teamid='".$TeamId."' seasionid='".$SeasonId."' class='removeplayerteam'>Remove</span><span data-playerid='".$rowPlayer['id']."' teamid='".$TeamId."' seasionid='".$SeasonId."'  divisionid='".$DivisionId."' conferenceid  = '".$ConferenceId."' selstatus='".$Status."' class='updateplayer updatestatus_".$rowPlayer['id']."'>Update</span><span data-playerid='".$rowPlayer['id']."' class='switchteam'  teamid='".$TeamId."'>Switch Team</span></p></td></tr>";			
		}		

	}else{
		$responseHtml .= "<tr><td>No players for this team</td></tr>";
	}
	$responseHtml .= "</table>";
	echo $responseHtml;
	exit;
}
?>