<?php
include_once('session_check.php'); 
include_once("connect.php");

if (!empty($_SESSION['loginid']))  {

    if ($_SESSION['usertype'] == 'admin') {
        $uid = $_SESSION['loginid']; 
    } else { 
        header('Location:login.php');
        exit;
    }
} else {
    header('Location:login.php');
    exit;
}

if (isset($_REQUEST['submit'])) {

    $n = date("ymdihs");
    $sourcePath = $_FILES['upload_file']['tmp_name']; // Storing source path of the file in a variable
    $targetPath = preg_replace('/\s+/', '',"uploader_files/".$n.$_FILES['upload_file']['name']); // Target path where file is to be stored

    move_uploaded_file($sourcePath, $targetPath) ; // Moving Uploaded file
    $to = $_POST['mail_send'];
    $headers  = 'MIME-Version: 1.0' . "\r\n";
    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
    $headers .= 'From: support@805stats.com';
    $message = "<a href='http://805stats.com/panel/'>".$targetPath."</a>";
    $message = "<div style='width:570px; text-align:center; padding:15px; height:400px; border:5px solid black; background-image: url(http://805stats.com/images/gr.jpg);background-size: 100% 100%;'><img src='http://805stats.com/images/logo.png' width='122'/><div style='width:480px;margin:auto; margin-top:20px;'><h3 style='text-align:center;color:#333333; margin-top:20px; font-weight:bold;'>Welcome to 805 STATS</h3><p style='text-align:left;'>Thank  you for choosing 805 STATS for your Live Scoring! Click the link provided and unzip the folder on your desktop. Read the PDF files for detailed instructions on how to install and operate the 805stats upload tool. </p><p><a href='http://805stats.com/panel/".$targetPath."'>Click Here to Download</a>
    </p>
    <p style='margin-top:60px;text-align:left;'>Please visit 805STATS.com for more information, Questions? Our FAQ section should answer any questions you may have. </p></div><p style='margin-top:200px;'>&copy; 805STATS.com  All Rights Reserved</p></div>";

    $result = mail($to, "Upload tool installation files", $message, $headers);
    if(!$result) {   
          $message= "Email not sent"; 
          $AlertSts = 'alert-danger';  
    } else {
        $message= "Email sent successfully";
        $AlertSts = 'alert-success';
    }
}
include_once('header.php');
?>
<link href="assets/custom/css/uploadertool.css" rel="stylesheet" type="text/css" />
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <?php if(!empty($message)){ ?>
            <div class="alert alert-block fade in <?php echo $AlertSts; ?>">
                <button type="button" class="close" data-dismiss="alert"></button>
                <p> <?php echo $message; ?> </p>
            </div>
            <?php } ?> 
             
            <div class="row uploader_searchheder">
                <div class="portlet-body">
                    <div class="widget-header uploaderlink"> 
                        <h3>
                            <i class="icon-settings font-red-sunglo"></i>
                            Uploader Tool Link                  
                        </h3>
                    </div>
                </div>       
                <form method="POST" id="frmmailsend" name="frmmailsend" enctype="multipart/form-data" class="form-inline">
                    <div class="col-md-12 col-sm-12 col-xs-12 uploader_searchbarstyle" style="border-top-right-radius: 0 !important;border-top-left-radius: 0 !important;">
                        <div class="col-md-12 col-sm-12 col-xs-12 forminput">
                            <div class="col-md-2 col-sm-2 col-xs-3">
                                <label class="uploadtoollbl">Email Address:<span class="error">*</span></label>
                            </div>
                            <div class="col-md-10 col-sm-10 col-xs-9">
                                <input type="text" name="mail_send" id="mail_send" class="form-control border-radius" placeholder="Email Address">
                            </div>
                        </div> 
                        <div class="col-md-12 col-sm-12 col-xs-12 forminput">
                            <div class="col-md-2 col-sm-2 col-xs-3">
                                <label for="upload_file">Files:<span class="error">*</span></label>
                            </div>
                            <div class="col-md-10 col-sm-10 col-xs-9">
                                 <input type="file" name="upload_file" id="upload_file" class="form-control filestyle border-radius" data-buttonBefore="true">
                            </div>
                        </div> 
                        <div class="col-md-12 col-sm-12 col-xs-12 send_btn forminput">
                            <div class="col-md-2 col-sm-2 col-xs-3">
                            </div>
                            <div class="col-md-10 col-sm-10 col-xs-9">
                                <button type="submit" name="submit" class="btn btnaddsaveupdategreen addsendform" style="">Send</button>
                            </div>
                            
                        </div>     
                    </div>                
                </form>  
                <!-- <form method="POST" id="frmmailsend" name="frmmailsend" enctype="multipart/form-data" class="form-inline">
                    <div class="col-md-12 col-sm-12 col-xs-12 uploader_searchbarstyle">
                        <div class="col-md-5 col-sm-12 col-xs-12">
                            <div class="col-md-5 col-sm-5 col-xs-5">
                                <label class="uploadtoollbl">Email Address:<span class="error">*</span></label>
                            </div>
                            <div class="col-md-7 col-sm-7 col-xs-7">
                                <input type="text" name="mail_send" id="mail_send" class="form-control border-radius" placeholder="Email">
                            </div>
                        </div> 
                        <div class="col-md-5 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label for="upload_file">Files:</label>
                                <input type="file" name="upload_file" id="upload_file" class="form-control filestyle border-radius" data-buttonName="btn-primary choosefile">
                            </div>

                        </div> 
                        <div class="col-md-2 col-sm-12 col-xs-12 send_btn">
                            <button type="submit" name="submit" class="btn btnaddsaveupdategreen addsendform" style="">Send</button>
                        </div>     
                    </div>                
                </form> -->
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>
<?php include_once('footer.php'); ?>
<script src="assets/custom/js/uploadertool.js" type="text/javascript" ></script>
<script src="assets/custom/js/bootstrap-filestyle.min.js" type="text/javascript" ></script>

