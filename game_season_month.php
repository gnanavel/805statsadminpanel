<?php 
include_once('session_check.php'); 
include_once("connect.php");
include_once('common_functions.php');
include_once('usertype_check.php');

if (isset($_POST["season"])) {

    $season = $_POST["season"];
    $team_id = '';
    if ($_SESSION['signin'] == 'team_manager') {
        $teamlogin_id = $_SESSION['team_manager_id'];
        $team_id = " AND (visitor_team_id=$teamlogin_id OR home_team_id=$teamlogin_id)";
    }

    if ($_SESSION['master'] == 1) {
        $children = $_SESSION['loginid'].",".$_SESSION['childrens'];
        $cid = $children;
    } else {
        $cid = $customerid;        
    }

    $DefaultSeasonDivCondn = "";
    if (empty($season)) {
        $DefaultSeasonDivQry = $conn->prepare("select distinct(season) from games_info where home_customer_id in ($cid) order by season asc limit 0, 1");
        $DefaultSeasonDivQry->execute();
        $FetchDefaultSeasonDiv = $DefaultSeasonDivQry->fetch(PDO::FETCH_ASSOC);
        $DefaultSeasonDiv =  $FetchDefaultSeasonDiv['season'];
        
        if (!empty($DefaultSeasonDiv)) {
            if ($_SESSION['signin'] != 'team_manager') {
                $DefaultSeasonDivCondn = " and season='$DefaultSeasonDiv'";
            }
        } 
    } else {
        $DefaultSeasonDivCondn = " and season='$season'";
    }
    // echo "select DISTINCT date_format(STR_TO_DATE(date, '%m/%d/%Y'),'%M, %Y') as seasonnmnthyr from games_info where (home_customer_id IN ($cid) or visitor_customer_id IN ($cid)) $team_id $DefaultSeasonDivCondn  ORDER BY STR_TO_DATE(date, '%m/%d/%Y'),time";
    $res = $conn->prepare("select DISTINCT date_format(STR_TO_DATE(date, '%m/%d/%Y'),'%M, %Y') as seasonnmnthyr from games_info where (home_customer_id IN ($cid) or visitor_customer_id IN ($cid)) $team_id $DefaultSeasonDivCondn  ORDER BY STR_TO_DATE(date, '%m/%d/%Y'),time");
    $res->execute();
    $cntRows = $res->rowCount();
    $returnArray = "";
    if ($cntRows > 0) {
        $fetchRows = $res->fetchAll(PDO::FETCH_ASSOC);
        foreach ($fetchRows as $fetchRes) {
            if ($fetchRes["seasonnmnthyr"] != NULL)
                $returnArray[] = $fetchRes["seasonnmnthyr"];
        }
    }
    echo json_encode($returnArray);
   

}



