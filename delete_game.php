<?php
include_once('session_check.php'); 
include_once("connect.php");
include_once('usertype_check.php');

if (isset($_GET['gid']) && !empty($_SESSION['sportid'])) {

	if ($_GET['action'] == "delete") {
		
		$gameid = base64_decode($_GET['gid']);
		$sport_id = $_SESSION['sportid'];
		
		if (!empty($gameid)) {
			$DeleteQry = $conn->prepare("DELETE FROM games_info WHERE id=:gameid AND sport_id=:sport_id");
			$DeleteQryArr = array(":gameid"=>$gameid, ":sport_id"=>$sport_id);
			$DeleteStatus = $DeleteQry->execute($DeleteQryArr);

			if ($DeleteStatus) {
		    	header('Location:game_list.php?msg=4');
			    exit;	
			}
		}	
	}
} 
?>