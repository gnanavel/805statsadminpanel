<?php
include_once('session_check.php');
include_once('connect.php');

if (isset ($_REQUEST['post_type']) && $_REQUEST['post_type'] == "checkpalyer") {

	$OldPlayerid = $_REQUEST["playerid"];
	$NewPlayerid = $_REQUEST["duplicateplayerid"];
	$Season = $_REQUEST["season"];

	//Commented on Nov 04, 2016
	// $CheckPlayer = $conn->prepare("SELECT * FROM player_stats_bb WHERE playercode in (:OldPlayerid ,:NewPlayerid) AND season=:Season GROUP BY playercode");
	// $CheckPlayerArr = array(":OldPlayerid"=>$OldPlayerid, ":NewPlayerid"=>$NewPlayerid, ":Season"=>$Season);

	//Commented on Nov 10, 2016
	// $CheckPlayer = $conn->prepare("SELECT * FROM player_stats_bb WHERE playercode in (:OldPlayerid ,:NewPlayerid) GROUP BY playercode");
	// $CheckPlayerArr = array(":OldPlayerid"=>$OldPlayerid, ":NewPlayerid"=>$NewPlayerid);
	// $CheckPlayer->execute($CheckPlayerArr);
	// $CntPlayers = $CheckPlayer->rowCount();

	//Added on Nov 10, 2016
	$CheckPlayer = $conn->prepare("SELECT * FROM player_info WHERE id in (:OldPlayerid ,:NewPlayerid) GROUP BY id");
	$CheckPlayerArr = array(":OldPlayerid"=>$OldPlayerid, ":NewPlayerid"=>$NewPlayerid);
	$CheckPlayer->execute($CheckPlayerArr);
	$CntPlayers = $CheckPlayer->rowCount();
	
	if ($CntPlayers > 1) {
		$Status = "exists";
	} else {
		$Status = "not-exists";
	}		
    echo $Status;
    exit;
}

if (isset ($_REQUEST['post_type']) &&  $_REQUEST['post_type'] == "combineplayer") {

	$OldPlayerid = $_REQUEST["playerid"];
	$NewPlayerid = $_REQUEST["duplicateplayerid"];
	$Season = $_REQUEST["season"];
	$customer_id = $_SESSION["loginid"];

	//Get gameID for the new player
	$NewPlayerQry = $conn->prepare("SELECT gamecode, checkname FROM individual_player_stats WHERE playercode=:NewPlayerid AND season=:Season");
	$NewPlayerQryArr = array(":NewPlayerid"=>$NewPlayerid, ":Season"=>$Season);
	$NewPlayerQry->execute($NewPlayerQryArr);
	$CntNewPlayersGameID = $NewPlayerQry->rowCount();

	$returnStatus = "";
	if ($CntNewPlayersGameID > 0) {

		$FetchGameCode = $NewPlayerQry->fetchAll(PDO::FETCH_ASSOC);
		$Arrdupgamecode = "";
		foreach ($FetchGameCode as $FetchRows) {
			$Arrdupgamecode[] = $FetchRows["gamecode"];			
		}
		$GameCode = implode(",", $Arrdupgamecode);

		$IndStatsQry = $conn->prepare("SELECT * FROM individual_player_stats WHERE playercode=:OldPlayerid AND gamecode IN (:GameCode) AND season=:Season");
		$IndStatsQryArr = array(":OldPlayerid"=>$OldPlayerid, ":GameCode"=>$GameCode, ":Season"=>$Season);
		$IndStatsQry->execute($IndStatsQryArr);
		$CntIndStats = $IndStatsQry->rowCount();

		if ($CntIndStats > 0) {
			//Delete actual playerID from individual_player_stats
			$deleteActPlayerinfo = $conn->prepare("delete from individual_player_stats where playercode=:OldPlayerid and gamecode in (:GameCode) and season=:Season");
	        $deleteActPlayerinfoArr = array(":OldPlayerid"=>$OldPlayerid, ":GameCode"=>$GameCode, ":Season"=>$Season);
	       	$deleteActPlayerinfo->execute($deleteActPlayerinfoArr);	
	    }

		//Get checkname
		$CheckNameQry = $conn->prepare("SELECT checkname FROM individual_player_stats WHERE playercode=:OldPlayerid AND season=:Season");
		$CheckNameQryArr = array(":OldPlayerid"=>$OldPlayerid, ":Season"=>$Season);
		$CheckNameQry->execute($CheckNameQryArr);
		$CntPlayerId = $CheckNameQry->rowCount();
		if ($CntPlayerId > 0) {
			$FetchCheckName = $CheckNameQry->fetch(PDO::FETCH_ASSOC);
			$Checkname = strtoupper($FetchCheckName["checkname"]);
		} else {
			$PlayerQry = $conn->prepare("SELECT firstname, lastname FROM player_info WHERE id=:OldPlayerid");
			$PlayerQryArr = array(":OldPlayerid"=>$OldPlayerid);
			$PlayerQry->execute($PlayerQryArr);
			$FetchPlayerInfo = $PlayerQry->fetch(PDO::FETCH_ASSOC);
			$Checkname = strtoupper($FetchPlayerInfo["lastname"].",".$FetchPlayerInfo["firstname"]);
		}

       	//Update duplicate playerID into actual playerID
		$updatePlayer = $conn->prepare("UPDATE individual_player_stats set playercode=:OldPlayerid, checkname=:checkname WHERE playercode=:NewPlayerid AND season=:Season");
		$updatePlayerArr = array(":NewPlayerid"=>$NewPlayerid, ":OldPlayerid"=>$OldPlayerid, ":checkname"=>$Checkname, ":Season"=>$Season);
		$updatePlayer->execute($updatePlayerArr); 

		$sumPlayerstats = $conn->prepare("SELECT *, SUM(gp) as gp,SUM(gs) as gs,SUM(fgm) as fgm,SUM(fga) as fga,SUM(fgm3) as fgm3,SUM(fga3) as fga3, SUM(ftm) as ftm,SUM(fta) as fta,SUM(tp) as tp,SUM(oreb) as oreb,SUM(dreb) as dreb,SUM(treb) as treb, 
        SUM(pf) as pf,SUM(tf) as tf,SUM(ast) as ast,SUM(to1) as to1, SUM(blk) as blk,SUM(stl) as stl,SUM(min) as min
        FROM individual_player_stats WHERE playercode = :OldPlayerid  and season=:Season GROUP BY teamcode");	
        $sumPlayerstatsArr = array(":OldPlayerid"=>$OldPlayerid, ":Season"=>$Season);
        $sumPlayerstats->execute($sumPlayerstatsArr);
        $cntPlayerStatsRows = $sumPlayerstats->rowCount();

        if ($cntPlayerStatsRows > 0) {
        	$fetchrows = $sumPlayerstats->fetchAll(PDO::FETCH_ASSOC);
        	foreach ($fetchrows as $fetchdata) {
        		$teamcode = $fetchdata["teamcode"];
        		$playercheckname = $fetchdata["checkname"];
        		$year = $fetchdata["year"];
        		$gamecode = $fetchdata["gamecode"];    	
	            $gp = $fetchdata["gp"];
	            $gs = $fetchdata["gs"];
	            $fgm = $fetchdata["fgm"];
	            $fga = $fetchdata["fga"];
	            $fgm3 = $fetchdata["fgm3"];
	            $fga3 = $fetchdata["fga3"];
	            $ftm = $fetchdata["ftm"];
	            $fta = $fetchdata["fta"];
	            $tp = $fetchdata["tp"];
	            $oreb = $fetchdata["oreb"];
	            $dreb = $fetchdata["dreb"];
	            $treb = $fetchdata["treb"];
	            $pf = $fetchdata["pf"];
	            $tf = $fetchdata["tf"];
	            $ast = $fetchdata["ast"];
	            $to1 = $fetchdata["to1"];
	            $blk = $fetchdata["blk"];
	            $stl = $fetchdata["stl"];
	            $min = $fetchdata["min"];

	            $playerStatsbb = $conn->prepare("SELECT * FROM player_stats_bb WHERE playercode = :OldPlayerid AND season=:Season AND teamcode=:teamcode ");
	            $playerStatsbbArr = array(":OldPlayerid"=>$OldPlayerid, ":Season"=>$Season, ":teamcode"=>$teamcode);
	            $playerStatsbb->execute($playerStatsbbArr);
	        	$cntPlayerStatsbbRows = $playerStatsbb->rowCount();
	            if ($cntPlayerStatsbbRows > 0) {
	            
	                //Update player_stats_bb
	            	$updatePlayerStatsbb = $conn->prepare("update player_stats_bb set checkname=:checkname, gamecode=:gamecode,gp=:gp, gs=:gs, fgm=:fgm, fga=:fga, fgm3=:fgm3, fga3=:fga3, ftm=:ftm, fta=:fta, tp=:tp, oreb=:oreb, dreb=:dreb, treb=:treb, pf=:pf, tf=:tf, ast=:ast, to1=:to1, blk=:blk, stl=:stl, min=:min where playercode=:OldPlayerid and teamcode=:teamcode and season=:Season and gamecode=:gamecode");
	            	$updatePlayerStatsbbArr = array(":checkname"=>$playercheckname, ":gamecode"=>$gamecode, ":gp"=>$gp, ":gs"=>$gs, ":fgm"=>$fgm, ":fga"=>$fga, ":fgm3"=>$fgm3, ":fga3"=>$fga3, ":ftm"=>$ftm, ":fta"=>$fta, ":tp"=>$tp, ":oreb"=>$oreb, ":dreb"=>$dreb, ":treb"=>$treb, ":pf"=>$pf, ":tf"=>$tf, ":ast"=>$ast, ":to1"=>$to1, ":blk"=>$blk, ":stl"=>$stl, ":min"=>$min, ":Season"=>$Season, ":teamcode"=>$teamcode, ":OldPlayerid"=>$OldPlayerid);
	                $updatePlayerStatsbb->execute($updatePlayerStatsbbArr); 

	            } else {
	            	
	            	//Insert player_stats_bb
	                $insertPlayerStatsbb = $conn->prepare("insert into player_stats_bb (checkname, gamecode, customer_id,gp,gs,fgm,fga,fgm3,fga3,ftm,fta,tp,oreb,dreb,treb,pf,tf,ast,to1,blk,stl,min,playercode,year,season,teamcode) 
	                values (:checkname, :gamecode, :customer_id, :gp, :gs, :fgm, :fga, :fgm3, :fga3, :ftm, :fta, :tp, :oreb, :dreb, :treb, :pf, :tf, :ast, :to1, :blk, :stl, :min, :OldPlayerid, :year, :Season, :teamcode)");
	                $insertPlayerStatsbbArr = array(":gp"=>$gp, ":gs"=>$gs, ":fgm"=>$fgm, ":fga"=>$fga, ":fgm3"=>$fgm3, ":fga3"=>$fga3, ":ftm"=>$ftm, ":fta"=>$fta, ":tp"=>$tp, ":oreb"=>$oreb, ":dreb"=>$dreb, ":treb"=>$treb, ":pf"=>$pf, ":tf"=>$tf, ":ast"=>$ast, ":to1"=>$to1, ":blk"=>$blk, ":stl"=>$stl, ":min"=>$min, ":Season"=>$Season, ":teamcode"=>$teamcode, ":year"=>$year, ":OldPlayerid"=>$OldPlayerid, ":customer_id"=>$customer_id, ":checkname"=>$playercheckname, ":gamecode"=>$gamecode);
	                $test = $insertPlayerStatsbb->execute($insertPlayerStatsbbArr);
	            } 
            }

            // Delete duplicate player from player_stats_bb
            $deletePlayerinfo = $conn->prepare("DELETE FROM player_stats_bb WHERE playercode=:NewPlayerid AND season=:Season");
            $deletePlayerinfoArr = array(":NewPlayerid"=>$NewPlayerid, ":Season"=>$Season);
        	$deletePlayerinfo->execute($deletePlayerinfoArr);			        	

            // Delete duplicate player from player_info
            $deletePlayerinfo = $conn->prepare("DELETE FROM player_info WHERE id=:NewPlayerid");
            $deletePlayerinfoArr = array(":NewPlayerid"=>$NewPlayerid);
        	$deletePlayerinfo->execute($deletePlayerinfoArr);

        	$returnStatus["status"] = "Player combined and duplicate player deleted successfully";
        	$returnStatus["responsestatus"] = "success";

        }
	} else {
		$returnStatus["status"] = "No duplicate player found for the season";
		$returnStatus["responsestatus"] = "failure";
	}
	echo json_encode($returnStatus);
	exit;
}

