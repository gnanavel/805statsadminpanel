<?php   
session_start(); //to ensure you are using same session

if ($_SESSION['usertype'] == "team_manager") {

	session_destroy(); //destroy the session
	header("Location:teamlogin.php"); //to redirect back to "index.php" after logging out
	exit();

} else {
	session_destroy(); //destroy the session
	header("Location:login.php"); //to redirect back to "index.php" after logging out
	exit();
}

?>