<?php
include_once('session_check.php'); 
include_once("connect.php");
include_once('usertype_check.php');

if ( isset($_GET['gid']) && $_GET['action'] == "delete" )  {

	$gameid = base64_decode($_GET['gid']);
	$redirectgameid = $_GET['gid'];
	if (!empty($gameid)) {
		
	    //Get gamecode from game_details table
	    $GamecodeQry = $conn->prepare("SELECT id, season, home_team_code, visitor_team_code FROM game_details WHERE xml_game_id=:xml_game_id");
	    $GamecodeQryArr = array(":xml_game_id"=>$gameid);
	    $GamecodeQry->execute($GamecodeQryArr);
	    $CntGamecode = $GamecodeQry->rowCount();

	    $Gamecode = "";
	    $GameSeason = "";
	    $Hometeamcode = "";
	    $Visitingteamcode = "";

	    if ($CntGamecode > 0) {
	    	$FetchGameDetails = $GamecodeQry->fetch(PDO::FETCH_ASSOC);
	    	$Gamecode = $FetchGameDetails["id"];
	    	$GameSeason = $FetchGameDetails["season"];
	    	$Hometeamcode = $FetchGameDetails["home_team_code"];
	    	$Visitingteamcode = $FetchGameDetails["visitor_team_code"];

	    	//Get playerstats from individual_player_stats for the particular gamecode
	    	$IndPlayerstatsQry = $conn->prepare("SELECT * FROM individual_player_stats WHERE gamecode=:gamecode AND season=:season");
		    $IndPlayerstatsQryArr = array(":gamecode"=>$Gamecode, ":season"=>$GameSeason);
		    $IndPlayerstatsQry->execute($IndPlayerstatsQryArr);
		    $CntIndPlayerstats = $IndPlayerstatsQry->rowCount();
		    if ($CntIndPlayerstats > 0) {
		    	$FetchPlayerstats = $IndPlayerstatsQry->fetchAll(PDO::FETCH_ASSOC);

		    	foreach ($FetchPlayerstats as $PlayerstatsRows) {
		    		$playercode = $PlayerstatsRows["playercode"];
		    		$teamcode = $PlayerstatsRows["teamcode"];
		    		$gp = $PlayerstatsRows["gp"];
		    		$dbl_dbl = $PlayerstatsRows["dbl_dbl"];
		    		$trip_dbl = $PlayerstatsRows["trip_dbl"];
		    		$fgm = $PlayerstatsRows["fgm"];
		    		$fga = $PlayerstatsRows["fga"];
		    		$gs = $PlayerstatsRows["gs"];
		    		$fgm3 = $PlayerstatsRows["fgm3"];
		    		$fga3 = $PlayerstatsRows["fga3"];
		    		$ftm = $PlayerstatsRows["ftm"];
		    		$fta = $PlayerstatsRows["fta"];
		    		$tp = $PlayerstatsRows["tp"];
		    		$blk = $PlayerstatsRows["blk"];
		    		$stl = $PlayerstatsRows["stl"];
		    		$ast = $PlayerstatsRows["ast"];
		    		$min = $PlayerstatsRows["min"];
		    		$oreb = $PlayerstatsRows["oreb"];
		    		$dreb = $PlayerstatsRows["dreb"];
		    		$treb = $PlayerstatsRows["treb"];
		    		$pf = $PlayerstatsRows["pf"];
		    		$tf = $PlayerstatsRows["tf"];
		    		$to1 = $PlayerstatsRows["to1"];		    		

		    		//Check whether players available or not in player_stats_bb
		    		$PlayerstatsQry = $conn->prepare("SELECT playercode FROM player_stats_bb WHERE playercode=:playercode AND season=:season AND teamcode=:teamcode");
				    $PlayerstatsQryArr = array(":playercode"=>$playercode, ":season"=>$GameSeason, ":teamcode"=>$teamcode);
				    $PlayerstatsQry->execute($PlayerstatsQryArr);
				    $CntPlayerstats = $PlayerstatsQry->rowCount();
				    if ($CntPlayerstats > 0) {

				    	// $FetchPlayerstats = $IndPlayerstatsQry->fetch(PDO::FETCH_ASSOC);
				    	//Minus playerstats for the particular gamecode
				    	$PlayerstatsUpdateQry = $conn->prepare("UPDATE player_stats_bb SET gp=gp-:gp, dbl_dbl=dbl_dbl-:dbl_dbl, trip_dbl=trip_dbl-:trip_dbl, fgm=fgm-:fgm, fga=fga-:fga, gs=gs-:gs, fgm3=fgm3-:fgm3, fga3=fga3-:fga3, ftm=ftm-:ftm, fta=fta-:fta, tp=tp-:tp, blk=blk-:blk, stl=stl-:stl, ast=ast-:ast,min=min-:min, oreb=oreb-:oreb, dreb=dreb-:dreb, treb=treb-:treb, pf=pf-:pf, tf=tf-:tf,to1=to1-:to1 WHERE playercode=:playercode AND season=:season AND teamcode=:teamcode");
					    $PlayerstatsUpdateQryArr = array(":gp"=>$gp, ":dbl_dbl"=>$dbl_dbl, ":trip_dbl"=>$trip_dbl, ":fgm"=>$fgm, ":fga"=>$fga, ":gs"=>$gs, ":fgm3"=>$fgm3, ":fga3"=>$fga3, ":ftm"=>$ftm, ":fta"=>$fta, ":tp"=>$tp, ":blk"=>$blk, ":stl"=>$stl, ":ast"=>$ast, ":min"=>$min, ":oreb"=>$oreb, ":dreb"=>$dreb, ":treb"=>$treb, ":pf"=>$pf, ":tf"=>$tf, ":to1"=>$to1, ":playercode"=>$playercode, ":season"=>$GameSeason, ":teamcode"=>$teamcode);
					    $PlayerstatsUpdateQry->execute($PlayerstatsUpdateQryArr);
				    }

		    	}

		    	//Delete players from individual_player_stats for gamecode
		    	$DeleteIndPlayerQry = $conn->prepare("DELETE FROM individual_player_stats WHERE gamecode=:gamecode AND season=:season");
			    $DeleteIndPlayerQryArr = array(":gamecode"=>$Gamecode, ":season"=>$GameSeason);
			    $DeleteIndPlayerQry->execute($DeleteIndPlayerQryArr);
		    }

		    //Get teamstats from individual_team_stats for the  gamecode
	    	$IndTeamstatsQry = $conn->prepare("SELECT * FROM individual_team_stats WHERE gamecode=:gamecode AND season=:season");
		    $IndTeamstatsQryArr = array(":gamecode"=>$Gamecode, ":season"=>$GameSeason);
		    $IndTeamstatsQry->execute($IndTeamstatsQryArr);
		    $CntIndTeamstats = $IndTeamstatsQry->rowCount();
		    $Teamstp = "";
		    if ($CntIndTeamstats > 0) {
		    	$FetchTeamstats = $IndTeamstatsQry->fetchAll(PDO::FETCH_ASSOC);
		    	foreach ($FetchTeamstats as $TeamstatsRows) {		    		

		    		$teamcode = $TeamstatsRows["teamcode"];
		    		$month = $TeamstatsRows["month"];
		    		$w = $TeamstatsRows["W"];
		    		$l = $TeamstatsRows["L"];
		    		$gp = $TeamstatsRows["gp"];
		    		$fgm = $TeamstatsRows["fgm"];
		    		$fga = $TeamstatsRows["fga"];
		    		$fgm3 = $TeamstatsRows["fgm3"];
		    		$fga3 = $TeamstatsRows["fga3"];
		    		$ftm = $TeamstatsRows["ftm"];
		    		$fta = $TeamstatsRows["fta"];
		    		$tp = $TeamstatsRows["tp"];
		    		$blk = $TeamstatsRows["blk"];
		    		$stl = $TeamstatsRows["stl"];
		    		$ast = $TeamstatsRows["ast"];		    		
		    		$oreb = $TeamstatsRows["oreb"];
		    		$dreb = $TeamstatsRows["dreb"];
		    		$treb = $TeamstatsRows["treb"];
		    		$pf = $TeamstatsRows["pf"];
		    		$tf = $TeamstatsRows["tf"];
		    		$to1 = $TeamstatsRows["to1"];
		    		$min = $TeamstatsRows["min"];
		    		$pts_bench = $TeamstatsRows["Pts_bench"];
		    		$pts_fastb = $TeamstatsRows["Pts_fastb"];
		    		$pts_paint = $TeamstatsRows["Pts_paint"];
		    		$pts_ch2 = $TeamstatsRows["Pts_ch2"];
		    		$pts_to = $TeamstatsRows["Pts_to"];
		    		$pa = $TeamstatsRows["tp"];


		    		//Check whether players available or not in player_stats_bb
		    		$TeamstatsQry = $conn->prepare("SELECT teamcode FROM team_stats_bb WHERE teamcode=:teamcode AND season=:season AND month=:month");
				    $TeamstatsQryArr = array(":teamcode"=>$teamcode, ":season"=>$GameSeason, ":month"=>$month);
				    $TeamstatsQry->execute($TeamstatsQryArr);
				    $CntTeamstats = $TeamstatsQry->rowCount();
				    

				    if ($CntTeamstats > 0) {

				    	//Minus playerstats for the particular gamecode
				    	$TeamstatsUpdateQry = $conn->prepare("UPDATE team_stats_bb SET W=W-:w,L=L-:l,gp=gp-:gp, fgm=fgm-:fgm,fga=fga-:fga, fgm3=fgm3-:fgm3, fga3=fga3-:fga3, ftm=ftm-:ftm, fta=fta-:fta, tp=tp-:tp, blk=blk-:blk, stl=stl-:stl, ast=ast-:ast, oreb=oreb-:oreb, dreb=dreb-:dreb, treb=treb-:treb, pf=pf-:pf, tf=tf-:tf,to1=to1-:to1,min=min-:min, pts_bench=pts_bench-:pts_bench, pts_fastb=pts_fastb -:pts_fastb, pts_paint=pts_paint-:pts_paint, pts_ch2=pts_ch2-:pts_ch2,pts_to=pts_to-:pts_to WHERE teamcode=:teamcode AND season=:season AND month=:month");
					    $TeamstatsUpdateQryArr = array(":w"=>$w, ":l"=>$l, ":gp"=>$gp, ":fgm"=>$fgm, ":fga"=>$fga, ":fgm3"=>$fgm3, ":fga3"=>$fga3, ":ftm"=>$ftm, ":fta"=>$fta, ":tp"=>$tp, ":blk"=>$blk, ":stl"=>$stl, ":ast"=>$ast, ":oreb"=>$oreb, ":dreb"=>$dreb, ":treb"=>$treb, ":pf"=>$pf, ":tf"=>$tf, ":to1"=>$to1, ":min"=>$min, ":pts_bench"=>$pts_bench, ":pts_fastb"=>$pts_fastb, ":pts_paint"=>$pts_paint, ":pts_ch2"=>$pts_ch2, ":pts_to"=>$pts_to, ":teamcode"=>$teamcode, ":season"=>$GameSeason, ":month"=>$month);
					    $TeamstatsUpdateQry->execute($TeamstatsUpdateQryArr);

					    $Teamstp[$teamcode] = $pa;
					    
				    }

		    	}

		    	//Update home/visiting team pa
    			foreach ($Teamstp as $Key => $Value) {

    				if ($Key == $Hometeamcode) {

	    				//Update hometeam pa in team_stats_bb
	    				$UpdatepaQry = $conn->prepare("UPDATE team_stats_bb SET pa=pa-:pa WHERE teamcode=:teamcode  and season=:season and month=:month");
	    				$UpdatepaQryArr = array(":pa"=>$Value, ":teamcode"=>$Visitingteamcode, ":season"=>$GameSeason , ":month"=>$month);
				    	$UpdatepaQry->execute($UpdatepaQryArr);
	    			} else {

	    				//Update visitingteam pa in team_stats_bb
	    				$UpdatepaQry = $conn->prepare("UPDATE team_stats_bb SET pa=pa-:pa WHERE teamcode=:teamcode  and season=:season and month=:month");
	    				$UpdatepaQryArr = array(":pa"=>$Value, ":teamcode"=>$Hometeamcode, ":season"=>$GameSeason , ":month"=>$month);
				    	$UpdatepaQry->execute($UpdatepaQryArr);
	    			}

    			}
    						    	

		    	//Delete Teams from individual_team_stats for gamecode
		    	$DeleteIndTeamQry = $conn->prepare("DELETE FROM individual_team_stats WHERE gamecode=:gamecode AND season=:season");
			    $DeleteIndTeamQryArr = array(":gamecode"=>$Gamecode, ":season"=>$GameSeason);
			    $DeleteIndTeamQry->execute($DeleteIndTeamQryArr);

		    }

		    // Delete game from game_details
	    	$DeleteGameQry = $conn->prepare("DELETE FROM game_details WHERE id=:gamecode AND season=:season");
		    $DeleteGameQryArr = array(":gamecode"=>$Gamecode, ":season"=>$GameSeason);
		    $DeleteGameQry->execute($DeleteGameQryArr);

		    header("Location:game_stats.php?gid=".$redirectgameid."&msg=1");
	    	exit;
	    }

	    header("Location:game_stats.php?gid=".$redirectgameid."&msg=2");
	    exit;
	}

}
?>