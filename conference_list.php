<?php 
include_once('session_check.php');
include_once('connect.php');
include_once('usertype_check.php');
include_once('common_functions.php');

if (isset($_SESSION["sportid"])) {
    
    $sportid = $_SESSION["sportid"];
    $sportname = $_SESSION["sportname"];
}


// Error msg Start Here
if(isset($_GET["msg"])){
  $msg            =   $_GET["msg"];
} else {
    $msg          =  "";
}
$alertclass = '';
if($msg==1){
    $message    =   "Conference has been added successfully.";
    $alertclass = "alert-success";
}
elseif($msg==2){
    $message    =   "Conference has been updated successfully.";
    $alertclass = "alert-success";
}
elseif($msg==3){
    $message    =   "Conference has been deleted successfully.";
    $alertclass = "alert-danger";
}

// if(isset($_GET["cid"])){
//     $c_id=base64_decode($_GET["cid"]);
//     $sport=$_GET["sport"];
//     $delResQry      = $conn->prepare("delete from customer_conference where id=:id");
//     if($delResQry->execute(array(":id"=>$c_id))){
//          header('Location:conference_list.php?msg=3&sport='.$sport);
//          exit;
        
//     }
// }


if(isset($_POST["addupadte"])){
    $confere_name         =   $_POST['confere_name'];
    $sportna              =   $_POST['sport_namels']; 
    $sportls              =   $_POST['sport_name']; 
    $conference_id       = base64_decode($_POST["hnd_conferenceid"]);

    if($sportna!=""){
        $sportname=$sportna;
    }
    if($sportls !=""){
        $sportname=$sportls;
    }
    if($conference_id!=""){
        $update_results=array(":confere_name"=>$confere_name,":id"=>$conference_id);
        $updateQry="update customer_conference set conference_name=:confere_name where id=:id";
        $prepupdateQry=$conn->prepare($updateQry);
        $updateRes=$prepupdateQry->execute($update_results);
        if($updateRes){
            header('Location:conference_list.php?msg=2');
           exit;
        }
   }
    else{
        $insert_results=array(":cid"=>$LoginCustId, ":confere_name"=>$confere_name);
        $insertqry="insert into customer_conference(customer_id, conference_name) values(:cid, :confere_name)";
        $prepinsertqry=$conn->prepare($insertqry);
        $insertRes=$prepinsertqry->execute($insert_results);
        if($insertRes){
            header('Location:conference_list.php?msg=1');
            exit;
        }
     }
}

if(isset($_REQUEST["hdnsearch"])){
    $HiddenSearch = $_REQUEST["hdnsearch"];
    $HiddenSearchText = ( $HiddenSearch )? $HiddenSearch : $_POST['confere_name'] ;
    $HiddenSearchCondn = !empty($HiddenSearchText)? "and conference_name like '%$HiddenSearchText%'": "";
} else {
    $HiddenSearchCondn="";
}

//$HiddenSearchCondn = !empty($HiddenSearchText)? "and conference_name like '%$HiddenSearchText%'" : "" ;

/****Paging ***/
$Page=1;$RecordsPerPage=25;
if(isset($_REQUEST['HdnPage']) && is_numeric($_REQUEST['HdnPage']))
    $Page=$_REQUEST['HdnPage'];
/*End of paging*/

include_once('header.php'); ?>
<link href="assets/custom/css/conferencelist.css" rel="stylesheet" type="text/css" />
<style type="text/css">

table.dataTable.no-footer {
    border-bottom: 0px solid #111; 
}
table.dataTable{
    border-collapse: collapse;
}
.error{
    color: red;
}
</style>
<div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            <div class="row">
                <div class="col-md-12 ">
                    <form  method="post" id="searchplayedform">
                         
                        <input type="hidden" name="customerid" id="customerid" value="<?php echo $LoginCustId ?>">
                        <input type="hidden" name="sportid" id="sportid" value="<?php echo $sportid ?>">
                        <div class="portlet light col-sm-12 col-xs-12 col-md-12" style="padding: 15px 15px 0px;background: #FFF;border: 1px solid #CCC;-moz-border-radius: 5px;-webkit-border-radius: 5px;border-radius: 5px !Important;    float: left;margin-bottom:10px;">
                            <div class="portlet-title " style="border-bottom: 0px solid #eee;">                            
                                <div class="col-md-9 col-sm-9 col-lg-7" style="padding:0px">
                                    <div class="col-md-7 col-sm-6 col-xs-12 searchboxstyle" style="padding-right:0px">
                                        <div class="form-group">
                                            <input class="form-control border-radius conferencesearch" type="text" name="conferencename"  placeholder="Conference Name" id="conferencename" value="<?php echo $HiddenSearch; ?>"> 
                                        </div>
                                    </div>
                                     <div class="col-md-2 col-sm-3 col-xs-3  style" id="" style="text-align: center;padding: 0px ;">
                                        <div class="form-group">
                                       <input type="button" id="searchbtn" name="searchbtnpost" class="btn searchbtnyellow" value="Search" style="border-radius:5px !important;line-height: 1.5;"> 
                                           
                                        </div>
                                    </div>

									<div class="col-md-2 col-sm-3 col-xs-3  style" id="" style="text-align:left;padding: 2px 0px 0px 0px ;">
                                        <div class="form-group">
                                            <a class="btn  resetbtn resetbtnred">Reset</a> 
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </form>
                 </div><!--Col-md-12 -->
            </div> <!--row -->

            <?php
                if(!empty($message)){
                ?>
                <div class="alert <?php echo $alertclass; ?>" style="margin-bottom:10px;">
                <a class="close" data-dismiss="alert" href="#">x</a>
                <?php echo $message;?>
                </div>
                <?php
                }
                ?>
            <div class="row">
                <div class="col-md-12">                    
                    <div class="portlet-body customerlist-tbl-pr clearfix" style="clear: both;">
                        <div class="widget-header"> 
                            <h3>
                            <i class="icon-settings font-red-sunglo"></i>
                            LIST OF CONFERENCES                       
                            </h3>
                            <div class="pull-right mobile_right">
                                <button  type="button" class="player_btn popup" style="margin-right: 14px;border-radius: 4px !important;">Add Conference</button> 
                                <input type="hidden" name="sportname" id="sportname" value="<?php echo $sportname; ?>">
                            </div>
                        </div>
                        <div id="static" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                        <h4 class="modal-title">Conference Information <span style="margin-right: 10%;" class="mode-color" id="mode"></span></h4>
                                    </div>
                                    <div class="modal-body" style="padding-bottom:7%;">
                                        
                                        <form role="form" name="frm_conference" id="frm_conference" method="POST">
                                            <input type="hidden" name="hnd_img" id="hnd_img" value="<?php echo  $image_db ?>">
                                            <input type="hidden" name="sport_name" id="sport_name" value="<?php echo $sportname; ?>">
                                            <input type="hidden" name="sport_namels" id="sport_namels" value="<?php echo $ls; ?>">
                                            <input type="hidden" name="hnd_conferenceid" id="hnd_conferenceid" >
                                            <input type="hidden" name="hnd_cid" id="hnd_cid">
                                            
                                            <input type="hidden" name="hndcoustid" id="hndcoustid" value="<?php echo $LoginCustId?>">
                                            <div class="portlet light  team_bio_portlet">
                                                <div class="portlet-body form"> 
                                                    <div class="form-body">
                                                        <div class="col-md-10 col-sm-10 col-xs-12 paddingremove confmodalpopup">
                                                            <div class="form-group  playerinfo_paddingleft">
                                                                <label>Conference Name: <span class="error">*</span></label>
                                                                <input class="form-control border-radius" type="text" name="confere_name" id="confere_name" placeholder="Conference Name" value="<?php echo $conference_name_db;?>" /> 
                                                            </div>
                                                            <div class="" style="text-align: left;">
                                                                <button type="submit" name="addupadte" class="btn btnpopupgreen conferencebtn" id="mode_btn">Save/Update</button>
                                                                <button type="button" class="btn btnpopupred" data-dismiss="modal" style="margin-left: 15px;">Cancel</button>
                                                            
                                                            </div>
                                                        </div> 
                                                        
                                                    </div>                                                    
                                                   
                                                </div>
                                            </div>
                                       </form>
                                      
                                    </div>            
                                </div>
                            </div>
                        </div>
                        <div class="loadingsection">
                            <img src="images/loading-publish.gif" alt="loadingimage" id="loadingimage">
                        </div>
                        <div class="portlet-body">
                       
                        <div class="table-responsive" id="sample" > 
                            <form id="frm_confrerence_list" name="frm_confrerence_list" method="post" action="conference_list.php">                          
                            <input type="hidden" name="hdnsearch" id="hdnsearch" value="<?php echo $HiddenSearch; ?>">
                            <input type="hidden" name="HdnPage" id="HdnPage" value="<?php echo $Page; ?>">
                            <input type="hidden" name="HdnMode" id="HdnMode" value="<?php echo $Page; ?>">
                            <input type="hidden" name="RecordsPerPage" id="RecordsPerPage" value="<?php echo $RecordsPerPage; ?>">
                            <table class="table table-striped table-bordered table-hover dataTable no-footer dataTable" id="sample_1" sytle="border: 1px solid #CCC;border-collapse: collapse;">
                                <thead>
                                <tr>
                                    <th class="tbl_center" style="width:150px;"> Conference  Id </th>
                                    <th class="tbl_center" > Conference  Name </th>
                                    <th class="tbl_center" > Action </th>
                                </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        // if ($_SESSION['master'] == 1) {
                                        if ($_SESSION['logincheck'] == 'master') {
                                            $SelCustId = $customerid;
                                        } else {
                                            $SelCustId = $LoginCustId;
                                        }
                                        $dbQry="select * from customer_conference where customer_id in ($SelCustId) $HiddenSearchCondn";

                                        $getResQry      =   $conn->prepare($dbQry);
                                        $getResQry->execute($srchArr);
                                        $getResCnt      =   $getResQry->rowCount();
                                        $getResQry->closeCursor();
                                        if($getResCnt>0){
                                            $TotalPages=ceil($getResCnt/$RecordsPerPage);
                                            $Start=($Page-1)*$RecordsPerPage;
                                            $sno=$Start+1;
                                                
                                            $dbQry.=" limit $Start,$RecordsPerPage";
                                                    
                                            $getResQry      =   $conn->prepare($dbQry);
                                            $getResQry->execute($srchArr);
                                            $getResCnt      =   $getResQry->rowCount();
                                        if($getResCnt>0){
                                            $getResRows     =   $getResQry->fetchAll();
                                            $getResQry->closeCursor();
                                            $s=1;
                                                foreach($getResRows as $conference_info){
                                                    $conference_no   =   $conference_info["id"];
                                                    $conference_name =   $conference_info["conference_name"];
                                                    $last_name       =   $conference_info["lastname"];
                                                   
                                                   
                                    ?>
                                   
                                    <tr class="odd gradeX">
                                        <td class="tbl_center_td" style="text-align:center"><?php echo $conference_no; ?></td>
                                        <td class="tbl_center_td">
                                            <?php echo $conference_name; ?>
                                        </td>

                                        <td class="tbl_center_td">                                            
                                            <a href="#" id="edit_division" data-id="<?php  echo base64_encode($conference_no); ?>" data-name="<?php echo $conference_name;?>"
                                            data-sport="<?php echo $sportname; ?>" data-toggle="modal" class="roundbtngreenedit btn-circle btn-icon-only edit_popup"  data-cid="<?php echo $LoginCustId;?>" customerid="<?php echo $division['id']; ?>"><i class="icon-note trash_btn"></i>
                                            </a>
                                    </td>

                                    </tr>
                                    <?php
                                        $s++;
                                       }
                                    } 
                                        else{
                                            // if ($_SESSION['master'] != 1)
                                            // echo "<tr><td colspan='3' style='text-align:center;'>No Conference(s) found.</td></tr>";
                                        }
                                    }
                                    else{
                                        // if ($_SESSION['master'] != 1)
                                            // echo "<tr><td  colspan='3' style='text-align:center;'>No Conference(s) found.</td></tr>";
                                    }
                                    ?>
                                    
                                </tbody>
                            </table>
                            <?php
                                if($TotalPages > 1){

                                echo "<tr><td style='text-align:center;' colspan='3' valign='middle' class='pagination'>";
                                $FormName = "frm_confrerence_list";
                                require_once ("paging.php");
                                echo "</td></tr>";

                                }
                           ?>
                        </div>
                        </form>
                    </div>
                    
                    </div><!--- portlet-body customerlist-tbl-pr clearfix !-->
                </div><!--- col-md-12 !-->
            </div><!--- row !-->          
            
                            <!-- END SAMPLE TABLE PORTLET-->
            </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->                
</div>
<!-- END CONTAINER -->
<?php include_once('footer.php'); ?>
<script src="assets/custom/js/manageconference.js" ></script>