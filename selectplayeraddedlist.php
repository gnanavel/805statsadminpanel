<?php
include_once('session_check.php');
include_once('connect.php'); 

$SportId= $_SESSION['sportid'];

if(isset($_POST['teamid'])){
	$SeasonId      = $_POST['seasonid'];
	$divisionid    = $_POST['divisionid'];
	$conferenceid  = $_POST['conferenceid'];
	$teamid		   = $_POST['teamid'];
	$PostType      = $_POST['post_type'];
	$responseHtml = '';

	if($PostType=='selectplayertbl'){

		$AssignedOtions = $SelectedTeamPlayer = $PlayeroptionsNot='';
		$AssigneidArr=array();$Assigneid=array();
		$Assstatus=array();	$AssstatusArr=array();
		$CusteamRes="select *, team_id as assigned,SUM(status) as pstatus from customer_team_player where season_id='$SeasonId' and customer_id=$MasterCustId group by id DESC";
		$Qry5		= $conn->prepare($CusteamRes);
		$Qry5->execute();
		$QryCntSeason5 = $Qry5->rowCount();
		if($QryCntSeason5 > 0){
			while ($row5 = $Qry5->fetch(PDO::FETCH_ASSOC)){	
				if($row5['isdelete']=='0'){
					if($row5['assigned']!=$teamid){
						$Assigneid[]=$row5['player_id'];
						$pstatus=$row5['pstatus'];
					}
					if($row5['assigned']==$teamid){
						$Assstatus[]=$row5['player_id'];
					}
				}
				
			}
		}	
		
		$AssigneidArr=implode(",",$Assigneid);
		$AssstatusArr=implode(",",$Assstatus);
		$QryCondn  = ($AssigneidArr!='')?"and id IN (".$AssigneidArr.")":'';
		 $AssRes="select * from customer_team_player as seasonplayer LEFT JOIN player_info as playertbl ON  seasonplayer.player_id=playertbl.id where seasonplayer.customer_id=$MasterCustId and seasonplayer.season_id=$SeasonId   and seasonplayer.isdelete=0 and  playertbl.lastname!='TEAM'";
		
		$Qry2		= $conn->prepare($AssRes);
		$Qry2->execute();
		$QryCntSeason2 = $Qry2->rowCount();
		if($QryCntSeason2 >0){
			while ($row2 = $Qry2->fetch(PDO::FETCH_ASSOC)){	
				$PlayeroptionsNot .="<option value='".$row2['id']."' disabled>".$row2['firstname']." ".$row2['lastname']."</option>";
			}	
		}
		
		$AssQryCondn  = ($AssigneidArr!='')?"and id NOT IN (".$AssigneidArr.")":'';
		$AssoptionRes="select  id,firstname,lastname from player_info where sport_id='$SportId' and customer_id in($customerid) and lastname!='TEAM' $AssQryCondn group by id DESC order by firstname ASC";
		
		$Qry3		= $conn->prepare($AssoptionRes);
		$Qry3->execute();
		$QryCntSeason3 = $Qry3->rowCount();
		if($QryCntSeason3 >0){
			while ($row3 = $Qry3->fetch(PDO::FETCH_ASSOC)){	
					$AssignedOtions .= "<option value='".$row3['id']."'>".$row3['firstname']." ".$row3['lastname']."</option>";	
			}
		}
		
		if($AssignedOtions=='' && $PlayeroptionsNot==''){
			$AssignedOtions = "<option value=''>No Player found</option>";
		}

		$QryExeDiv = $conn->prepare("select * from customer_team_player as seasonplayer LEFT JOIN player_info as playertbl ON  seasonplayer.player_id=playertbl.id where seasonplayer.customer_id=$MasterCustId and seasonplayer.conference_id=:conference_id and season_id=:season_id and division_id=:division_id and seasonplayer.team_id=:team_id and seasonplayer.isdelete=0 and playertbl.lastname!='TEAM'");
		$QryarrCon = array(":conference_id"=>$conferenceid,":season_id"=>$SeasonId,":division_id"=>$divisionid,":team_id"=>$teamid);
		$QryExeDiv->execute($QryarrCon);
			
			$QryCntSeasonconf = $QryExeDiv->rowCount();
			if($QryCntSeasonconf>0){				
				while ($rowPlayer = $QryExeDiv->fetch(PDO::FETCH_ASSOC)){
					$SelectedTeamPlayer .= "<option value='".$rowPlayer['id']."'>".$rowPlayer['firstname']." ".$rowPlayer['lastname']."</option>";					
				}				
			}
			$responseHtml .='<form name="addplayersform" id="playerfrm" method="POST" class="form-horizontal" novalidate="novalidate">		 
			<input type="hidden" name="teamid" id="teamidhidden" value="'.$teamid.'" /> 
			<div class="col-md-12 formcontainer" style="margin:auto;float:none;">
				<div class="row">
					<div class="col-sm-5">
						<select name="from[]" id="multiselect" class="form-control" size="8" multiple="multiple">'.$AssignedOtions.$PlayeroptionsNot.'</select>
					</div>				
					<div class="col-sm-2" style="margin-top: 110px;">
						<button type="button" id="multiselect_rightAll" class="btn btn-block"><i class="glyphicon glyphicon-forward"></i></button>
						<button type="button" id="multiselect_rightSelected" class="btn btn-block"><i class="glyphicon glyphicon-chevron-right"></i></button>
						<button type="button" id="multiselect_leftSelected" class="btn btn-block"><i class="glyphicon glyphicon-chevron-left"></i></button>
						<button type="button" id="multiselect_leftAll" class="btn btn-block"><i class="glyphicon glyphicon-backward"></i></button>
					</div>				
					<div class="col-sm-5">
						<select name="to[]" id="multiselect_to" class="form-control" size="8" multiple="multiple">'.$SelectedTeamPlayer.'</select>		 
						<div class="row">
							<div class="col-sm-6">
								<button type="button" id="multiselect_move_up" class="btn btn-block"><i class="glyphicon glyphicon-arrow-up"></i></button>
							</div>
							<div class="col-sm-6">
								<button type="button" id="multiselect_move_down" class="btn btn-block col-sm-6" style="float: left;"><i class="glyphicon glyphicon-arrow-down" ></i></button>
							</div>
						</div>
					</div>
				</div>			
				
				<div class="form-group col-md-12 ">										
					<input class="btn addnewplayerbtn" type="button" value="Submit">
					<button class="btn cancelbtn" type="button" data-dismiss="modal">Cancel</button>
				</div>	
			</div>
			</form>
			<table width="100%" id="loadingplayer"><tr><td align="center"><img src="assets/custom/imgs/loading.gif" style="margin-right: 10px;width: 75px;"></td><tr><td align="center" style="font-size:15px;color:green;">Assign players to team... Please wait...</td></tr></table>

			<table width="100%" id="playerstsmsg"><tr><td align="center" style="font-size:15px;color:green;">Players assigned to team successfully..</td></tr></table><script type="text/javascript">jQuery(document).ready(function($) {$("#multiselect").multiselect({sort:false,search: {
				left: \'<input type="text" name="q" class="form-control searchteambox" placeholder="Search Player" /><label>Select Players</label>\',
				right: \'<p class="clearfix" style="margin-top: 48px;margin-bottom: 2px;"><label>Selected Player</label></p>\',
			},});});</script>';
			
			echo $responseHtml;			
		}	
	exit;
}
?>