<?php 
include_once('session_check.php');
include_once('connect.php'); 
include_once('header.php'); 
?>
<link href="assets/custom/css/manageseason.css" rel="stylesheet" type="text/css" />
<script src="assets/global/scripts/jquery-ui.js" type="text/javascript"></script>
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            
            <div class="row">            	
				<div class="col-md-12 ">
					<!-- BEGIN SAMPLE FORM PORTLET-->
					<div class="portlet light headertopbox" style="margin-bottom: 10px;padding-bottom: 10px;">
						<div class="portlet-title">
							<div class="caption font-red-sunglo">
								<i class="icon-settings font-red-sunglo"></i>
								<span class="caption-subject bold uppercase"> Season / Conference / Division</span>
							</div>
							<button type="button" class="btn uppercase addseasonbtntop" data-toggle="modal" data-target="#seasonModal"  style='float: right;'>Add season</button>
						</div>
					</div>

					 <div class="portlet light bottommainwrap" style="overflow: auto;padding: 12px 0px;">
						<div class="portlet-body form seasonmainwrapper" style="overflow:hidden;">							
						<?php	
							$Qry		= $conn->prepare("select * from customer_season where custid in($customerid) order by season_order DESC");							
							$Qry->execute();
							$QryCntSeason = $Qry->rowCount();
							$DivisionWrapHtml= $AddNewSeasonTree='';
							$CopyFromSeason = false;
							$Inc =0;
							$TreeLine  =48;
							if ($QryCntSeason > 0) {
								$CopyFromSeason = true;
								while ($row = $Qry->fetch(PDO::FETCH_ASSOC)){

									$QryExe = $conn->prepare("select * from customer_season_conference as seasonconf LEFT JOIN customer_conference as custconf ON  seasonconf.conference_id=custconf.id where season_id=:season_id order by  seasonconf.conf_order ASC");
									$Qryarr = array(":season_id"=>$row['id']);
									$QryExe->execute($Qryarr);
									$QryCntSeasonconf	= $QryExe->rowCount();	
									$confCount = $QryCntSeasonconf;
									$SeletedArrConf		= array();
									$SeletedArrDiv		= array();
									$AddNewSeason = $AddSeasontbl = $Conferencetbl= '';
									$TreeLine=47;	
									if ($QryCntSeasonconf > 0) {
										while ($rowSeason = $QryExe->fetch(PDO::FETCH_ASSOC)){												
											$QryExeDiv = $conn->prepare("select * from customer_conference_division as seasonconfdiv LEFT JOIN customer_division as custconf ON  seasonconfdiv.division_id=custconf.id where seasonconfdiv.conference_id=:conference_id and season_id=:season_id order by seasonconfdiv.div_order ASC");
											$QryarrCon = array(":conference_id"=>$rowSeason['conference_id'],":season_id"=>$row['id']);
											$QryExeDiv->execute($QryarrCon);
											$QryCntSeasonconf = $QryExeDiv->rowCount();
											$Divisiontbl = $AddNewSeason='';										
											$TreeLine=48+($QryCntSeasonconf*44)-23;

											while ($rowSeasonDiv = $QryExeDiv->fetch(PDO::FETCH_ASSOC)){												
												$Selected = ($rowSeasonDiv['status'])?'checked':'';
												$Divisiontbl .= "<div class='divisions' data-divid='".$rowSeasonDiv['id']."'><table class='table innerdivtable'><tr><td class='divisionbtns'><span class='divisioncircle circle'>D</span><label class='mt-checkbox'><input type='checkbox' class='division-checked' name='division[]'  value='".$rowSeasonDiv['id']."' $Selected> ".$rowSeasonDiv['name']."<span></span></label><a class='btn btn-circle btn-icon-only btn-default red deletebtndiv tooltips' href='javascript:;' data-container='body' data-placement='top' data-original-title='Delete division' ><i class='icon-trash'></i></a><a href='add_divisionteam.php?divisionid=".$rowSeasonDiv['id']."&conferenceid=".$rowSeason['id']."&seasonid=".$row['id']."' class='btn btn-circle btn-icon-only btn-default green adddivisionbtn tooltips'			data-conferencename='".$rowSeason['conference_name']."'data-container='body' data-placement='top' data-original-title='Add Team'><i class='fa fa-plus'></i></a></td></tr></table></div>";	
												$SeletedArrDiv[] = $rowSeasonDiv['id'];
												
												$AddNewSeason .= "<table class='table innerdivtable'><tr><td class='divisionbtns'><span class='divisioncircle circle'>D</span><label class='mt-checkbox'><input type='checkbox' name='season[conference".$rowSeason['id']."][division".$rowSeasonDiv['id']."]' value='".$rowSeasonDiv['id']."###".$rowSeason['id']."' $Selected> ".$rowSeasonDiv['name']."<span></span></label></td></tr></table>";	

													
												
											}
											
											$Selected = ($rowSeason['status'])?'checked':'';
											$Conferencetbl .= "<div class='conferencetbltoggle' data-conf='".$rowSeason['id']."'><table class='table innertable' id='innertblid".$rowSeason['id']."'><tr><td class='conferencebtns '><span class='conferencecircle circle'>C</span><label class='mt-checkbox '><input type='checkbox' name='' class='conference-checked'  value='".$rowSeason['id']."' $Selected> ".$rowSeason['conference_name']."<span></span></label><a class='btn btn-circle btn-icon-only btn-default red deletebtnconf tooltips' href='javascript:;' data-container='body' data-placement='top' data-original-title='Delete conference' data-conferenceid='".$rowSeason['id']."' data-seasonid='".$row['id']."'
											><i class='icon-trash'></i></a><a class='btn btn-circle btn-icon-only btn-default blue managedivisionbtn tooltips' href='javascript:;' data-container='body' data-placement='top' data-original-title='Manage division'  data-target='#managedivModal' data-conferenceid='".$rowSeason['id']."' data-seasonid='".$row['id']."'><i class='icon-wrench'></i></a><a class='btn btn-circle btn-icon-only btn-default green adddivisionbtn tooltips' href='javascript:;' data-container='body' data-placement='top' data-original-title='Add division' data-toggle='modal' data-target='#DivisionModal' data-conferenceid='".$rowSeason['id']."' data-seasonid='".$row['id']."'
											data-conferencename='".$rowSeason['conference_name']."'><i class='fa fa-plus'></i></a></td></tr></table><div class='divisiontbltoggle'>".$Divisiontbl."</div></div>";	

											$AddSeasontbl .= "<table class='table innertable' id='innertblid".$rowSeason['id']."'><tr><td class='conferencebtns '><span class='conferencecircle circle'>C</span><label class='mt-checkbox'><input type='checkbox' class='conference-checked' name='season[conference".$rowSeason['id']."]' value='".$rowSeason['id']."###".$row['id']."' $Selected class='conferencechkbox'> ".$rowSeason['conference_name']."<span></span></label></td></tr></table>".$AddNewSeason;	
											$SeletedArrConf[] = $rowSeason['id'];
											$SeletedArrDiv		= array();
										}
									}									

									

									$SeasonTree = "<table class='table  ms_seasontble msseasontbleborder_".$row['id']."' data-seasion='".$row['id']."'><tr><td><span class='seasoncircle circle'>S</span><label class='mt-checkbox'><input type='checkbox' name='' value='".$row['id']."' checked='checked' disabled> ".$row['name']."<span></span></label><a class='btn btn-circle btn-icon-only btn-default red deleteseasonbtn tooltips' href='javascript:;' data-container='body' data-placement='top' data-original-title='Delete season' data-seasonid='".$row['id']."'><i class='icon-trash' ></i></a><a class='btn btn-circle btn-icon-only btn-default blue    manageconferencemodel tooltips' href='javascript:;'  data-seasonid='".$row['id']."' data-container='body' data-placement='top' data-original-title='Manage conference'><i class='icon-wrench'></i></a><a class='btn btn-circle btn-icon-only btn-default green addconferencebtn tooltips' href='javascript:;' data-toggle='modal' data-target='#ConferenceModal' data-seasonid='".$row['id']."' data-seasonname='".$row['name']."' data-container='body' data-placement='top' data-original-title='Add conference'><i class='fa fa-plus'></i></a><div class='parent_confernce'>$Conferencetbl</div></td></tr></table>";


									$AddNewSeasonTree .= "<div class='mt-radio-list clearfix'><label class='mt-radio'><input type='radio' name='seasonnamesnewid' value='".$row['id']."' class='seasonnamesnew'>".$row['name']."<span></span></label></div><table class='table  ms_seasontble popupmodelseason'><tr><td><form method='POST' class='selectedseasontree' novalidate='novalidate'><span class='seasoncircle circle'>S</span><label class='mt-checkbox'><input type='checkbox' name='seasonnames[]' value='".$row['id']."' checked disabled> ".$row['name']."<span></span></label>$AddSeasontbl</form></td></tr></table>";					
									if($Inc==0){
										$Disp = 'display:block;';
									}else{
										$Disp = 'display:none;';
									}
									//$confCount = $QryCntSeasonconf;							
									

									if($TreeLine==25){
										$TreeLine=45;
									}
									
									$BorderStyleCss = '<div id="msseasontbleborder_'.$row['id'].'" style="display:none;"><style>table.msseasontbleborder_'.$row['id'].'>tbody>tr>td:after{bottom: '.$TreeLine.'px;}</style></div>';
									
									echo '<div class="col-md-12 col-sm-12 seasonwrapcont" id="seasonmaincont_'.$row['id'].'" data-seasonid="'.$row['id'].'">'.$BorderStyleCss.'<div class="portlet box grey seasontbltogglewrap">
										<div class="portlet-title">
											<div class="caption tools" style="width: 98%;">
												<a href="javascript:;" class="expand" style="color:#000;background-image:none;display: block;width: 100%;"> '.$row['name'].'</a>
											</div>												
											<div class="tools">
												<a href="javascript:;" class="expand" style=""></a>
											</div>
										</div>
										<div class="portlet-body seasontbltoggle" style="'.$Disp.'">'.$SeasonTree.$DivisionWrapHtml.'
										</div>
									</div></div>';
									$conference='';
									$Inc++;
								} 	
							}else{
								echo '<div class="col-md-12 col-sm-12 seasonwrapcont donotdraganddrap text-center">
									<div class="portlet box grey seasontbltogglewrap">
										<div class="portlet-title">
											<div class="caption tools" style="width: 98%;">
												<a href="javascript:;" class="collapse" style="color:#000;background-image:none;display: block;width: 100%;">No season(s) found</a>
											</div>
										</div>
									</div>
								</div>';
							}							   
							?>							
						  </div> 
						  <div style='min-height:100px;'></div>
						</div>
					</div>               
            </div>
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->                
</div>
<!-- END CONTAINER -->

<?php include_once('manage_season_popup.php'); ?>
<?php include_once('footer.php'); ?>
<style>
.seasonwrapcont.ui-sortable-placeholder{
	height:45px;
}

.ui-sortable-helper{
	z-index:999999 !important;
}
.conferenceplaceholder{
	-webkit-box-shadow: inset 0px 1px 11px 0px rgba(50, 50, 50, 0.75);
	-moz-box-shadow:    inset 0px 1px 11px 0px rgba(50, 50, 50, 0.75);
	box-shadow:         inset 0px 1px 11px 0px rgba(50, 50, 50, 0.75);
	min-height: 45px;
	visibility: visible !Important;
	opacity: 1 !important;
	display: block !Important;
	width:100% !important;
	margin:auto !important;
	margin-left:1% !important;
}
@media(max-width:1300px){
	.seasontbltogglewrap .caption.tools{
		width:90% !important;
	}
	.innerdivtable {
		width:93%
	}
}
/*** **/
</style>

<script src="assets/custom/js/manageseasoncustom.js" type="text/javascript"></script>
