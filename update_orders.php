<?php 
include_once('session_check.php');
include_once('connect.php'); 


if((isset($_POST['post_type'])) && ($_POST['post_type']=='seasonorderchange')){	
	$seasonidArr  = $_POST['seasonid'];	
    $Inc = 1;
	echo $TotalSeason = count($seasonidArr);
	
	foreach($seasonidArr as $seasonid){		
		$updateRes = $conn->prepare("UPDATE customer_season SET season_order=:season_order WHERE custid in($customerid) and id=:season_id ");
		$QryCond   = array(":season_order"=>$TotalSeason,':season_id'=>$seasonid);
		$updateRes->execute($QryCond);		
		$TotalSeason--;
	}	
	echo "success";
	exit;
}

if((isset($_POST['post_type'])) && ($_POST['post_type']=='conforderchange')){	
	$seasonid         = $_POST['seasonid'];
	$conferenceidArr  = $_POST['conferenceid'];	
    $Inc = 1;

	foreach($conferenceidArr as $conferenceid){
	
		$updateRes = $conn->prepare("UPDATE customer_season_conference SET conf_order=:conf_order WHERE customer_id=:cid and season_id=:season_id and conference_id=:conference_id");
		$QryCond   = array(":conf_order"=>$Inc,':cid'=>$MasterCustId, ':season_id'=>$seasonid,':conference_id'=>$conferenceid);
		$updateRes->execute($QryCond);
		
		$Inc++;
	}	
	echo "success";
	exit;
}

if((isset($_POST['post_type'])) && ($_POST['post_type']=='divisionorderchange')){
	$seasonid      = $_POST['seasonid'];
	$conferenceid  = $_POST['conferenceid'];
	$divisionidArr = $_POST['divisionid'];
	
	$Inc = 1;

	foreach($divisionidArr as $divisionid){
		
		$updateRes = $conn->prepare("UPDATE customer_conference_division SET div_order=:div_order WHERE customer_id=:cid and season_id=:season_id and division_id=:division_id and conference_id=:conference_id");
		$QryCond   = array(":div_order"=>$Inc,':cid'=>$MasterCustId, ':season_id'=>$seasonid, ':division_id'=>$divisionid, ':conference_id'=>$conferenceid);
		$updateRes->execute($QryCond);
		
		$Inc++;
	}	
	echo "success";
	exit;
}




if((isset($_POST['post_type'])) && ($_POST['post_type']=='divisionorderchangeconftoconf')){
	$seasonid			= $_POST['seasonid'];
	$conferenceid		= $_POST['newconferenceid'];
	$divisionidArr		= $_POST['divisionid'];
	$oldconferenceid	= $_POST['oldconferenceid'];
	$moveddivision		= $_POST['moveddivision'];
	
	$Inc = 1;
	
	foreach($divisionidArr as $divisionid){
		
		$updateRes = $conn->prepare("UPDATE customer_conference_division SET div_order=:div_order WHERE customer_id=:cid and season_id=:season_id and division_id=:division_id and conference_id=:conference_id");
		$QryCond   = array(":div_order"=>$Inc,':cid'=>$MasterCustId, ':season_id'=>$seasonid, ':division_id'=>$divisionid, ':conference_id'=>$conferenceid);
		$updateRes->execute($QryCond);
		if($divisionid==$moveddivision){			
			
			$updateRes2 = $conn->prepare("UPDATE customer_conference_division SET div_order=:div_order, conference_id=:conference_id WHERE customer_id=:cid and season_id=:season_id and division_id=:division_id and conference_id=:oldconferenceid");
			$QryCond2   = array(":div_order"=>$Inc,':cid'=>$MasterCustId, ':season_id'=>$seasonid, ':division_id'=>$moveddivision, ':conference_id'=>$conferenceid,':oldconferenceid'=>$oldconferenceid);
			$updateRes2->execute($QryCond2);

			$updateRes3 = $conn->prepare("UPDATE customer_division_team set conference_id=:conference_id WHERE customer_id=:cid and season_id=:season_id and division_id=:division_id and conference_id=:oldconferenceid");
			$QryCond3   = array(':cid'=>$MasterCustId, ':season_id'=>$seasonid, ':division_id'=>$moveddivision, ':conference_id'=>$conferenceid,':oldconferenceid'=>$oldconferenceid);
			$updateRes3->execute($QryCond3);

			$updateRes4 = $conn->prepare("UPDATE customer_team_player  set conference_id=:conference_id WHERE customer_id=:cid and season_id=:season_id and division_id=:division_id and conference_id=:oldconferenceid");
			$QryCond4   = array(':cid'=>$MasterCustId, ':season_id'=>$seasonid, ':division_id'=>$moveddivision, ':conference_id'=>$conferenceid,':oldconferenceid'=>$oldconferenceid);
			$updateRes4->execute($QryCond4);

		}
		$Inc++;
	}	
	echo "success";
	exit;
}
?>