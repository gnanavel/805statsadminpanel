<?php 
include_once('session_check.php');
include_once('connect.php');
include_once('common_functions.php');
include_once('usertype_check.php');
// error_reporting(E_ALL);
$team_manager_id = "";
$signin = "";
if (isset($_SESSION['signin'])) {

    $signin = $_SESSION['signin'];
    $teamlogin_id = $_SESSION['team_manager_id'];
    $team_manager_id = " and team_id='$teamlogin_id'";
}

if (isset($_SESSION["sportid"])) {

    $sportid = $_SESSION["sportid"];
    $sportname = $_SESSION["sportname"]; 
}

if ($sportname == 'softball' || $sportname == 'baseball') {
    $table = "player_stats_ba";
    $itable = "individual_player_stats_ba";
} 
if ($sportname == 'basketball') {
    $table = "player_stats_bb";
    $itable = "individual_player_stats";
}
if ($sportname == 'football') {
    $table = "player_stats_fb";
    $itable = "individual_player_stats_fb";
}

if(isset($_POST['playerid'])&& !empty($_POST['seasons'])){
    $PlayerID     = $_POST['playerid'];
    $SeasonsArr   = $_POST['seasons'];
    $Inc = 0;
    foreach($SeasonsArr as $SeasonId){      
        $SeasonIds      = "##".implode("##",$SeasonId)."##";
        $TeamQry        = "select * from player_info where id=:PlayerID";
        $getTeamQry      =   $conn->prepare($TeamQry);
        $getTeamQry->execute(array(":PlayerID"=>$PlayerID));
        $getTeamCount      =  $getTeamQry->rowCount();
        $TeamRes     =   $getTeamQry->fetch();
        $TeamId         = $TeamRes['team_id'];
        $created_date   = date("Y-m-d H:i:s");

        $tmpFilePath = $_FILES['upload']['tmp_name'][$Inc];
        if ($tmpFilePath != ""){
            $FileName  = date('YmdHis').$_FILES['upload']['name'][$Inc];
            $newFilePath = "uploads/players/".$FileName;
            move_uploaded_file($tmpFilePath, $newFilePath);
            $data = array(
             'width' => "200",
             'height' =>"200",
             'image_source' =>SYSTEM_ROOT_PATH."/uploads/players/".$FileName,
             'destination_folder' =>SYSTEM_ROOT_PATH."/uploads/players/thumb/",
             'image_name' => $FileName
           );     
           $cropsucessfully = cropImage($data); 
            
        }
        
        $insert_results=array(
			":PlayerID"=>$PlayerID, 
			":cid"=>$LoginCustId, 
			":SeasonIds"=>$SeasonIds, 
			":TeamId"=>$TeamId, 
			":FileName"=>$FileName, 
			":created_date"=>$created_date, 
			":modified_date"=>$created_date
		);
        $insertqry="insert into player_images(player_id, customer_id,season_id,team_id,profile_imgs,created_date,modified_date)values(:PlayerID, :cid, :SeasonIds, :TeamId, :FileName, :created_date, :modified_date)";
        $prepinsertqry=$conn->prepare($insertqry);
        $insertRes=$prepinsertqry->execute($insert_results);
        $insertplayer = "INSERT INTO player_images(player_id, customer_id,season_id,team_id,profile_imgs,created_date,modified_date) VALUES ('$PlayerID','$LoginCustId','$SeasonIds','$TeamId','$FileName','$created_date','$created_date')";
        $FileName = $newFilePath='';
        $Inc++;
    }
    
    header("Location:player_list.php?msg=4");
    exit;
}

// Error msg Start Here
if(isset($_GET["msg"])){
  $msg            =   $_GET["msg"];
} else {
    $msg          =  "";
}
if($msg==1){
    $message    =   "Player has been added successfully.";
}
elseif($msg==2){
    $message    =   "Player has been updated successfully.";
}
elseif($msg==3){
    $message    =   "Player has been deleted successfully.";
}
elseif($msg==4){
    $message    =   "Player season image has been added successfully.";
}

$hdn_team_id = "";
$hdn_season_id = "";
$hnd_status = "";
$status = "";
$status1 = "";

if(isset($_POST["hnd_team_id"])){
    $hdn_team_id = $_POST["hnd_team_id"];
}

if(isset($_POST["hnd_season_id"])){
    $hdn_season_id = $_POST["hnd_season_id"];
}

if(isset($_POST["hnd_status"])){
    $hnd_status = $_POST["hnd_status"];

} else {
    $status = "and player_info.isActive='1'";
    $status1 = "and isActive='1'";
}

$hdn_status = "";
if (isset($_POST['hnd_status'])) {

    $hdn_status = $_POST["hnd_status"]?$_POST['hnd_status']:"active";
    
    if($hdn_status=="active"){
        $status= "and player_info.isActive=1";
        $status1= "and isActive=1";
    } if($hdn_status=="Inactive" ) {
        $status= "and player_info.isActive=0";
        $status1= "and isActive='0'";
    }    
}

$team_manager_id = "";
if ($signin == "team_manager") {
    $team_manager_id = " and team_id='$teamlogin_id'";
} 

if ($_SESSION['logincheck'] == 'children' ) {
    $SelCustId = $LoginCustId;
} elseif ($signin == "team_manager") {
    $SelCustId = $LoginCustId;        
} else {
    $SelCustId = $customerid;
}

$team_id = "";
$season_id = "";
$player_name = "";

if (isset($_POST['team']) || isset($_POST['hnd_team_id']) ) {
   
   $team_id = ( isset($_POST['hnd_team_id']) )?$_POST['hnd_team_id']:$_POST['team'];
   $season_id = ( isset($_POST["hnd_season_id"]) )?$_POST['hnd_season_id']:"";
   $player_name = ( isset($_POST["hdn_player_name"]) )?$_POST['hdn_player_name']:"";


    // if ($_SESSION['logincheck'] == 'master' || $_SESSION['logincheck'] == 'children') { 

        $searchPlayerCondtn = !empty($player_name) ? " AND (lastname like '%$player_name%' OR firstname like '%$player_name%')" : "" ;

        // $DbQryCondtn = "";
        // if ($team_id != "all" && !empty($team_id)) 
        //     $DbQryCondtn = " and  team_id='$team_id'";
       
        // $DbQry1 = "select * from player_info where customer_id in ($SelCustId) $DbQryCondtn and lastname!='TEAM' $status1  and (sport_id='$sportid') $team_manager_id $searchPlayerCondtn order by lastname ";

    // }  else {
        
        $searchPlayerInfoCondtn = !empty($player_name) ? " AND (lastname like '%$player_name%' OR firstname like '%$player_name%')" : "" ;
        $HiddenCondtn = "";
         $searchPlayerStatsCondtn = "";
        if (!empty($team_id) && !empty($season_id)) {
            
            $HiddenCondtn = " and $table.season = '$season_id'";
            if ($team_id != "all")
                $HiddenCondtn = " and $table.season = '$season_id' and player_info.team_id='$team_id'"; 

            $searchPlayerStatsCondtn = !empty($player_name) ? " AND (player_info.lastname like '%$player_name%' OR player_info.firstname like '%$player_name%')" : "" ;
            $DbQry1 = "SELECT * FROM player_info JOIN $table ON $table.playercode = player_info.id WHERE $table.customer_id in ($SelCustId) and player_info.lastname!='TEAM' $status $HiddenCondtn $searchPlayerStatsCondtn  order by $table.playercode";
            
        } else if (empty($team_id) && !empty($season_id)) {
            
            $HiddenCondtn = " and $table.season = '$season_id'";
            $searchPlayerStatsCondtn = !empty($player_name) ? " AND (player_info.lastname like '%$player_name%' OR player_info.firstname like '%$player_name%')" : "" ;
            $DbQry1 = "SELECT * FROM player_info JOIN $table ON $table.playercode = player_info.id WHERE $table.customer_id in ($SelCustId) and player_info.lastname!='TEAM' $status $HiddenCondtn $searchPlayerStatsCondtn  order by $table.playercode";
        
        } else if (!empty($team_id) && empty($season_id)) {
            
                $HiddenCondtn = "";
                if ($team_id != "all") {
                    $HiddenCondtn = " and team_id='$team_id'";
                }

                $DbQry1 = "select * from player_info where customer_id in ($SelCustId)  $HiddenCondtn $team_manager_id and lastname!='TEAM' $status and (sport_id='$sportid') $searchPlayerInfoCondtn order by lastname"; 
            
        } else {
             
            $DbQry1 = "select * from player_info where customer_id in ($SelCustId) and lastname!='TEAM' $status1 and (sport_id='$sportid') $searchPlayerInfoCondtn $team_manager_id order by lastname ";
        }        
    // }    
} else {    

    $DbQry1 = "select * from player_info where customer_id in ($SelCustId) and lastname!='TEAM' $status1 and (sport_id='$sportid')  $team_manager_id  order by id ";
    
}

echo $DbQry1;

/****Paging ***/
$Page=1;$RecordsPerPage=25;
if(isset($_REQUEST['HdnPage']) && is_numeric($_REQUEST['HdnPage']))
    $Page=$_REQUEST['HdnPage'];
/*End of paging*/

include_once('header.php'); ?>
<link href="assets/custom/css/playerlist.css" rel="stylesheet" type="text/css" />

<div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            <div class="row">
                
                <div class="col-md-12">
                    <form action="" method="post" id="searchplayedform">
                        <input type="hidden" name="customerid" id="customerid" value="<?php echo ($_SESSION['logincheck'] == 'master') ? $customerid : $LoginCustId ?>">
                        <input type="hidden" name="sportid" id="sportid" value="<?php echo $sportid ?>">
                        
                    
                        <div class="portlet light col-md-12 col-sm-12 col-xs-12" style="padding: 20px 15px 0px;background: #FFF;border: 1px solid #CCC;-moz-border-radius: 5px;-webkit-border-radius: 5px;border-radius: 5px !Important;    float: left;margin-bottom:10px;">
                            <div class="portlet-title " style="border-bottom: 0px solid #eee;">                            
                                <div class="col-md-12 col-sm-12 col-xs-12 search_mobile_device">
                                    <?php 
                                    $ResClass = "col-md-3 col-sm-3 col-xs-12 col-lg-3";
                                    $SearchArea = "col-md-9 col-sm-9 col-xs-12 col-lg-9";
                                    if ($_SESSION['logincheck'] == 'master' || $_SESSION['logincheck'] == 'children') {
                                        $SelCustId = $customerid;
                                        $SeasonHideShow = $TeamHideShow = $PlayersearchHideShow = $ActveHideShow = "display:block;";                                        
                                    } else {
                                        $SelCustId = $LoginCustId;
                                        $SeasonHideShow = $TeamHideShow = $PlayersearchHideShow = $ActveHideShow = "display:block;";                                        
                                    }
                                    $AddClassSearcPlayer = "";
                                    if ($signin == "team_manager") {

                                        $SelCustId = $LoginCustId;
                                        $SeasonHideShow = "display:none;";
                                        $TeamHideShow = "display:none;";
                                        $PlayersearchHideShow = "display:block;";
                                        $ActveHideShow = "display:block;";                                        
                                        $ResClass = "col-md-6 col-sm-6 col-xs-12 col-lg-6";
                                        $SearchArea = "col-md-6 col-sm-6 col-xs-12 col-lg-6";
                                        $AddClassSearcPlayer = "teamplayersearch";
                                    }
                                    
                                    ?>
                                    <div class="<?php echo $SearchArea; ?>" style="padding: 0px;">
                                        <?php
                                            $seasonres="SELECT season,name from customer_season JOIN $table on customer_season.id=$table.season  where  customer_season.custid in (:cid) and $table.season != '' group by season ORDER BY customer_season.season_order DESC";
                                            $seasonresqry = $conn->prepare($seasonres);
                                            $seasonresqry->execute(array(":cid"=>$LoginCustId));
                                            $getSeasonRes     =   $seasonresqry->fetchAll();
                                            ?>
                                        <div class="<?php echo $ResClass; ?> searchseason" style="<?php echo $SeasonHideShow;?>">
                                            <div class="form-group ">
                                                <div class="form-group">
                                                    <input type="hidden" name="lsid" id="tableid" value="<?php echo $table ?>">

                                                    <select class="form-control player_form border-radius" name="searchbyseason" id="searchbyseasonid" >
                                                        <option value="">Select Season</option>
                                                        <?php foreach($getSeasonRes as $seasonrow) {
                                                        if (!empty($seasonrow['season'])) {?>
                                                        <option value="<?php echo $seasonrow['season'] ?>"><?php echo $seasonrow['name'] ?></option>
                                                        <?php } }?>
                                                    </select>
                                                     <?php if($hdn_season_id!="")
                                                     {$seasonid=$hdn_season_id;} else {$seasonid=$season_id;}?>
                                                    <script>$("#searchbyseasonid").val("<?php echo $seasonid;?>")</script>
                                                </div>
                                            </div>
                                        </div>                                       
                                        
                                        <div class="<?php echo $ResClass; ?> player_team_search" style="padding: 0px;<?php echo $TeamHideShow;?>">
                                            <div class="form-group ">
                                                <div class="form-group">
                                                    <select class="form-control  player_form border-radius" name="team" id="searchbynameid"> 
                                                        <option value="all">All Team</option>
                                                        <?php                                                                                                                   
                                                            $tres="select * from teams_info where customer_id in ($LoginCustId) and (sport_id='$sportid')";
                                                            $treslistsqry = $conn->prepare($tres);
                                                            $treslistsqry->execute(array(":cid"=>$LoginCustId));
                                                            $trowRes     =   $treslistsqry->fetchAll();
                                                            foreach ($trowRes as $trow) {
                                                                if(!empty($trow['team_name'])){
                                                            ?>
                                                            <option value="<?php echo $trow['id']; ?>"><?php echo $trow['team_name']; ?></option>
                                                        <?php } } ?>
                                                    </select>
                                                    <?php if($team_id!="" ){$teamids=$team_id;} else if($hnd_team_id!="") {$teamids=$hnd_team_id;} else {$teamids="all";} ?>
                                                     <script>$("#searchbynameid").val("<?php echo $teamids;?>")</script>
                                                </div>
                                            </div> 
                                        </div> 
                                        <div class="<?php echo $ResClass; ?> removerightpadding search_txt" style="<?php echo $PlayersearchHideShow;?>">
                                            <div class="col-md-12 col-sm-12 col-xs-12 search-name-section <?php echo $AddClassSearcPlayer;?>">
                                                <div class="form-group">
                                                    <input type="text" id="searchbyplayername" class="form-control border-radius" placeholder="Search by player" name="searchbyplayername" value="">
                                                    <?php $player_name = !empty($hdn_player_name) ? $hdn_player_name : $player_name;?>                                                    
                                                    <script>$("#searchbyplayername").val("<?php echo $player_name;?>")</script>
                                                </div>
                                            </div>
                                        </div>                                        
                                        <div class="<?php echo $ResClass; ?> searchteamstatus" style="<?php echo $ActiveHideShow;?>">
                                            <div class="form-group ">
                                                <div class="form-group">
                                                    <select class="form-control  player_form border-radius" name="active_status" id="searchbyteamstatus"> 
                                                        <option value="all">All Player</option>
                                                        <option  value="active">Active</option>
                                                        <option value="Inactive">InActive</option>
                                                    </select>
                                                    <?php if($hdn_status!=""){$active=$hdn_status;} else{$active="active";} ?>
                                                    <script>$("#searchbyteamstatus").val("<?php echo $active;?>")</script>
                                                  
                                                </div>
                                            </div>
                                        </div>
                                    </div>                                
                                    <div class="caption font-red-sunglo col-md-3 col-sm-6 col-xs-12 col-lg-3 search_name" id="search_label">
                                       <input type="button" class="btn searchbtnyellow reset-left1 searchplayercustteam" style="border-radius:5px !important;line-height: 1.5;" value="Search" name="search">
                                       <a href="player_list.php" id="resetbtn" class="btn resetbtnred reset-left" style="margin-left:5px" value="Reset" name="reset">Reset</a>
                                    </div>
                                    <?php //}?>
                                </div>
                            </div>
                        </div>
                    <?php //exit;?>
                    </form>
                 </div><!--Col-md-12 -->
            </div> <!--row -->
            <?php
                if(!empty($message)){
                ?>
                <div class="alert alert-success">
                <a class="close" data-dismiss="alert" href="#">x</a>
                <?php echo $message;?>
                </div>
                <?php
                }
                ?>
            <div class="row">
                <div class="col-md-12">                    
                    <div class="portlet-body customerlist-tbl-pr clearfix" style="clear: both;">
                        <div class="widget-header"> 
                            <h3>
                            <i class="icon-settings font-red-sunglo"></i>
                            LIST OF PLAYER                       
                            </h3>
                            <div class="pull-right add_play">

                            <button type="button" class="player_btn adddarkbtnlist" style="" onclick="document.location='manage_player.php'">Add Player</button>                             
                            <button type="button" class="player_btn adddarkbtnlist " style="text-transform:capitalize;" onclick="document.location='add_bulkentry_player.php'"> Add bulk player</button>
                            <input type="hidden" name="sportname" id="sportname" value="<?php echo $sportname; ?>">
                            <?php if ($sportname == 'basketball') {?>
                            <button type="button" class="player_btn adddarkbtnlist combineplayer"  >Combine Player</button>
                            <?php }?>                                 
                                
                            </div>
                        </div>
                        <div class="portlet-body">
                         <div class="loadingplayersection" style="display:none;">
                            <img src="images/loading-publish.gif" alt="loadingimage" id="loadingimage">
                         </div>
                        <div class="table-responsive" id="sample" >
                            <form id="frm_player_list" name="frm_player_list" method="post" action="" enctype="multipart/form-data">
                            <input type="hidden" name="HdnPage" id="HdnPage" value="<?php echo $Page; ?>">
                            <input type="hidden" name="HdnMode" id="HdnMode" value="<?php echo $Page; ?>">
                            <input type="hidden" name="RecordsPerPage" id="RecordsPerPage" value="<?php echo $RecordsPerPage; ?>">
                            <input type="hidden" name="customerid" id="customerid" value="<?php echo $LoginCustId ?>">
                            <input type="hidden" name="sportid" id="sportid" value="<?php echo $sportid ?>">
                            <input type="hidden" name="hnd_team_id" id="hnd_team_id" value="<?php echo $hdn_team_id ?>">
                            <input type="hidden" name="hnd_season_id" id="hnd_season_id" value="<?php echo $season_id;?>">
                            <input type="hidden" name="hnd_status" id="hnd_status" value="<?php echo $hnd_status;?>">
                            <input type="hidden" name="hdn_player_name" id="hdn_player_name" value="<?php echo $player_name;?>">

                            
                            <table class="table table-striped table-bordered table-hover no-footer dataTable " id="sample_1" sytle="border: 1px solid #CCC;border-collapse: collapse;">
                                <thead>
                                <tr>
                                    <th nowrap class="tbl_center" > Player&nbsp;Id </th>
                                    <th nowrap class="tbl_center" > First&nbsp;Name </th>
                                    <th nowrap class="tbl_center" > Last&nbsp;Name </th>
                                    <th nowrap class="tbl_center" > Uniform&nbsp;No </th>
                                    <th nowrap class="tbl_center" > Position </th>
                                    <th nowrap class="tbl_center" > Team&nbsp;Name </th>
                                    <th nowrap class="tbl_center" > Player&nbsp;Image </th>
                                    <th nowrap class="tbl_center" > Player&nbsp;Image&nbsp;by&nbsp;Season</th>
                                    <th nowrap class="tbl_center" > isActive </th>
                                    <th nowrap class="tbl_center" > Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        $dbQry = $DbQry1; 
                                        $getResQry      =   $conn->prepare($dbQry);
                                        $getResQry->execute();
                                        $getResCnt      =   $getResQry->rowCount();
                                        if($getResCnt>0){
                                            $TotalPages=ceil($getResCnt/$RecordsPerPage);
                                            $Start=($Page-1)*$RecordsPerPage;
                                            $sno=$Start+1;
                                                
                                            $dbQry.=" limit $Start,$RecordsPerPage";
                                                    
                                            $getResQry      =   $conn->prepare($dbQry);
                                            $getResQry->execute();
                                            $getResCnt      =   $getResQry->rowCount();
                                        if($getResCnt>0){
                                            $getResRows     =   $getResQry->fetchAll();
                                            $s=1;
                                                foreach($getResRows as $player_info){
                                                    $player_no  =   $player_info["id"];
                                                    $first_name =   $player_info["firstname"];
                                                    $last_name  =   $player_info["lastname"];
                                                    $uniform_no =   $player_info["uniform_no"];
                                                    $position   =   $player_info["position"];
                                                    $home_town  =   $player_info["hometown"];
                                                    $image      =   $player_info["image"]; 
                                                    if($image!=""){
                                                       $img="uploads/players/thumb/".$image;
                                                        $img1="uploads/players/".$image;
                                                    } else {
                                                       $img="uploads/players/thumb/download.jpg";
                                                        $img1="";
                                                    }
                                                    $player_note      =   $player_info["player_note"];
                                                    $teamid=$player_info['team_id'];
                                                    $res2="select * from teams_info where id='$teamid' and (sport_id='$sportid')";
                                                    $getResQry2      =   $conn->prepare($res2);
                                                    $getResQry2->execute();
                                                    $getResCnt2      =   $getResQry2->rowCount();
                                                    $getResRow     =   $getResQry2->fetch();
                                                    $teamname=$getResRow ['team_name'];    
                                    ?>
                                    <tr class="odd gradeX">
                                        <td nowrap class="text-center"><?php echo $player_no; ?></td>
                                        <td nowrap class="tbl_center_td"><a href="manage_player.php?p_id=<?php echo base64_encode($player_no);?>" ><?php echo $first_name; ?></a></td>
                                        <td nowrap class="tbl_center_td"><?php echo $last_name; ?></td>
                                        <td nowrap class="tbl_center_td"><?php echo $uniform_no; ?></td>
                                        <td nowrap class="tbl_center_td"><?php echo $position; ?></td>
                                        <td nowrap class="tbl_center_td"><?php echo $teamname; ?> </td>
                                        <td nowrap class="tbl_center_td">
                                            <?php if($img1!=""){ ?>
                                            <a class="btn_round_green  btn-circle btn-icon-only tooltips btn_round_green" style="line-height: 1.00;"  data-toggle="modal" data-id="<?php echo $player_no; ?>" href="#small<?php echo $player_no; ?>"><i class="fa fa-photo"></i></a>
                                            <div class="modal fade bs-modal-sm" id="small<?php echo $player_no; ?>" tabindex="-1" role="dialog" aria-hidden="true">
                                                <div class="modal-dialog modal-sm">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                            <h4 class="modal-title">Player Image</h4>
                                                        </div>
                                                        <div class="modal-body" style="padding-left: 0px;padding-right: 0px;"> <img width="150" height="150" alt="" src="<?php echo $img1; ?>"</div>
                                                        <div class="modal-footer" style="margin-top:10px;">
                                                            <button type="button" class="btn red btn-danger" data-dismiss="modal">Close</button>
                                                        </div>
                                                    </div>
                                                    <!-- /.modal-content -->
                                                </div>
                                                <!-- /.modal-dialog -->
                                            </div>
                                            <!-- /.modal -->
                                            <?php }?>
                                        </td>
                                        <td nowrap class=""><button type="button" class="btn purple btn-sm btn_purple" data-toggle="modal" data-target="#myModal<?php echo $player_no; ?>">Season Image</button>

                                        <div class="modal fade draggable-modal model-season" id="myModal<?php echo $player_no; ?>" tabindex="-1" role="basic" aria-hidden="true">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                        <h4 class="modal-title">Season Profile Images</h4>
                                                    </div>
                                                    <div class="modal-body"> 
                                                      <form class="profileimgform " method="POST" enctype="multipart/form-data">                 
                                                 <input name="playerid" type="hidden" value="<?php echo $player_no; ?>" class="hidplayerid"/>
                                                    <?php
                                                        $PlayerImgIds = "select * from player_images where player_id=:PlayerId";
                                                        $getPlayerQry    =   $conn->prepare($PlayerImgIds);
                                                        $getPlayerQry->execute(array(":PlayerId"=>$player_no));
                                                        $ProfileCnt      =   $getPlayerQry->rowCount();
                                            
                                                    ?>
                                                    <div class="col-md-12 playerimgcont">
                                                        <div class="alert" id="deletestatus"></div>
                                                        <div class="playerimagetable">
                                                        <table class="" style="border: 1px solid #ccc;width:100%;">
                                                        <tr><th class="imghead">Action</th><th class="imghead">Season</th><th class="imghead">Profile Image</th><?php if ($ProfileCnt){ echo '<th class="imghead">Remove</th>';}?></tr>
                                                        <?php                                               
                                                        $SeasonIdsArr = array();
                                                         if ($ProfileCnt){
                                                            $getPlayersRows     =   $getPlayerQry->fetchAll();
                                                                foreach($getPlayersRows as $ImgList){
                                                                    $ProfileImgId= $ImgList['player_img_id'];
                                                                    $SeasonIdArr  = array_filter(explode("##",$ImgList['season_id']));
                                                                    $SeasonIdsArr  = array_merge($SeasonIdsArr,$SeasonIdArr);

                                                                    $SeasonId    = implode(",",$SeasonIdArr);
                                                                    
                                                                    $SelectedSeason = '<select class="profileimgsedit" multiple="multiple">';                                   
                                                                     $sealist="select * from customer_season where custid in (:cid)";
                                                                     $getseaQry      =   $conn->prepare($sealist);
                                                                     $getseaQry->execute(array(":cid"=>$LoginCustId));
                                                                     $getseaCount      =   $getseaQry->rowCount();
                                                                     $getseaRows     =   $getseaQry->fetchAll();
                                                                    foreach($getseaRows as $sowlist){
                                                                        $Selected  = (in_array($sowlist["id"],$SeasonIdArr))?'selected':'';
                                                                        
                                                                        $SelectedSeason .='<option value="'.$sowlist["id"].'" '.$Selected.'>'.$sowlist["name"].'</option>';
                                                                     } 
                                                                    $SelectedSeason .='</select>';  

                                                                    $ProfileImg  = $ImgList['profile_imgs'];
                                                                    $ProfImgHtml = '';

                                                                    if($ProfileImg!="" && is_file("uploads/players/".$ProfileImg)){ 
                                                                        $ProfImgHtml ="<img src='uploads/players/$ProfileImg' alt='' width='40' height='40'/>";
                                                                    } 
                                                                    $Seasons     = "select name from customer_season where id in ($SeasonId)";
                                                                     $getSeasonsQry      =   $conn->prepare($Seasons);
                                                                     $getSeasonsQry->execute();
                                                                     $getSeasonsCount      =   $getSeasonsQry->rowCount();
                                                                    echo '<tr><td style="vertical-align:middle;"><span date-imgid='.$ProfileImgId.' class="editplayimg">Edit</span><p class="loading_img"><img src="images/loading-publish.gif"></p></td><td class="text-center seasonsnames profileimgswrap">';
                                                                     $getSeasonsRows     =   $getSeasonsQry->fetchAll();
                                                                    foreach( $getSeasonsRows as $SeasonsList ){                      
                                                                        echo '<p style="margin-top:0px">'.$SeasonsList['name'].'</p>';                             
                                                                    }
                                                                    echo $SelectedSeason;
                                                                    echo '</td>';
                                                                    echo "<td class='text-center profileimgdips'>$ProfImgHtml</td>";
                                                                    echo "<td class='text-center deleteimgicons'><img src='images/delete.png' class='imguploadicons deleteprofileimg' date-imgid='$ProfileImgId'></td></tr>";
                                                             }  
                                                         }else{
                                                            echo '<tr><td colspan="3" class="imgslistbody">Season image not uploaded</td></tr>';
                                                         }?>        
                                                         </table>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12 col-sm-12 col-xs-12 playerimguploadwrap imguploadcont"> 
                                                        <h4 class='addnewheading text-left' style="color: #000;">Add image to season</h4>
                                                        <div class="col-md-5 col-sm-5 col-xs-12">                        
                                                            <div class="col-md-12 col-sm-12 col-xs-12 profileimgswrap bottomadddmore" >    
                                                                              
                                                                <select name="seasons[0][]" class="profileimgs" multiple="multiple">
                                                                    <?php
                                                                    $sealistQry="select * from customer_season where custid in (:cid)";
                                                                    $getsealistResQry      =   $conn->prepare($sealistQry);
                                                                    $getsealistResQry->execute(array(":cid"=>$LoginCustId));
                                                                    $getRowCount      =   $getsealistResQry->rowCount();
                                                                    $getseasonListRows     =   $getsealistResQry->fetchAll();
                                                                    foreach ($getseasonListRows as $sowlist) {
                                                                    $SelDsbld   = (in_array($sowlist["id"],$SeasonIdsArr))?" disabled":"";
                                                                        echo '<option value="'.$sowlist["id"].'" '.$SelDsbld.'>'.$sowlist["name"].'</option>';

                                                                    } ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-5 col-sm-5 col-xs-12 imgsuploaderfile">    <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                                                <input type="file" name="upload[]"  multiple="multiple" class="file-id">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2 col-sm-2 col-xs-12 imgsuploader">      
                                                            <div class="imgiconcont">
                                                                <img src="images/addmoreimg.png" class='imguploadicons addplusicon'>
                                                            </div>
                                                        </div>                                          
                                                    </div>
                                                    <div id="moreuploadwrap">
                                                    </div>
                                                    <div class="col-md-12 col-sm-12 col-xs-12" style='margin-top:10px;padding-bottom: 25px;'>
                                                        <input class="btn btn-success uploadimgbtn" type="button" name="addsubmit" value="Update/Save" >&nbsp;&nbsp;<input class="btn btn-danger" type="button" value="Cancel" id="cancelbtn" data-dismiss="modal">
                                                    </div>
                                                    
                                                  </form>



                                                    </div>
                                                    <div class="modal-footer" style="border-top: 0px solid #e5e5e5; ">
                                        
                                                </div> 
                                                <!-- /.modal-content -->
                                            </div>
                                            <!-- /.modal-dialog -->
                                        </div>
                                    </td>
                                    <td nowrap>
                                        <?php 
                                        $isactive=$player_info['isActive'];
                                        if($isactive=="1")
                                        {
                                        $status_val='<img src="./images/active_player.png"  class="tooltips" data-original-title="Click here to Deactive" ></img>';
                                        $change_status='Deactive';
                                        }else
                                        {
                                        $status_val='<img src="./images/deactive_player.png"  class="tooltips" data-original-title="Click here to Active"></img>';
                                        $change_status='Active';
                                        }
                                        ?>
                                        <a href="javascript:void(0);"  id="<?php echo $player_info['id']?>" rel="<?php echo $change_status;?>"   class="status_change tooltips"  ><?php echo $status_val;?></a>
                                    </td>
                                    <td nowrap class="tbl_center_td">
                                      
                                            <a class="roundbtnreddelete  btn-circle btn-icon-only red tooltips " style="line-height: 1.00;" type="button " onclick="return confirmation('<?php echo base64_encode($player_no); ?>','<?php echo $sportname; ?>');" style="cursor:pointer;" data-original-title="Delete Player" /><i class="icon-trash trash_btn" ></i></a>
                                    </td>

                                    </tr>
                                    <?php
                                        $s++;
                                       }
                                    } else{
                                       echo "<td colspan='10' style='text-align:center;'>No Player(s) found.</td>";
                                    }
                                    }
                                    else{
                                        echo "<tr><td colspan='10' style='text-align:center;'>No Player(s) found.</td></tr>";
                                    }
                                    ?>
                                    
                                </tbody>
                            </table>
                            <?php
                                if($TotalPages > 1){

                                echo "<tr><td style='text-align:center;' colspan='12' valign='middle' class='pagination'>";
                                $FormName = "frm_player_list";
                                require_once ("paging.php");
                                echo "</td></tr>";

                                }
                           ?>
                        </div>
                    </div>
                    <!-- </form> -->
                    </div><!--- portlet-body customerlist-tbl-pr clearfix !-->
                </div><!--- col-md-12 !-->
            </div><!--- row !-->
           
            
                            <!-- END SAMPLE TABLE PORTLET-->
            </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT --> 
              
</div>
<!-- END CONTAINER -->

<!-- Modal -->
<div class="modal fade" id="CombineModal" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
      <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">Combine Player</h4>
            </div>
            <div class="modal-body">
                <form name="combineplayer" id="combineplayerfrm" method="POST" class="form-horizontal" novalidate="novalidate" action="">
                    <div class="combinemakecenter">                        
                        <div class="col-md-10 combinemakecenter">
                            <div class="cobmineform">
                                <div class="form-group col-md-12">
                                    <label>Season<span class="error">*</span></label>
                                    <select class="form-control player_form border-radius" name="combineseason" id="combineseason" >
                                        <option value="">Select Season</option>
                                        <?php foreach($getSeasonRes as $seasonrow) {
                                        if (!empty($seasonrow['season'])) {?>
                                        <option value="<?php echo $seasonrow['season'] ?>"><?php echo $seasonrow['name'] ?></option>
                                        <?php } }?>
                                    </select>                                     
                                </div>

                                <div class="form-group col-md-12">
                                    <label>Actual Player ID<span class="error">*</span></label>
                                    <input class="form-control requiredcs border-radius" type="text" name="playerid" id="playerid" placeholder="Actual Palyer ID" /> 
                                    <label id="playeriderror" class="error" for="playerid">Please enter actual player id</label>              
                                </div>  
                                <div class="form-group col-md-12">

                                    <label>Duplicate Player ID<span class="error">*</span></label>
                                    <input class="form-control requiredcs border-radius" type="text" name="duplicateplayerid" id="duplicateplayerid" placeholder="Duplicate Palyer ID" />
                                    <label id="duplicateplayeriderror" class="error" for="duplicateplayerid">Please enter duplicate player id</label> 
                                </div>
                            </div>
                            <div class="cobmineloading">
                                <img src="images/loading-publish.gif" alt="loadingimage" id="loadingimage">
                            </div>
                            <div class="cobminestatus">
                                <div class="alert-success combineappend">Player combined and duplicate player deleted successfully</div>
                            </div>
                            <div class="cobminestatusfailure">
                                <div class="alert-danger combineappend">Player not combined</div>
                            </div>
                            <div class="form-group col-md-12 popupbtn">                                     
                                <input class="btn btnpopupgreen addcombinebtn" style="line-height: 1.7;" id="popupcombinebtn" value="Submit" type="button">
                                <button class="btn cancelbtn btnpopupred" type="button" data-dismiss="modal">Cancel</button>
                            </div>  
                        </div>
                    </div>
                </form>
            </div>
            <!-- <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div> -->        
        </div>      
    </div>
</div>

<?php include_once('footer.php'); ?>
<script src="assets/custom/js/playerlist.js" ></script>
<script>
$(document).ready(function () {

    $("#combineplayerfrm").validate({
        rules: {
            combineseason:{
                required:true,
            },
            playerid:{
                required:true,
                number: true
            },
            duplicateplayerid:{
                required:true,
                number: true,
                uniquedivision:true
            },
          
        },
        messages: {
            combineseason:{
                required:"Please select season",
            },
            playerid:{
                required:"Please enter actual player id",
                number:"Please enter actual player id in number"
            },
            duplicateplayerid:{
                required:"Please enter duplicate player id",
                number:"Please enter duplicate player id in number",
                uniquedivision:"Please enter valid player id"
            },
        },

    });
    $('#popupcombinebtn').click( function() {

        $("#combineplayerfrm").valid();

        if ($("#combineplayerfrm").valid()) {

            $( "#CombineModal" ).modal("show");
            
            $(".cobmineloading").show();
            $("#popupcombinebtn, .cobmineform").hide();
            $(".popupbtn").css("padding-left", "0px");
            var post_type = "combineplayer";
            var playerid = $("#playerid").val();
            var duplicateplayerid=$("#duplicateplayerid").val();
            var season = $("#combineseason").val();            
            
            $.ajax({
                url:"update_combine_player_stats.php",  
                method:'POST',
                data:{"post_type": post_type, "playerid": playerid, "duplicateplayerid": duplicateplayerid, "season": season},
                success:function(data) {
                    $(".cobmineloading").hide();
                    var response = $.parseJSON(data);
                    var appendHTML = "";
                    $(".popupbtn").css("padding-left", "15px");
                    if (response["responsestatus"] == "success") {
                        $("#popupcombinebtn, .cobmineform").hide();
                        $(".cancelbtn").css("margin-left", "0px");
                        $(".cobminestatus").show();
                        appendHTML = '<div class="alert-success combineappend">'+response["status"]+'</div>';
                        $(".cobminestatus").empty().append(appendHTML);
                        
                    } else {
                        $("#popupcombinebtn, .cobmineform").hide();
                        $(".cancelbtn").css("margin-left", "0px");
                        $(".cobminestatusfailure").show();
                        appendHTML = '<div class="alert-danger combineappend">'+response["status"]+'</div>';
                        $(".cobminestatusfailure").empty().append(appendHTML);
                    }
                    
                }
            });
        }
    });

}); 

$(document).ready(function() { 
	loading_sorting();
 });

function loading_sorting(){
    
	$('.profileimgs').multiselect({
		includeSelectAllOption: false,
		numberDisplayed: 1,
		nonSelectedText: "Select season",
	});
	 $('.profileimgsedit').multiselect({
		includeSelectAllOption: false,
		numberDisplayed: 1,
		nonSelectedText: "Select season",
	});

	$('#sample_1').DataTable({
		 "retrieve": true,
		"paging": false,
		"bInfo": false,
	   "bFilter":false,
		"bLengthChange":false,
		"bPaginate":false,
		"aaSorting": [[0,'asc']],
		"aoColumnDefs": [ { "bSortable": false, "aTargets": [6,7,8,9] } ], 
	});        
   
}

// var MultiSelectCnt = 1;

$(document).on("click",".searchplayercustteam",function(){
    filter_players_by_team();
});
// $(document).on("click",".searchplayerabateam",function(){
//   filter_aba_players_by_team();
// });
 // function filter_aba_players_by_team() {

 //            var team_manager_id = "<?php echo $team_manager_id; ?>";
 //            var searchbyplayername = "";
 //            if ( $( "#searchbynameid" ).length )
 //                searchbyplayername   = $('#searchbyplayername').val();
 //            var Searchbyname = "";
 //            if ( $( "#searchbynameid" ).length )
 //                var Searchbyname=$( "#searchbynameid" ).val();
 //            var Searchbystatus   = $('#searchbyteamstatus').val();
 //            $("#searchbyteamstatus").val(Searchbystatus);
 //            var CustomerID     = $('#customerid').val();    
 //            var SportID        = $('#sportid').val(); 
 //            var ls             = $("#tableid").val();
 //            var sportname      = $("#sportname").val();
 //            var HdnPage        = $("#HdnPage").val();
 //            var HdnMode        = $("#HdnMode").val();
 //            var RecordsPerPage = $("#RecordsPerPage").val();
 //            $(".loadingplayersection").show();
 //            $("#sample").hide();
 //            $.ajax({
 //                url:"filter_aba_players.php",  
 //                method:'GET',
 //                data:"team_manager_id="+team_manager_id+"&sportid="+SportID+"&searchbyname="+Searchbyname+"&cid="+CustomerID+"&sportname="+sportname+"&HdnPage="+HdnPage+"&HdnMode="+HdnMode+"&PerPage="+RecordsPerPage+"&Searchbystatus="+Searchbystatus+"&searchbyplayername="+searchbyplayername,
 //                success:function(data) { 
 //                    $(".loadingplayersection").hide();
 //                    $("#sample").show();                        
 //                    document.getElementById('sample').innerHTML = data;                   
 //                    $('.table-header').remove();
 //                    loading_sorting();
 //                }
 //              });                
            
 //        }
</script>

