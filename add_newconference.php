<?php
include_once('session_check.php');
include_once('connect.php');
error_reporting(E_ALL);

if(isset($_GET['addconfrencenew']) && !empty($_GET['addconfrencenew'])){	
	$SeasonId    = $_GET['seasonid'];
	$Conference  = $_GET['conference'];
	$Status		 = $_GET['activeconference'];
	$createdate  = date('Y-m-d H:i:s');
	
	$sqlconf="select (MAX(conf_order)) as confvalue from customer_season_conference where season_id='$SeasonId' and customer_id='$MasterCustId' ";
	$conconf=$conn->prepare($sqlconf);
	$conconf->execute();
	$QryconconfRow = $conconf->fetch();
	if($QryconconfRow['confvalue']!=NULL){
		$confmaxval=$QryconconfRow['confvalue']+1;
	}else{
		$confmaxval=1;
	}


	$stmt		 = $conn->prepare("INSERT INTO customer_conference (customer_id, conference_name) 
    VALUES (:customer_id, :conference_name)");
    $stmt->execute(array(':customer_id' => $LoginCustId, ':conference_name' => $Conference));	
	$conferenceId = $conn->lastInsertId();

	$stmt2		 = $conn->prepare("INSERT INTO customer_season_conference (season_id, conference_id, customer_id, status, conf_order,created_date) VALUES (:season_id, :conference_id, :customer_id,:status,:conf_order,:created_date)");

    $stmt2->execute(array(':season_id'=>$SeasonId,':conference_id'=>$conferenceId,':customer_id' => $MasterCustId, ':status' => $Status, 'conf_order'=>$confmaxval,':created_date'=>$createdate));
	
								
	$Qry		= $conn->prepare("select * from customer_season where custid in($customerid) and id=:season_id order by season_order ASC");
	$Qryarr		= array(":season_id"=>$SeasonId);
	$Qry->execute($Qryarr);
	$QryCntSeason = $Qry->rowCount();
	$DivisionWrapHtml='';
	$Inc =0;
	if ($QryCntSeason > 0) {
		while ($row = $Qry->fetch(PDO::FETCH_ASSOC)){

			$QryExe = $conn->prepare("select * from customer_season_conference as seasonconf LEFT JOIN customer_conference as custconf ON  seasonconf.conference_id=custconf.id where season_id=:season_id order by  seasonconf.conf_order ASC");
			$Qryarr = array(":season_id"=>$row['id']);
			$QryExe->execute($Qryarr);
			$QryCntSeasonconf	= $QryExe->rowCount();
			$Conferencetbl		= '';
			$SeletedArrConf		= array();
			$SeletedArrDiv		= array();

			if ($QryCntSeasonconf > 0) {
				while ($rowSeason = $QryExe->fetch(PDO::FETCH_ASSOC)){												
					$QryExeDiv = $conn->prepare("select * from customer_conference_division as seasonconfdiv LEFT JOIN customer_division as custconf ON  seasonconfdiv.division_id=custconf.id where seasonconfdiv.conference_id=:conference_id and season_id=:season_id order by seasonconfdiv.div_order ASC");
					$QryarrCon = array(":conference_id"=>$rowSeason['conference_id'],":season_id"=>$row['id']);
					$QryExeDiv->execute($QryarrCon);
					$QryCntSeasonconf = $QryExeDiv->rowCount();
					$Divisiontbl ='';
					while ($rowSeasonDiv = $QryExeDiv->fetch(PDO::FETCH_ASSOC)){													
						$Selected = ($rowSeasonDiv['status'])?'checked':'';
						$Divisiontbl .= "<div class='divisions' data-divid='".$rowSeasonDiv['id']."'><table class='table innerdivtable'><tr><td class='divisionbtns'><span class='divisioncircle circle'>D</span><label class='mt-checkbox'><input type='checkbox' name='division[]' class='division-checked'  value='".$rowSeasonDiv['id']."' $Selected> ".$rowSeasonDiv['name']."<span></span></label><a class='btn btn-circle btn-icon-only btn-default red deletebtndiv tooltips' href='javascript:;' data-container='body' data-placement='top' data-original-title='Delete division' data-divisionid='".$rowSeasonDiv['id']."' data-conferenceid='".$rowSeason['id']."' data-seasionid='".$row['id']."' ><i class='icon-trash'></i></a><a href='add_divisionteam.php?divisionid=".$rowSeasonDiv['id']."&conferenceid=".$rowSeason['id']."&seasonid=".$row['id']."' data-conferencename='".$rowSeason['conference_name']."' class='btn btn-circle btn-icon-only btn-default green adddivisionbtn tooltips' data-container='body' data-placement='top' data-original-title='Add Team'><i class='fa fa-plus'></i></a></td></tr></table></div>";
						
						$SeletedArrDiv[] = $rowSeasonDiv['id'];

					}				


					$Selected = ($rowSeason['status'])?'checked':'';
					$Conferencetbl .= "<div class='conferencetbltoggle' data-conf='".$rowSeason['id']."'><table class='table innertable' id='innertblid".$rowSeason['id']."'><tr><td class='conferencebtns '><span class='conferencecircle circle'>C</span><label class='mt-checkbox '><input type='checkbox' class='conference-checked' name='' value='".$rowSeason['id']."' $Selected> ".$rowSeason['conference_name']."<span></span></label><a class='btn btn-circle btn-icon-only btn-default red deletebtnconf tooltips' href='javascript:;' data-container='body' data-placement='top' data-original-title='Delete conference' data-conferenceid='".$rowSeason['id']."' data-seasonid='".$row['id']."'><i class='icon-trash'></i></a><a class='btn btn-circle btn-icon-only btn-default blue managedivisionbtn tooltips' href='javascript:;' data-container='body' data-placement='top' data-original-title='Manage division'  data-conferenceid='".$rowSeason['id']."' data-seasonid='".$row['id']."'><i class='icon-wrench'></i></a><a class='btn btn-circle btn-icon-only btn-default green adddivisionbtn tooltips' href='javascript:;' data-container='body' data-placement='top' data-original-title='Add division' data-toggle='modal' data-target='#DivisionModal' data-conferenceid='".$rowSeason['id']."' data-seasonid='".$row['id']."' data-conferencename='".$rowSeason['conference_name']."'><i class='fa fa-plus'></i></a></td></tr></table><div class='divisiontbltoggle'>".$Divisiontbl."</div></div>";

					$SeletedArrConf[] = $rowSeason['id'];
					$SeletedArrDiv		= array();
				}										
				
			}
			
			$SeasonTree = "<table class='table  ms_seasontble' data-seasion='".$row['id']."'><tr><td><span class='seasoncircle circle'>S</span><label class='mt-checkbox'><input type='checkbox' name='' value='".$row['id']."' checked='checked' disabled> ".$row['name']."<span></span></label><a class='btn btn-circle btn-icon-only btn-default red deleteseasonbtn tooltips' href='javascript:;' data-container='body' data-placement='top' data-original-title='Delete season' data-seasonid='".$row['id']."'><i class='icon-trash' ></i></a><a class='btn btn-circle btn-icon-only btn-default blue    manageconferencemodel tooltips' href='javascript:;' data-toggle='modal' data-target='#ManageConferenceModal' data-seasonid='".$row['id']."' data-seasonname='".$row['name']."' data-container='body' data-placement='top' data-original-title='Manage conference'><i class='icon-wrench'></i></a><a class='btn btn-circle btn-icon-only btn-default green addconferencebtn tooltips' href='javascript:;' data-toggle='modal' data-target='#ConferenceModal' data-seasonid='".$row['id']."'  data-seasonname='".$row['name']."' data-container='body' data-placement='top' data-original-title='Add conference'><i class='fa fa-plus'></i></a><div class='parent_confernce'>$Conferencetbl</div></td></tr></table>";
			
			
			if($Inc==0){				
				$DispCond    = "";
			}else{				
				$DispCond    = "display:none;";
			}
			echo '<div class="portlet box grey seasontbltogglewrap">
				<div class="portlet-title">
					<div class="caption tools" style="width: 98%;">
						<a href="javascript:;" class="expand" style="color:#000;background-image:none;display: block;width: 100%;"> '.$row['name'].'</a>
					</div>												
					<div class="tools">
						<a href="javascript:;" class="expand" style=""></a>
					</div>
				</div>
				<div class="portlet-body seasontbltoggle" style="'.$DispCond.'">'.$SeasonTree.$DivisionWrapHtml.'
				</div>
			</div>';
			$conference='';
			$Inc++;
		} 									
	}							   
							
}else{
	echo "failure";
}
exit;
?>