<?php
include_once('session_check.php');
include_once("connect.php");
include_once('usertype_check.php');  
include_once('header.php');

if (isset($_SESSION["sportid"])) {

    $sportid = $_SESSION["sportid"];
    $sportname = $_SESSION["sportname"]; 
}

$chk_team_id = "";
$teamlogin_id = '';
if (isset($_SESSION['team_manager_id']) && $_SESSION['team_manager_id']) {
    $teamlogin_id = $_SESSION['team_manager_id'];
    $chk_team_id = " AND id=".$teamlogin_id;
} 

if ($_SESSION['logincheck'] != 'master') {
	$SelectCustomerId = $LoginCustId;
} else {
	$SelectCustomerId = $customerid;
}

$tres = $conn->prepare("select * from teams_info where customer_id in ($SelectCustomerId) and sport_id='$sportid' $chk_team_id");
$tres->execute();
$Cnttres = $tres->rowCount();
$TeamLists ='';

if ($Cnttres > 0) {
	$Fetchtres = $tres->fetchAll(PDO::FETCH_ASSOC);
	foreach ($Fetchtres as $trow) {
		$TeamLists .= '<option value="'.$trow['id'].'">'.$trow['team_name'].'</option>';
	}
}

include('playerposition.php');

?>
<!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER--> 
			<div id="addplayermaincont">

				<div id="addmoreplayercont">
				<?php
					echo $signleplayformentry;
				?>
				</div>

				<!-- <div id="content_main" class="clearfix">
					<h2>Player Information</h2>
				</div> -->
				
				<div class="col-md-12 left-right-padding">
                    <div class="portlet light info-caption">
                        <div class="portlet-title">
                            <div class="caption font-red-sunglo">
                                <i class="icon-settings font-red-sunglo"></i>
                                <span class="caption-subject bold uppercase"> Player Information</span>
                            </div>
                        </div>
                    </div>
                </div>

	            <div class="col-md-12 bulkplayerparent">
					<div id="bultplayentrycont" class="col-md-12 col-sm-12 col-xs-12 bulkplayerupload">	

						<?php
							for($i=1;$i<=1;$i++){			
								echo $signleplayformentry;	
							}
						?>
						
					</div>
					<div class="addmorebtncont">
						<div class="pull-right">
							<button type="button" class="btn btn-danger deleteplayerbtn customredbtn">Remove</button>
							<button type="button" class="btn btn-success addmoreplayerbtn customgreenbtn">Add more</button>
						</div>
					</div>
					<div class="submitbtncont">
						<div class="pull-left">			
							<button type="button" class="btn btn-danger customredbtn" onclick="window.location='player_list.php'">Back</button>
							<button type="button" class="btn btn-success submitallplayerbtn customgreenbtn">Submit</button>
						</div>
					</div>
				</div>

				<div id="myModal" class="modal fade updateplayerform" role="dialog" data-backdrop="static" data-keyboard="false">
				  <div class="modal-dialog">

					<!-- Modal content-->
					<div class="modal-content">
					  <div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Update Player Information</h4>
					  </div>
					  <div class="modal-body" style="text-align:center;">
						<div class="loadingimgcont">
							<img src="images/playerloading.gif" style="margin:auto; width: 80px;" class="loadingimg">
							<div class="alert alert-success" id="updatemsg" style="display: block;">Details updated successfully</div>
						</div>
					  </div>
					  <div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					  </div>
					</div>

				  </div>
				</div>
			</div>
			<!-- end #content -->
		</div>
	</div>
</div>
<link href="assets/custom/css/addbulkentryplayer.css" rel="stylesheet" type="text/css">
<!-- <script src="assets/custom/js/addplayer.js" type="text/javascript"></script> -->
<script src="assets/custom/js/bulkplayerupload.js" type="text/javascript"></script>


<?php include('footer.php');   ?>