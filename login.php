<?php 
session_start();
include_once("connect.php");

if (isset($_POST['email'])) {

    $Email = $_POST['email'];
    $Password = $_POST['password'];
    $LoginQry = $conn->prepare("select * from customer_info where email=:email and password=:password");
    $LoginArr = array(":email"=>$Email, ":password"=>$Password);
    $LoginQry->execute($LoginArr);
    $CntLogin = $LoginQry->rowCount();

    if ($CntLogin > 0) {
        $FetchLoginRow = $LoginQry->fetch(PDO::FETCH_ASSOC);
        $LoginId = $FetchLoginRow['id'];
        $UserType = $FetchLoginRow['user_type'];
        $UserName = $FetchLoginRow['name'];
        $_SESSION['video_child'] = $FetchLoginRow['video_child'];
        $_SESSION['team_id'] = $FetchLoginRow['team_id'];

        $_SESSION['team_manager_id']="";
		$_SESSION['logincheck']="";

        if ($FetchLoginRow['parent']=='yes') {
            $_SESSION['master'] = 1 ;
            $_SESSION['childrens'] = $FetchLoginRow['children'];
			$_SESSION['logincheck'] = 'master';
			$_SESSION['site_url'] = $FetchLoginRow['site_url'];
        } else {
            $_SESSION['master'] = 0;

			if($FetchLoginRow['children']=='yes'){
				$_SESSION['parent'] = $FetchLoginRow['parent'];
				$_SESSION['logincheck'] = 'children';
			}else{
				$_SESSION['logincheck'] = 'customer';
			}

        }
        
        $_SESSION['loginid'] = $LoginId;
        $_SESSION['usertype'] = $UserType;
        $_SESSION['username'] = $UserName;

        if(!empty($_POST["remember"])) {
            setcookie ("member_email",$_POST["email"],time()+ (10 * 365 * 24 * 60 * 60));
            setcookie ("member_password",$_POST["password"],time()+ (10 * 365 * 24 * 60 * 60));
        } else {
            if(isset($_COOKIE["member_email"])) {
                setcookie ("member_email","");
            }
            if(isset($_COOKIE["member_password"])) {
                setcookie ("member_password","");
            }
        }

        $Sports = array();
        $SportListArr = array();
        $SportsLists = $conn->prepare("select * from customer_subscribed_sports where customer_id=:customer_id");
        $SportListArr = array(":customer_id"=>$LoginId);
        $SportsLists->execute($SportListArr);
        $CntSportsLists = $SportsLists->rowCount();
        if ($CntSportsLists == 1) {            
            $SporstRow = $SportsLists->fetch(PDO::FETCH_ASSOC);
            $SportidRow = $SporstRow['sport_id'];
            if ($SportidRow == "4444") 
                $Sname = "basketball";
            if ($SportidRow == "4443") 
                $Sname = "football";
            if ($SportidRow == "4441") 
                $Sname = "baseball";
            if ($SportidRow == "4442") 
                $Sname = "softball";

            $_SESSION['sportname'] = $Sname;
            $_SESSION['sportid'] = $SportidRow;
			$SportId= $_SESSION['sportid'];
        }        

        header('Location:index.php?msg=1');
        exit;

    } else {
        header('Location:login.php?msg=2');
        exit;
    }
}

if (isset($_POST['forgetsubmit'])) {

    $Email = $_POST['forgetemail'];
    $LoginQry = $conn->prepare("select * from customer_info where email=:email or contact_email=:email");
    $LoginArr = array(":email"=>$Email);
    $LoginQry->execute($LoginArr);
    $CntLogin = $LoginQry->rowCount();

    if ($CntLogin > 0) {
        $FetchLoginRow = $LoginQry->fetch(PDO::FETCH_ASSOC); 
        $UserName = $FetchLoginRow['name'];
        $Password = $FetchLoginRow['password'];
        $Email = $FetchLoginRow['email'];

        $Headers  = 'MIME-Version: 1.0' . "\r\n";
        $Headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
        $Headers .= 'From: support@805stats.com';
        $Messages = "Password for your account is: ".$Password;
        mail($Email,'Password', $Messages, $Headers);
        header('Location:login.php?msg=3');
        exit;
    } else {
        header('Location:login.php?msg=4');
        exit;
    }
}

$AlertMessage = $ForgetAlertMessage = '';
$AlertClass = $ForgetAlertClass = '';
$AlertFlag = $ForgetAlertFlag = false;
if (isset($_GET['msg'])) { 

    if ($_GET['msg'] == 2) {
        $AlertMessage = "Invalid Email and Password!!";
        $AlertClass = "alert-danger";
        $AlertFlag = true;
    } elseif ($_GET['msg'] == 3) {
        $AlertMessage = $ForgetAlertMessage = "Password sent to registered Email";
        $AlertClass = $ForgetAlertClass = "alert-success";
        $ForgetAlertFlag = $AlertFlag = true;
    } elseif ($_GET['msg'] == 4) {
        $AlertMessage = $ForgetAlertMessage = "Invalid Email Entered!!";
        $AlertClass = $ForgetAlertClass = "alert-danger";
        $ForgetAlertFlag = $AlertFlag = true;
    }
}
?>


<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
<link href="assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
<link href="assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
<link href="assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN THEME GLOBAL STYLES -->
<link href="assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
<link href="assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
<!-- END THEME GLOBAL STYLES -->
<!-- BEGIN PAGE LEVEL STYLES -->
<link href="assets/pages/css/login.min.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL STYLES -->

<!-- BEGIN PAGE LEVEL CUSTOM STYLES -->
<link href="assets/custom/css/signin.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL CUSTOM STYLES -->

<link rel="shortcut icon" href="favicon.ico" /> </head>

<body class="login">
    <!-- BEGIN LOGO -->
    <div class="logo">
        <a href="index.php">
            <img src="assets/pages/img/logo.png" alt="805stats-logo" class=""/> </a>
    </div>
    <!-- END LOGO -->
    <!-- BEGIN LOGIN -->
    <div class="content loginbox">
        <!-- BEGIN LOGIN FORM -->
        <form class="login-form" method="POST">
            <h3 class="form-title font-green">Sign In</h3>
            <?php if ($AlertFlag == true) { ?>
            <div class="alert alert-block fade in <?php echo $AlertClass; ?>">
                <button type="button" class="close" data-dismiss="alert"></button>
                <p> <?php echo $AlertMessage; ?> </p>
            </div>
            <?php } ?>
            <!-- <div class="alert alert-danger display-hide">
                <button class="close" data-close="alert"></button>
                <span> Enter any username and password. </span>
            </div> -->
            <div class="form-group">
                <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                <label class="control-label visible-ie8 visible-ie9">Email</label>
                <input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" placeholder="Email Address" name="email" value="<?php if(isset($_COOKIE["member_email"])) { echo $_COOKIE["member_email"]; } ?>" /> </div>
            <div class="form-group">
                <label class="control-label visible-ie8 visible-ie9">Password</label>
                <input class="form-control form-control-solid placeholder-no-fix"  type="password" autocomplete="off" placeholder="Password" name="password" value="<?php if(isset($_COOKIE["member_password"])) { echo $_COOKIE["member_password"]; } ?>" /> </div>
            <div class="form-actions">
                <button type="submit" class="btn green uppercase" id="login">Login</button>
                <label class="rememberme check mt-checkbox mt-checkbox-outline">
                    <input type="checkbox" name="remember" value="1" <?php if(isset($_COOKIE["remember"])) { ?> checked <?php } ?> />Remember
                    <span></span>
                </label>
                <a href="javascript:;" id="forget-password" class="forget-password">Forgot Password?</a>
            </div>
        </form>
        <!-- END LOGIN FORM -->
        <!-- BEGIN FORGOT PASSWORD FORM -->
        <form class="forget-form" method="POST">
            <h3 class="font-green">Forget Password ?</h3>
            <p> Enter your e-mail address below to reset your password. </p>
            <?php if ($ForgetAlertFlag == true) { ?>
            <div class="alert alert-block fade in <?php echo $ForgetAlertClass; ?>">
                <button type="button" class="close" data-dismiss="alert"></button>
                <p> <?php echo $ForgetAlertMessage; ?> </p>
            </div>
            <?php } ?>
            <div class="form-group">
                <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Email Address" name="forgetemail" /> </div>
            <div class="form-actions">
                <button type="button" id="back-btn" class="btn green btn-outline">BACK</button>
                <button type="submit" class="btn btn-success uppercase pull-right" name="forgetsubmit">Submit</button>
            </div>
        </form>
        <!-- END FORGOT PASSWORD FORM -->        
    </div>
    <div class="copyright"> 2016 &copy; Powered By 805stats.com. All Rights Reserved. </div>

    <!-- BEGIN CORE PLUGINS -->
    <script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>
    <script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
    <script src="assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <script src="assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
    <script src="assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
    <!-- END CORE PLUGINS -->
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
    <script src="assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
    <script src="assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN THEME GLOBAL SCRIPTS -->
    <script src="assets/global/scripts/app.min.js" type="text/javascript"></script>
    <!-- END THEME GLOBAL SCRIPTS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="assets/pages/scripts/login.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL SCRIPTS -->

</body>
<script>
    $(document).ready(function(){
        $(".forget-form").submit(function(){
            $('.forget-form').show();
            $('.login-form').hide();
        });
        $(".login-form").submit(function(){
            $('.forget-form').hide();
            $('.login-form').show();
        });
    });
</script>
