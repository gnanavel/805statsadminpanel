<?php ?>
<!-- Manage conference popup model -->
<div id="ManageConferenceModal" class="modal fade" role="dialog">
  <div class="modal-dialog">							
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal">&times;</button>
		<h4 class="modal-title">Manage Conference</h4>
	  </div>
	  <div class="modal-body">
	  <form name="manageconfform" id="manageconfformid" method="POST" class="form-horizontal" role="form">
		  <div class="row">
			  <div class="col-md-11 btn_managedivision" style="margin:auto;float:none; padding-bottom: 6px;">									  
				  <input type="hidden" name="seasionidconf" value="" id="seasionidconfid">
				  <div id="manageconfformcont" style="padding-bottom: 6px;">
				  </div>				 
				  <input class="btn btn-success manageconfpopbtn" type="button" value="Submit">
				  <button class="btn btn-danger btn_cancel cancelbtn" type="button" data-dismiss="modal">Cancel</button>					  
			  </div>
		  </div>
	  </form>
	  <table width='100%' id="loadingconferenceselected"><tr><td align='center'><img src='assets/custom/imgs/loading.gif' style='margin-right: 10px;width: 75px;'></td><tr><td align='center' style='font-size:15px;color:green;'>Loading conference... Please wait...</td></tr></table>
	   <table width='100%' id="loadingconference"><tr><td align='center'><img src='assets/custom/imgs/loading.gif' style='margin-right: 10px;width: 75px;'></td><tr><td align='center' style='font-size:15px;color:green;'>Saving conference... Please wait...</td></tr></table>

		<table width="100%" id="manageconfmsg"><tbody><tr><td align="center" style="font-size:15px;color:green;">Conference details updated successfully.</td></tr></tbody></table>
	  </div>
	  <div class="modal-footer">
		<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
	  </div>
	</div>
  </div>
</div>
<!-- Manage division popup model -->
<div id="managedivModal" class="modal fade" role="dialog">
  <div class="modal-dialog">							
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal">&times;</button>
		<h4 class="modal-title">Manage Division</h4>
	  </div>
	  <div class="modal-body">
		  <form name="managecdivform" id="managedivformid" method="POST" class="form-horizontal" role="form">
			  <div class="row">
				  <div class="col-md-11 " style="margin:auto;float:none;padding-bottom: 6px;">									  
					  <input type="hidden" name="seasioniddiv" value="" id="seasioniddivid">
					  <input type="hidden" name="seasionidconfdiv" value="" id="seasionidconfdivid">
					  <div id="managedivformcont" style="padding-bottom: 6px;">
					  </div>				 
					  <input class="btn btn-success managedivpopbtn" type="button" value="Submit">
					  <button class="btn btn-danger btn_cancel cancelbtn" type="button" data-dismiss="modal">Cancel</button>					  
				  </div>
			  </div>
		  </form>
		 <table width='100%' id="loadingdivisionselected"><tr><td align='center'><img src='assets/custom/imgs/loading.gif' style='margin-right: 10px;width: 75px;'></td><tr><td align='center' style='font-size:15px;color:green;'>Loading division... Please wait...</td></tr></table>	
		  <table width='100%' id="loadingdivison"><tr><td align='center'><img src='assets/custom/imgs/loading.gif' style='margin-right: 10px;width: 75px;'></td><tr><td align='center' style='font-size:15px;color:green;'>Saving division... Please wait...</td></tr></table>
		  <table width="100%" id="managedivinmsg"><tbody><tr><td align="center" style="font-size:15px;color:green;">Division details updated successfully.</td></tr></tbody></table>
	  </div>
	  <div class="modal-footer">
		<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
	  </div>
	</div>
  </div>
</div>

<!-- Season model popup -->
<div id="seasonModal" class="modal fade" role="dialog">
  <div class="modal-dialog">							
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal">&times;</button>
		<h4 class="modal-title">Add New Season</h4>
	  </div>
	  <div class="modal-body">
			<div class="col-md-11" style='margin:auto;float:none;'>
				   <table width='100%' id="loadingseasonaddform"><tr><td align='center'><img src='assets/custom/imgs/loading.gif' style='margin-right: 10px;width: 75px;'></td><tr><td align='center' style='font-size:15px;color:green;'>Loading season... Please wait...</td></tr></table>
					<div class="row">
						<form name="addseason" id="addseasonfrm" method="POST" class="form-horizontal" novalidate="novalidate">
							<div class="form-group col-md-12 ">
								<label>Season Name <span class="required" aria-required="true"> * </span></label>
								<input class="form-control requiredcs" type="text" name="seasonnamenew" id="seasonnamenew" placeholder="Season Name" />	
							</div>
						</form>
					</div>
				<div id="addseasonformcont" >
				</div>
				<div class="row" style="margin-top: 25px;" id="addseasonbtnshow">
						<input class="btn addnewseasonbtn" type="button" value="Submit">
						<button class="btn btn-danger cancelbtn" type="button" data-dismiss="modal" style="margin-left:15px;">Cancel</button>
				</div>
				<table width='100%' id="loadingseason"><tr><td align='center'><img src='assets/custom/imgs/loading.gif' style='margin-right: 10px;width: 75px;'></td><tr><td align='center' style='font-size:15px;color:green;'>Saving season... Please wait...</td></tr></table>
				<table width="100%" id="seasonstsnmsg"><tbody><tr><td align="center" style="font-size:15px;color:green;">Season details updated successfully.</td></tr></tbody></table>
			</div>
	  </div>
	  <div class="modal-footer">
		<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
	  </div>
	</div>
  </div>
</div>

<!-- Conference model popup -->
<div id="ConferenceModal" class="modal fade" role="dialog">
  <div class="modal-dialog">							
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal">&times;</button>
		<h4 class="modal-title">Add New Conference   <b class="season-name-conference">::</b> <span id="season-name-conference" class="season-name-conference"></span> </h4>
	  </div>
	  <div class="modal-body">
	  <form name="addconference" id="conferencefrm" method="POST" class="form-horizontal" novalidate="novalidate">
		  <input type="hidden" name="addconfrencenew" value="addconfrencenew"/> 
		  <input type="hidden" name="seasonid" id="seasonid" /> 
		<div class="col-md-10" style='margin:auto;float:none;'>
			<div class="form-group col-md-12 ">
				<label>Conference Name <span class="required" aria-required="true"> * </span></label>
				<input class="form-control requiredcs border-radius" type="text" name="conference" id="conferencename" placeholder="Conference Name" /> 	
				
			</div>	
			<div class="form-group col-md-12 ">
				<label>Status</label>
				<div class="form-group col-md-12 btn_managesea">
					<label class="mt-radio status_radio">
						<input type="radio" name="activeconference" value="1" checked="checked"> Active
						<span></span>
					</label>
					<label class="mt-radio status_radio">
						<input type="radio" name="activeconference" value="0" > Inactive
						<span></span>
					</label>
				</div>
			</div>	
			<div class="form-group col-md-12 btn_managesea">										
				<input class="btn btn-success addnewconferencebtn" type="button" value="Submit">
				<button class="btn btn-danger cancelbtn" type="button" data-dismiss="modal">Cancel</button>
			</div>	
		</div>
		</form>
		<table width='100%' id="loadingaddconfer"><tr><td align='center'><img src='assets/custom/imgs/loading.gif' style='margin-right: 10px;width: 75px;'></td><tr><td align='center' style='font-size:15px;color:green;'>Saving conference... Please wait...</td></tr></table>

		<table width="100%" id="conferencestsmsg" style="display: none;"><tbody><tr><td align="center" style="font-size:15px;color:green;">Conference details saved successfully.</td></tr></tbody></table>

	  </div>
	  <div class="modal-footer">
		<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
	  </div>
	</div>
  </div>
</div>						

<!-- Division model popup -->
<div id="DivisionModal" class="modal fade" role="dialog">
  <div class="modal-dialog">							
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal">&times;</button>
		<h4 class="modal-title">Add New Division   <b class="conference-name-division">::</b> <span id="conference-name-division" class="conference-name-division"></span></h4>
	  </div>
	  <div class="modal-body">
	  <form name="adddivision" id="divisionfrm" method="POST" class="form-horizontal" novalidate="novalidate">
		  <input type="hidden" name="adddivisionnew" value="adddivision11"/> 
		  <input type="hidden" name="conferenceid" id="conferenceid" /> 
		  <input type="hidden" name="seasonid" id="seasoniddiv" /> 
		<div class="col-md-10" style='margin:auto;float:none;'>
			<div class="form-group col-md-12 ">
				<label>Division Name <span class="required" aria-required="true"> * </span></label>
				<input class="form-control requiredcs border-radius" type="text" name="division" id="divisionname" placeholder="Division Name" /> 				
			</div>	
			<div class="form-group col-md-12 ">
				<label>Division Rule <span class="required" aria-required="true"> * </span></label>
				<select name="divisionrule" class="form-control seldivinrule uniquedivision border-radius " id="divisionrule">
					<option value=''>Select division rule</option>
					<?php								
					$sql		= "select * from customer_division_rule";			
					$stmt = $conn->query($sql); 
					$QryCntRule = $stmt->rowCount();
					if ($QryCntRule > 0) {
						while ($QryCntRuleRow = $stmt->fetch(PDO::FETCH_ASSOC)){
							echo "<option value='".$QryCntRuleRow['id']."'>".$QryCntRuleRow['name']."</option>";	
						}
					}
					?>
				</select>
			</div>	
			<div class="form-group col-md-12 ">
				<label>Status</label>
				<div class="form-group col-md-12 btn_managesea">
					<label class="mt-radio status_radio">
					<input type="radio" name="activedivision" class= "activedivision" value="1" checked="checked" > Active
						<span></span>
					</label>
					<label class="mt-radio status_radio">
					<input type="radio" name="activedivision" class= "inactivedivision" value="0"> Inactive
						<span></span>
					</label>
				</div>
			</div>	
			<div class="form-group col-md-12 btn_managesea">										
				<input class="btn btn-success addnewdivibtn" type="button" value="Submit">
				<button class="btn btn-danger cancelbtn" type="button" data-dismiss="modal">Cancel</button>
			</div>	
		</div>
		</form>
		<table width='100%' id="loadingadds"><tr><td align='center'><img src='assets/custom/imgs/loading.gif' style='margin-right: 10px;width: 75px;'></td><tr><td align='center' style='font-size:15px;color:green;'>Saving division... Please wait...</td></tr></table>

		<table width='100%' id="divisionstsmsg"><tr><td align='center' style='font-size:15px;color:green;'>Division details saved successfully.</td></tr></table>
	  </div>
	  <div class="modal-footer">
		<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
	  </div>
	</div>
  </div>
</div>