<?php
include_once('session_check.php');
include_once('connect.php');
include_once('common_functions.php');
include_once('usertype_check.php');

if (isset($_GET['action'])) {

	$pid  =   base64_decode($_GET["pid"]);
	$sport_name = $_GET['sportname'];
    if ($sport_name=='softball' || $sport_name=='baseball') {
	    $table="player_stats_ba";
	    $itable="individual_player_stats_ba";
	} 
	if ($sport_name=='basketball') {
	    $table="player_stats_bb";
	    $itable="individual_player_stats";
	}
	if ($sport_name=='football') {
	    $table="player_stats_fb";
	    $itable="individual_player_stats_fb";
	}
        
    // delete from table player_stats_fb,player_stats,player_stats_ba
    $updateQry      = "update $table set customer_id='1', teamcode='1' where playercode=:id";
    $prepupdateQry  = $conn->prepare($updateQry);
    $update_results = array(":id"=>$pid );
    $updateRes      = $prepupdateQry->execute($update_results);

    // delete from itable individual_player_stats_ba, individual_player_stats,individual_player_stats_fb
    $updateindividualStatusQry   = "update $itable set customer_id='1', teamcode='1' where playercode=:id";
    $prepupdateIndividualQry = $conn->prepare($updateindividualStatusQry);
    $update_Individual_results = array(":id"=>$pid );
    $updateResIndividual     = $prepupdateIndividualQry->execute($update_Individual_results);

    // delete from player_info table
    $updateplayer_infoQry     = "update player_info  set customer_id='1', team_id='1' where id=:id";
    $prepupdatePlayerInfoQry = $conn->prepare($updateplayer_infoQry);

    $update_Player_results = array(":id"=>$pid);
    $updateResPlayer       = $prepupdatePlayerInfoQry->execute($update_Player_results);

    // delete from customer_team_player table
    $updateplayer_infoQry     = "update customer_team_player  set customer_id='1', team_id='1' where player_id=:id";
    $prepupdatePlayerInfoQry = $conn->prepare($updateplayer_infoQry);
    $prepupdatePlayerInfoQry->execute($update_Player_results);

    
    if($sport_name!=""){
        header("Location:player_list.php?msg=3");
        exit;
    }
    else{
        header("Location:player_list.php");
        exit;
    }
	
}
?>