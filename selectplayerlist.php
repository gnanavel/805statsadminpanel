<?php
include_once('session_check.php');
include_once('connect.php'); 

$SportId= $_SESSION['sportid'];

if(isset($_POST['teamid'])){
	$SeasonId      = $_POST['seasonid'];
	$divisionid    = $_POST['divisionid'];
	$conferenceid  = $_POST['conferenceid'];
	$teamid		   = $_POST['teamid'];
	$PostType      = $_POST['post_type'];
	
	$QryExeDiv = $conn->prepare("select * from customer_team_player as seasonplayer LEFT JOIN player_info as playertbl ON  seasonplayer.player_id=playertbl.id where seasonplayer.customer_id=$MasterCustId and seasonplayer.conference_id=:conference_id and season_id=:season_id and division_id=:division_id and seasonplayer.team_id=:team_id and seasonplayer.isdelete=0 and playertbl.lastname!='TEAM'");

	$QryarrCon = array(":conference_id"=>$conferenceid,":season_id"=>$SeasonId,":division_id"=>$divisionid,":team_id"=>$teamid);
	$QryExeDiv->execute($QryarrCon);
	$QryCntSeasonconf = $QryExeDiv->rowCount();
	$responseHtml = '';
	$AssignPlayerArr  = array();

	if($PostType=='selectplayertbl'){
		$responseHtml .= "<table class='table assignplayertbl'>";
		if($QryCntSeasonconf>0){
				
			while ($rowPlayer = $QryExeDiv->fetch(PDO::FETCH_ASSOC)){			
				$AssignPlayerArr  = $rowPlayer['id'];
				$InActiveBg='';
				if($rowPlayer['status']==0){							
					$InActiveBg = 'inactiveplayerbg';
				}

				$src =HTTP_ROOT.'/uploads/players/thumb/'.$rowPlayer['player_image'];
				//$src ='/uploads/players/thumb/'.$rowPlayer['player_image'];
				$PlayerImg='';
				if (@getimagesize($src)) {
					$PlayerImg   = "<img src='$src' class='playerimageinlist'>";
				}else{
					$PlayerImg   = "<img src='images/defaultplayer.png' class='playerimageinlist'>";
				}
				$responseHtml .= "<tr class='".$InActiveBg."' id='player_".$rowPlayer['id']."'><td class='playerimgtbl'>".$PlayerImg."</td><td class='playernamevalgin'>".$rowPlayer['firstname']." ".$rowPlayer['lastname']."</td><td><p class='playeractionwrap'><span date-playerid='".$rowPlayer['id']."' teamid='".$teamid."' seasionid='".$SeasonId."' class='removeplayerteam'>Remove</span><span data-playerid='".$rowPlayer['id']."' teamid='".$teamid."' seasionid='".$SeasonId."'  divisionid='".$divisionid."' conferenceid  = '".$conferenceid."' selstatus='".$rowPlayer['status']."' class='updateplayer updatestatus_".$rowPlayer['id']."'>Update</span><span data-playerid='".$rowPlayer['id']."' class='switchteam'  teamid='".$teamid."'>Switch Team</span></p></td></tr>";		
			}				

		}else{
			$responseHtml .= "<tr><td>No players for this team</td></tr>";		
			
		}
		
		$Playeroptions =$AssignedOtions = $SelectedTeamPlayer = $PlayeroptionsNot='';
		$AssigneidArr=array();$Assigneid=array();
		$Assstatus=array();	$AssstatusArr=array();

			
		$CusteamRes="select *, team_id as assigned,SUM(status) as pstatus,id as tmplid from customer_team_player where season_id='$SeasonId' and customer_id=$MasterCustId and  division_id='$divisionid' and conference_id='$conferenceid' and  status='1'  group by id DESC";
		$Qry5		= $conn->prepare($CusteamRes);
		$Qry5->execute();
		$QryCntSeason5 = $Qry5->rowCount();
		if($QryCntSeason5 > 0){
			while ($row5 = $Qry5->fetch(PDO::FETCH_ASSOC)){	
				if($row5['isdelete']=='0'){
					if($row5['assigned']!=$teamid){
						$Assigneid[]=$row5['player_id'];
						$pstatus=$row5['pstatus'];
					}
					if($row5['assigned']==$teamid){
						$Assstatus[]=$row5['player_id'];
					}
				}
				
			}
		}	
		
		$AssigneidArr=implode(",",$Assigneid);
		$AssstatusArr=implode(",",$Assstatus);
		$QryCondn  = ($AssigneidArr!='')?"and id IN (".$AssigneidArr.")":'';
		$AssRes="select id,firstname,lastname from player_info where customer_id in($customerid) and isActive=1 and lastname!='TEAM' $QryCondn group by id DESC";
		$Qry2		= $conn->prepare($AssRes);
		$Qry2->execute();
		$QryCntSeason2 = $Qry2->rowCount();
		if($QryCntSeason2 >0){
		while ($row2 = $Qry2->fetch(PDO::FETCH_ASSOC)){	
			if($AssigneidArr!=''){
				$PlayeroptionsNot .="<option value='".$row2['id']."' disabled>".$row2['firstname']." ".$row2['lastname']."</option>";
			}
		}	
		}else{
				//$Playeroptions .= "<option value=''>No Player found</option>";
		}
		
		$AssQryCondn  = ($AssigneidArr!='')?"and id NOT IN (".$AssigneidArr.")":'';
		$AssoptionRes="select  id,firstname,lastname from player_info where sport_id='$SportId' and customer_id in($customerid) and isActive=1 and lastname!='TEAM' $AssQryCondn group by id DESC order by firstname ASC";
		$Qry3		= $conn->prepare($AssoptionRes);
		$Qry3->execute();
		$QryCntSeason3 = $Qry3->rowCount();
		if($QryCntSeason3 >0){
			while ($row3 = $Qry3->fetch(PDO::FETCH_ASSOC)){	
				$AssignedOtions .= "<option value='".$row3['id']."'>".$row3['firstname']." ".$row3['lastname']."</option>";	
			}
		}

		$StatusQryCondn  = ($AssstatusArr!='')?"and id IN (".$AssstatusArr.")":'';
		$AsstatusRes="select  id,firstname,lastname from player_info where sport_id='$SportId' and customer_id in($customerid) and   isActive=1 and lastname!='TEAM' $StatusQryCondn group by id DESC";
		$Qry4		= $conn->prepare($AsstatusRes);
		$Qry4->execute();
		$QryCntSeason4 = $Qry4->rowCount();
		if($QryCntSeason4 >0){
			while ($row4 = $Qry4->fetch(PDO::FETCH_ASSOC)){	
				$SelectedTeamPlayer .="<option value='".$row4['id']."' selected>".$row4['firstname']." ".$row4['lastname']."</option>";
			}
		}
	
		$responseHtml .= "</table>#####";
		echo $responseHtml;
	}	
	exit;
}
?>