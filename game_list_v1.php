<?php 
include_once('session_check.php'); 
include_once("connect.php");
include_once('common_functions.php');
include_once('usertype_check.php');
error_reporting(E_ALL); 
$team_manager_id = "";
$signin = "";
if (isset($_SESSION['signin'])) {

    $signin = $_SESSION['signin'];
    $teamlogin_id = $_SESSION['team_manager_id'];
    $team_manager_id = " and team_id='$teamlogin_id'";
}

$team_id = '';
if (isset($_SESSION['signin']) && $_SESSION['signin'] == 'team_manager') {
    $team_id = " AND (visitor_team_id=$teamlogin_id OR home_team_id=$teamlogin_id)";
}

if (isset($_SESSION["sportid"])) {
    
    $SportId = $_SESSION["sportid"];
    $SportName = $_SESSION["sportname"];
}

if ($_SESSION['logincheck'] == 'master') {
    // $DefaultSeasonDivQry = $conn->prepare("select distinct(season) from games_info where home_customer_id in ($customerid) order by season desc limit 0, 1");
    $DefaultSeasonDivQry = $conn->prepare("SELECT season,name from customer_season JOIN games_info on customer_season.id=games_info.season  where  customer_season.custid in ($customerid) and games_info.season != '' group by season ORDER BY customer_season.season_order DESC");
} else {
    // $DefaultSeasonDivQry = $conn->prepare("select distinct(season) from games_info where home_customer_id in ($LoginCustId) order by season asc limit 0, 1");
    $DefaultSeasonDivQry = $conn->prepare("SELECT season,name from customer_season JOIN games_info on customer_season.id=games_info.season  where  customer_season.custid in ($LoginCustId) and games_info.season != '' group by season ORDER BY customer_season.season_order DESC");
}

$DefaultSeasonDivQry->execute();
$FetchDefaultSeasonDiv = $DefaultSeasonDivQry->fetch(PDO::FETCH_ASSOC);
$DefaultSeasonDiv =  $FetchDefaultSeasonDiv['season'];
$DefaultSeasonDiv = 41;
$DefaultSeasonDivCondn = "";
if (!empty($DefaultSeasonDiv)) {
    if ($signin != 'team_manager') {
        $DefaultSeasonDivCondn = " and season='$DefaultSeasonDiv'";
    } 
}

$SelectCustId = "";
if ($_SESSION['logincheck'] == 'master') {
    $SelectCustId = $customerid;
} else {
    $SelectCustId = $LoginCustId;
}

$currentMonth = date("F");
$currentYear = date("y");
$currentMonthGame = $currentMonth."-".$currentYear;

$currentMonthCondtn =  "AND date_format(STR_TO_DATE(date, '%m/%d/%Y'),'%M-%y') = '$currentMonthGame'";
$currentMonthCondtn =  "";
$res = "select * from games_info where (home_customer_id in ($SelectCustId) or visitor_customer_id in ($SelectCustId)) $team_id $DefaultSeasonDivCondn  $currentMonthCondtn ORDER BY STR_TO_DATE(date, '%m/%d/%Y'),time";

$ListingGameQry = $conn->prepare($res);
$ListingQryArr = array(":cid"=>$LoginCustId, ":sportid"=>$SportId);
$ListingGameQry->execute($ListingQryArr);
$CntListingGame = $ListingGameQry->rowCount();

// echo $res;
$GameRes = $ListingGameQry->fetchAll(PDO::FETCH_ASSOC);  
$GameDetail = array();
$j =0;
foreach ($GameRes as $GameRow) {
    $GameYear = date("Y", strtotime($GameRow['date']));
    $GameMonth = date("m", strtotime($GameRow['date']));
    $MatchDetail = $GameRow['visitor_team_id']. " vs ".$GameRow['home_team_id'];
    $GameDetail[$GameYear][$GameMonth][$j] = $GameRow;
    $j++;
}

// print_r($GameDetail);
$alert_message = '';
$alert_class = '';
if (isset($_GET['msg'])) { 
    if ($_GET['msg'] == 1) {
        $alert_message = "Added new game successfully";
        $alert_class = "alert-success";
    } else if($_GET['msg'] == 2) {
        $alert_message = "Duplicate game!!";
        $alert_class = "alert-danger";
    } else if($_GET['msg'] == 3) {
        $alert_message = "Game updated successfully";
        $alert_class = "alert-success";
    } else if($_GET['msg'] == 4) {
        $alert_message = "Game deleted successfully";
        $alert_class = "alert-success";
    } else if($_GET['msg'] == 5) {
        $alert_message = "Games data updated successfully!";
        $alert_class = "alert-success";
    }
}

include_once('header.php');
?>
<link href="assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css" rel="stylesheet" type="text/css" />
<link href="assets/custom/css/gamelist.css" rel="stylesheet" type="text/css" />
<link href="assets/custom/css/swiper.css" rel="stylesheet" type="text/css" />
<input type="hidden" name="sportid" id="sportid" value="<?php echo $SportId ?>">
<input type="hidden" name="customerid" id="customerid" value="<?php echo $LoginCustId ?>">
    <div class="page-content-wrapper">
        <div class="page-content gamelisting">

            <?php if (isset($_GET['msg'])) { ?>
            <div class="alert alert-block fade in <?php echo $alert_class; ?>">
                <button type="button" class="close" data-dismiss="alert"></button>
                <p> <?php echo $alert_message; ?> </p>
            </div>
            <?php } 

            if ($signin != 'team_manager') {?>
            <div class="row searchheder">                
                <?php 
                if ($_SESSION['logincheck']!= 'master') {
                    
                    $ParenClass = "col-md-12 col-sm-12 col-xs-12";
                    $SearchBoxClass = "col-md-4 col-sm-4 col-xs-12";
                    $SearchBtnClass = "col-md-3 col-sm-3 col-xs-12";
                    $Inlinestyle = "";
                    $Inlinestyle2 = "padding-left:11px;";
                    $class="searchgamefilter";

                } else {
                    $ParenClass = "col-md-12 col-sm-12 col-xs-12";
                    $SearchBoxClass = "col-md-4 col-sm-4 col-xs-12";
                    $SearchBtnClass = "col-md-3 col-sm-3 col-xs-12";
                    $Inlinestyle = "";
                    $Inlinestyle2 = "padding-left:11px;";
                    $class="searchgameabafilter";
                }
                ?>                
                <div class="<?php echo $ParenClass; ?> searchbarstyle" style="<?php echo $Inlinestyle; ?>">
                    <?php 
                        if ($signin != 'team_manager') {
                            if ($_SESSION['logincheck'] == 'master' || $_SESSION['logincheck']=='children') {
                                $ids = $customerid;
                            } else {
                                $ids = $LoginCustId;
                            }
                        ?>
                        <div class="col-md-4 col-sm-4 col-xs-12 search_season">
                            <div class="form-group ">                                
                                <select class="form-control border-radius" id="searchbyseasonid" >
                                <?php
                                $SeasonRes = $conn->prepare("SELECT season,name from customer_season JOIN games_info on customer_season.id=games_info.season  where  customer_season.custid in ($ids) and games_info.season != '' group by season ORDER BY customer_season.season_order DESC");
                                $SeasonResArr = array(":cid"=>$LoginCustId);
                                $SeasonRes->execute();
                                $SeasonResCnt = $SeasonRes->rowCount();
                                if ($SeasonResCnt > 0) {
                                    $FetchSeason = $SeasonRes->fetchAll(PDO::FETCH_ASSOC);
                                    foreach ($FetchSeason as $SeasonRow) { 
                                        ?>
                                    <option <?php echo ($SeasonRow['season'] == $DefaultSeasonDiv)? "selected":"" ?> value="<?php echo $SeasonRow['season']; ?>"><?php echo $SeasonRow['name'] ?></option>
                                    <?php }
                                } else { ?>
                                    <option value="">Select season</option>
                                <?php }?>
                                </select>
                            </div>
                        </div>
                    <?php }?>
                    <div class="<?php echo $SearchBoxClass; ?> removerightpadding" >
                        <form class="search-form search-form-expanded" >
                            <div class="form-group">                                
                                <input type="text" id="searchtext" class="form-control border-radius" placeholder="Search by team name" name="search">                                
                                
                            </div>
                        </form>
                    </div>
                    <div class="<?php echo $SearchBtnClass; ?> searchrightpadding" style="<?php echo $Inlinestyle2; ?>">
                        <div class="form-group" style="">
                             <input type="button" class="btn searchbtnyellow reset-left1 <?php echo $class;?>" style="border-radius:5px !important;line-height: 1.5;" value="Search" name="search">
                            <input type="button" id="resetbtn" class="btn resetbtnred" style="margin-left:10px;" value="Reset" name="query" >
                        </div>
                    </div>
                </div>
            </div>
            <?php }?>
            <input type="hidden" id="gamedate" value="<?php echo date("F, Y"); ?>">
                    <div class="col-md-12 mycarousalnew">
                        <div class="col-md-4 col-sm-4 col-xs-12 logames ">
                            <i class="icon-settings font-red-sunglo"></i>
                            <h3 class="caption-subject font-red-mint uppercase listgametitle"> List of games</h3>                      
                        </div>                        
                        <div class="col-md-4 col-sm-4 col-xs-12 monthrcds prev-next">
                            <div class="col-md-2 col-sm-2 col-xs-2 monthrcds">
                                <a class="left carousel-control carouselstle" href="#myCarousel" role="button" data-slide="prev">
                                    <i class='fa fa-chevron-left prev'></i>
                                    <span class="sr-only">Previous</span>

                                </a>
                            </div>
                            <div class="col-md-7 col-sm-7 col-xs-7 monthrcds">
                                <div class="gamemonth">
                                    <strong class=""></strong>
                                    <input type="hidden" id="datepicker" value=""><i class="fa fa-calendar" id="datepicker-button"></i>                                    
                                </div>
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-2 monthrcds">
                                <a class="right carousel-control carouselstle" href="#myCarousel" data-container="body" role="button" data-slide="next">
                                    <i class='fa fa-chevron-right next' ></i>
                                    <span class="sr-only">Next</span>
                                </a>
                            </div>

                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12 logamesbtn">
                        <a href="manage_game.php" class="btn btn-small addcustomerbtn" >
                         Add Game </a> 
                         <a href="add_bulk_game.php" style="margin-right:10px;" class="btn btn-small addcustomerbtn" >
                         Add Bulk Game </a>
                        </div>
                    </div>
                    <div class="loadingsection">
                        <img src="images/loading-publish.gif" alt="loadingimage" id="loadingimage">
                    </div>
                    <div class="swiper-wrapper">
                        <table class="table advancedtable" style="margin-bottom: 0px !important;">
                            <tr >
                                <th><div class="swiper-button-prev"></div></div></th>
                                <th class="text-center" colspan="2"><a href="#">schedule</a></th>
                                <th><div class="swiper-button-next"></th>
                            </tr>
                        </table>
                    </div>
                    <style>
                    .swiper-slide.portlet{
                        background-color: #f5f5f5;
                    }
                    </style>
                    <div class="col-md-12 col-sm-12 col-xs-12 swiper-container mycarousalnew list-game-top">
                        <div class="swiper-wrapper">
                            <?php  
                            // print_r($GameDetail);
                            if ($CntListingGame > 0) { 
                                foreach ($GameDetail as $GameDetailYearVal) {?>
                            <div class="swiper-slide portlet box grey">
                                <?php foreach ($GameDetailYearVal as $GameDetailMonth => $GameDetailMonthInfo) {?>
                                <div class="portlet box grey portletdown" style="margin-bottom:10px;">
                                <div class="portlet-title">
                                    <div class="caption captionstyle  tools" style="width: 98%;">
                                        <a href="javascript:;" class="expand" style="color:#000;background-image:none;display: block;width: 100%;"> <?php echo date('F', mktime(0, 0, 0, $GameDetailMonth, 10)); ?></a>
                                    </div>                                              
                                    <div class="tools">
                                        <a href="javascript:;" class="expand" style=""></a>
                                    </div>
                                </div>
                                <?php 
                                // print_r($GameDetailMonthInfo);
                                // $first_key = key($GameDetailMonthInfo); 
                                // $currentMonthNum = date("n")+1;
                                // $month = date("F", mktime(0,0,0,$GameDetailMonth+1,0,0));                        
                                foreach ($GameDetailMonthInfo as $Values) {
                                    // print_r($Values);
                                    $VisitName = json_decode(getTeamName($Values['visitor_team_id']), true);
                                    $HomeName = json_decode(getTeamName($Values['home_team_id']), true);
                                    $DivisionName = json_decode(getDivisionName($Values['division']), true);
                                    $GameType = json_decode(getGameType($Values['isLeagueGame']), true);
                                    $TournamentName = ($Values['isLeagueGame']) ? $Values['isLeagueGame'] : " " ;

                                    $enableModify = "display:none;";
                                    if ($_SESSION["usertype"] == "team_manager") {
                                        $start_date = new DateTime(date("Y-m-d H:i:s", strtotime($Values["date"]." ".$Values["time"])));
                                        $end_date = new DateTime(date("Y-m-d H:i:s"));
                                        $interval = $start_date->diff($end_date);
                                        if ($interval->y == 0 && $interval->m == 0 && $interval->d <= 3) {
                                            $diffDay = $interval->d;
                                            $enableModify = "";
                                        }
                                    }
                                    ?>
                                <div class="portlet-body " style="display:none;">
                                    <div class="portlet box grey portletdown">
                                        <div class="portlet-title">
                                            <div class="caption captionstyle tools" style="width: 98%;float:left;">
                                                <!-- <a href="javascript:;" class="expand" style="color:#000;background-image:none;display: block;width: 100%;"> Spring 2016</a> -->
                                                <a href="javascript:;" class="expand" id="shortgamedetail">
                                                <?php echo $VisitName." vs ".$HomeName." "; ?><small><?php echo "&nbsp;&nbsp;&nbsp;".date("F d, Y", strtotime($Values['date'])); echo " ".$Values['time']; ?></small>
                                                </a>
                                            </div>                                              
                                            <div class="tools">
                                                <a href="javascript:;" class="expand" style=""></a>
                                            </div>
                                        </div>
                                        <div class="portlet-body " style="display:none;">
                                            <div class="col-md-12 rm-padding table-responsive">
                                                <table class="table table-hover table-bordered gameinfotable">
                                                    <tr>
                                                        <th nowrap ><strong>Game Id</strong></th>
                                                        <th nowrap><strong>Game Name</strong></th>
                                                        <th nowrap><strong>Visitor Team</strong></th>
                                                        <th nowrap><strong>Home Team</strong></th>
                                                        <th nowrap><strong>Division</strong></th>
                                                        <th nowrap><strong>Game Type</strong></th>
                                                        <th nowrap><strong>Location</strong></th>
                                                        <th nowrap><strong>Tournament</strong></th>
                                                        <th nowrap><strong>Action</strong></th>
                                                    </tr>
                                                    <tr>
                                                        <td nowrap><?php echo $Values['id']; ?></td>
                                                        <td nowrap><?php echo $Values['game_name']; ?></td>
                                                        <td nowrap><?php echo $VisitName; ?></td>
                                                        <td nowrap><?php echo $HomeName; ?></td>
                                                        <td nowrap><?php echo $DivisionName; ?></td>
                                                        <td nowrap><?php echo $GameType; ?></td>
                                                        <td nowrap><?php echo $Values['game_location']; ?></td>
                                                        <td nowrap><?php echo $Values['tournament']; ?></td>
                                                        
                                                        <td nowrap>
                                                            <div class="actions">
                                                                <a title="Edit game" href="manage_game.php?gid=<?php echo base64_encode($Values['id']); ?>" class="roundbtngreenedit btn-circle btn-icon-only green_btn">
                                                                    <i class="icon-note trash_btn"></i> 
                                                                </a>&nbsp;&nbsp;
                                                                <a title="Delete game" class="roundbtnreddelete btn-circle btn-icon-only red_btn" onclick="return deleteGame('<?php echo $Values['id']; ?>','<?php echo $SportName; ?>');">
                                                                    <i class="icon-trash trash_btn" ></i>
                                                                </a>&nbsp;&nbsp;

                                                                <?php if ($SportName == 'basketball') {
                                                                    if ($signin != 'team_manager') {
                                                                ?>
                                                                <a href="game_stats.php?gid=<?php echo base64_encode($Values['id']); ?>" title="Modify game" class="roundbtnyellow btn-circle btn-icon-only modifystatsbtn yellow_btn">
                                                                    <i class="icon-doc" ></i> 
                                                                </a>&nbsp;&nbsp;
                                                                <?php } else { ?>
                                                                     <a style="<?php echo $enableModify; ?>" href="game_stats.php?gid=<?php echo base64_encode($Values['id']); ?>" title="Modify game" class="roundbtnyellow btn-circle btn-icon-only yellow_btn modifystatsbtn">
                                                                    <i class="icon-doc" ></i> 
                                                                    </a>&nbsp;&nbsp;

                                                                <?php }  
                                                                $GameDetailsQry = $conn->prepare("select * from game_details where xml_game_id=:gameid");
                                                                $gameid = $Values['id'];
                                                                $GameDetailsQryArr = array(":gameid"=>$gameid);
                                                                $GameDetailsQry->execute($GameDetailsQryArr);
                                                                $CntGameDetails = $GameDetailsQry->rowCount();
                                                                if($CntGameDetails >0){
                                                                    $FetchGameDetails = $GameDetailsQry->fetch(PDO::FETCH_ASSOC);
                                                                ?>
                                                                 <a herf="javascript:;" title="combine player" class=" roundbtnblue btn-circle btn-icon-only  combineplayer blue_btn"  data-toggle="modal" data-target="#CombineModal" data-gameinfoid="<?php echo $Values['id'];?>" data-date="<?php echo $FetchGameDetails['date'];?>" data-season="<?php echo $FetchGameDetails['season'];?>" 
                                                                 data-id="<?php echo $FetchGameDetails['id'];?>"><i class="icon-tag" ></i></a> 
                                                                 <?php }}

                                                                 ?>
                                                                  
                                                            </div>
                                                        </td>
                                                    </tr>

                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                   
                                </div>

                             <?php }?>
                             </div>
                        <?php } } }
                        // exit;
                        ?>
                            </div>
                        </div>
                    </div>                    
        </div>
    </div>
</div>
<div id="CombineModal" class="modal fade" role="dialog">
    <div class="modal-dialog">                            
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Combine Player </h4>
            </div>
            <div class="modal-body">
                <form name="combineplayer" id="combineplayerfrm" method="POST" class="form-horizontal" novalidate="novalidate" action="">
                    <div class="combinemakecenter">
                        <input type="hidden" name="combine_gameid" id="combine_gameid" value="">
                        <input type="hidden" name="combine_season" id="combine_season" value="">
                        <input type="hidden" name="combine_date" id="combine_date" value="">
                        <input type="hidden" name="combine_gameinfoid" id="combine_gameinfoid" value="">
                        <div class="col-md-5 gameidtable combinemakecenter">
                            <table  class="table comineplatable">
                            <tr>
                            <td >Game ID</td>
                            <td id="game_view_id"></td>
                            </tr>
                            </table>                
                        </div>
                        <div class="col-md-10 combinemakecenter">
                            <div class="cobmineform">
                                <div class="form-group col-md-12 ">
                                    <label>Actual Player ID<span class="error">*</span></label>
                                    <input class="form-control requiredcs border-radius" type="text" name="playerid" id="playerid" placeholder="Actual Palyer ID" /> 
                                    <label id="playeriderror" class="error" for="playerid">Please enter actual player id</label>              
                                </div>  
                                <div class="form-group col-md-12 ">

                                    <label>Duplicate Player ID<span class="error">*</span></label>
                                    <input class="form-control requiredcs border-radius" type="text" name="duplicateplayerid" id="duplicateplayerid" placeholder="Duplicate Palyer ID" />
                                    <label id="duplicateplayeriderror" class="error" for="duplicateplayerid">Please enter duplicate player id</label> 
                                </div>
                            </div>
                            <div class="cobminestatus">
                                <div class="alert-success combineappend">Player combined and duplicate player deleted successfully</div>
                            </div>
                            <div class="cobminestatusfailure">
                                <div class="alert-danger combineappend">Player not combined</div>
                            </div>
                            <div class="form-group col-md-12 popupbtn">                                     
                                <input class="btn btnpopupgreen addcombinebtn" style="line-height: 1.7;" id="popupcombinebtn" value="Submit" type="button">
                                <button class="btn cancelbtn btnpopupred" type="button" data-dismiss="modal">Cancel</button>
                            </div>  
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php include_once('footer.php'); ?>

<script src="assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js" type="text/javascript"></script>
<script src="assets/pages/scripts/components-bootstrap-select.min.js" type="text/javascript"></script>
<link rel="stylesheet" href="assets/custom/css/jquery-monthpicker-ui.css" rel="stylesheet" type="text/css"> 
<script src="assets/custom/js/jquery-monthpicker-ui.js" type="text/javascript"></script>
<script src="assets/custom/js/jquery.mtz.monthpicker.js"></script>
<script src="assets/custom/js/gamelist_v1.js" type="text/javascript"></script>
<script src="assets/custom/js/swiper.jquery.js" type="text/javascript"></script>
<script>

$( "#searchtext" ).keyup(function() {
    //ajaxGameList("");
});

$(document).ready(function () {

    $("#combineplayerfrm").validate({
        rules: {
          playerid:{required:true,number: true},
       duplicateplayerid:{required:true,number: true,uniquedivision:true},
          
         },
        messages: {
             playerid:{required:"Please enter actual player id",number:"Please enter actual player id in number"},
          duplicateplayerid:{required:"Please enter duplicate player id",number:"Please enter duplicate player id in number",uniquedivision:"This Player Id is not in this game"},
        },

    });
    $('.addcombinebtn').click( function() { 
        $("#combineplayerfrm").valid();  // test the form for validity
        if ($("#combineplayerfrm").valid()) {
            $( "#CombineModal" ).modal("show");
            var gameid   = $('#combine_gameid').val();
            var season = $('#combine_season').val();
            var playerid  = $('#playerid').val();
            var duplicateplayerid = $('#duplicateplayerid').val();  
            var date = $('#combine_date').val();
            
            $.ajax({
                url:"updatecombineplayerstats.php",  
                method:'POST',
                data:{gameid: gameid, season: season, playerid: playerid, duplicateplayerid: duplicateplayerid, date: date},
                success:function(data) {
                    var response = $.parseJSON(data);
                    var appendHTML = "";
                    if (response["responsestatus"] == "success") {
                        $("#popupcombinebtn, .gameidtable, .cobmineform").hide();
                        $(".cancelbtn").css("margin-left", "0px");
                        $(".cobminestatus").show();
                        appendHTML = '<div class="alert-success combineappend">'+response["status"]+'</div>';
                        $(".cobminestatus").empty().append(appendHTML);
                        
                    } else {
                        $("#popupcombinebtn, .gameidtable, .cobmineform").hide();
                        $(".cancelbtn").css("margin-left", "0px");
                        $(".cobminestatusfailure").show();
                        appendHTML = '<div class="alert-danger combineappend">'+response["status"]+'</div>';
                        $(".cobminestatusfailure").empty().append(appendHTML);
                    }
                    
                }
            });
        }
    });

});
var swiper = new Swiper('.swiper-container', {
    pagination: '.swiper-pagination',
    paginationClickable: true,
    nextButton: '.swiper-button-next',
    prevButton: '.swiper-button-prev',
    spaceBetween: 30
}); 
</script>
