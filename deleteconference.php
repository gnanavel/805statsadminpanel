<?php 
include_once('session_check.php');
include_once('connect.php');

if ((isset($_POST['conferenceid'])) && (!empty($_POST['conferenceid']))) {
    $conferenceid	= $_POST['conferenceid'];
	$seasonid		= $_POST['seasonid'];
	
	$QryArr			= array(":season_id"=>$seasonid,":conference_id"=>$conferenceid,":customer_id"=>$MasterCustId);
	
    $delseasonqry = $conn->prepare("delete from customer_season_conference where season_id=:season_id and conference_id=:conference_id and customer_id=:customer_id");	
    $delseasonqry->execute($QryArr);

	$delconfqry = $conn->prepare("delete from customer_conference_division where season_id=:season_id and  conference_id=:conference_id and customer_id=:customer_id");
    $delconfqry->execute($QryArr);
	
	$delteamqry = $conn->prepare("delete from customer_division_team where conference_id=:conference_id and season_id=:season_id and customer_id=:customer_id");
    $delteamqry->execute($QryArr);

	$delplayerqry = $conn->prepare("delete from customer_team_player where conference_id=:conference_id and season_id=:season_id and customer_id=:customer_id");	
    $delplayerqry->execute($QryArr);

	echo "success";
	exit;
}
