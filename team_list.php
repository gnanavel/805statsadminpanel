<?php 
include_once('session_check.php');
include_once('connect.php');
include_once('common_functions.php');
include_once('usertype_check.php');

if (isset($_SESSION["sportid"])) {
    
    $SportId = $_SESSION["sportid"];
    $SportName = $_SESSION["sportname"];
}

if ($SportId=='4444') { $tablename='team_stats_bb'; } 
if ($SportId=='4442' || $SportId=='4441') { $tablename='team_stats_ba'; } 
if ($SportId=='4443') { $tablename='team_stats_fb'; }

if ($_SESSION['logincheck'] == 'master') {

    $DefaultQry = $conn->prepare("select * from customer_division where custid in ($customerid) order by id DESC limit 0,1");
    $DefaultQry->execute();
    $Cntdres = $DefaultQry->rowCount(); 
    $FetchDefaultDivision = $DefaultQry->fetch(PDO::FETCH_ASSOC);
    $DefaultDivision = $FetchDefaultDivision['id'];
    $DefaultDivQryCondn = " and division=".$DefaultDivision;
} else {    

    $DefaultSesQry = $conn->prepare("select distinct(season) from $tablename where customer_id in ($LoginCustId) and season<>'' order by id DESC limit 0,1");
    $DefaultSesQry->execute();
    $Cntsres = $DefaultSesQry->rowCount(); 
    $FetchDefaultSeason = $DefaultSesQry->fetch(PDO::FETCH_ASSOC);
    $DefaultSeason = $FetchDefaultSeason['season'];
    $DefaultSesQryCondn = " and season=".$DefaultSeason;
}

//Destroy seached session values while click RESET BUTTON
if (isset($_POST["reset"])) {
    $_SESSION["team"] = "";
}
// print_r($_SESSION);
$Page = 1 ;
$SortingColumnNum = "";
$SortingColumnOrder = "";
if (isset($_POST["SortingColumnNum"])) {
    $SortingColumnNum = $_POST["SortingColumnNum"];
    $SortingColumnOrder = $_POST["SortingColumnOrder"];  
}

if (isset($_SESSION["team"]) && !empty($_SESSION["team"])) {
    $Teamsearch = $_SESSION["team"];
    $HiddenDivid = $Teamsearch["hdndivid"] ? $Teamsearch["hdndivid"] : "" ;
    $HiddenSesid = $Teamsearch["hdnsesid"] ? $Teamsearch["hdnsesid"] : "" ;
    $HiddenSearchtext = $Teamsearch["hdnsearchteam"] ? $Teamsearch["hdnsearchteam"] : "" ;
    $hdn_status = $Teamsearch["hdn_status"] ? $Teamsearch["hdn_status"] : "" ;    
    $Page =  $Teamsearch["HdnPage"] ? $Teamsearch["HdnPage"] : $Page ;
    // $Page =  isset($_POST["HdnPage"]) ? $_POST["HdnPage"] : $Page ;
    $SortingColumnNum =  isset($_POST["SortingColumnNum"]) ? $_POST["SortingColumnNum"] : $Teamsearch["SortingColumnNum"] ;
    $SortingColumnOrder =  isset($_POST["SortingColumnOrder"]) ? $_POST["SortingColumnOrder"] : $Teamsearch["SortingColumnOrder"] ;    
}
$_SESSION["team"]["SortingColumnNum"] = $SortingColumnNum;
$_SESSION["team"]["SortingColumnOrder"] = $SortingColumnOrder;


if(isset($_POST["hdndivid"])){
    $HiddenDivid = $_POST["hdndivid"];
}

if(isset($_POST["hdnsesid"])){
    $HiddenSesid = $_POST["hdnsesid"];
}

if(isset($_POST["hdnsearchteam"])){
    $HiddenSearchtext = $_POST["hdnsearchteam"];
}

if(isset($_POST["hdn_status"])){
    $hdn_status = $_POST["hdn_status"];
} 
else {
    if (empty($_SESSION["team"]["hdn_status"])) {
        $status="and teams_info.archive='0'";
        $status1="and archive='0'";
    }

}
if (isset($_POST['hdn_status'])) {

    $hdn_status = $_POST["hdn_status"]?$_POST['hdn_status']:"active";

    if ($hdn_status == "active") {
        $status = "and teams_info.archive='0'";
        $status1 = "and archive='0'";
    } 
    if ($hdn_status == "Inactive") {
        $status = "and teams_info.archive='1'";
        $status1 = "and archive='1'";
    }

}

// $HiddenDivsion = ( $HiddenDivid )? $HiddenDivid : $_POST['divisionid'] ;
// $HiddenSeson = ( $HiddenSesid )? $HiddenSesid : $_POST['seasonid'] ;
// $HiddenSearchTeam = ( $HiddenSearchtext )? $HiddenSearchtext : $_POST['hdnsearchteam'] ;
$HiddenDivsion = ( $HiddenDivid )? $HiddenDivid : "" ;
$HiddenSeson = ( $HiddenSesid )? $HiddenSesid : "" ;
$HiddenSearchTeam = ( $HiddenSearchtext )? $HiddenSearchtext : "" ;
$HiddenContn = "";
// if (isset($_POST['hdndivid']) || isset($_POST['hdnsesid']) ) {   

    if ($_SESSION['logincheck'] == 'master') {
        
        if (!empty($HiddenDivsion) && !empty($HiddenSearchTeam)) { 
            $HiddenContn = " and division='$HiddenDivsion' and team_name like '%$HiddenSearchTeam%'";
        } else if (!empty($HiddenDivsion) && empty($HiddenSearchTeam)) {
            $HiddenContn = " and division='$HiddenDivsion'";
        } else if (empty($HiddenDivsion) && !empty($HiddenSearchTeam)) {
            $HiddenContn = " and team_name like '%$HiddenSearchTeam%'";
        }
        $res = "SELECT * FROM teams_info WHERE customer_id IN ($customerid) $HiddenContn $status1 and (sport_id='$SportId') order by team_name";
        
    } else {

        if (!empty($HiddenSeson) && !empty($HiddenSearchTeam)) {   
            $HiddenContn = " and $tablename.season='$HiddenSeson' and teams_info.team_name like '%$HiddenSearchTeam%'";
        } else if (!empty($HiddenSeson) && empty($HiddenSearchTeam)) {
            $HiddenContn = " and $tablename.season='$HiddenSeson'";
        } else if (empty($HiddenSeson) && !empty($HiddenSearchTeam)) {
            $HiddenContn = " and teams_info.team_name like '%$HiddenSearchTeam%'";
        }

        $res = "select teams_info.* from teams_info LEFT JOIN $tablename ON teams_info.id=$tablename.teamcode where  teams_info.customer_id in ($LoginCustId) $HiddenContn $status group by teams_info.id order by teams_info.team_name";
    }
// } 

// echo $res;
if($_SESSION['logincheck'] == 'master') {?>
<style>
#sample_1_wrapper table.dataTable.dtr-inline.collapsed>tbody>tr>td:first-child:before, table.dataTable.dtr-inline.collapsed>tbody>tr>th:first-child:before{
    display:none;
}
</style>
<?php }

$alert_message = '';
$alert_class = '';
if (isset($_GET['msg'])) { 
    if ($_GET['msg'] == 1) {
        $alert_message = "Team has been added successfully";
        $alert_class = "alert-success";
    } else if ($_GET['msg'] == 2) {
        $alert_message = "Team has been updated successfully";
        $alert_class = "alert-success";
    } else if ($_GET['msg'] == 3) {
        $alert_message = "Team has been deleted successfully";
        $alert_class = "alert-danger";
    } else {
        $alert_message = "Something wrong!!";
        $alert_class = "alert-danger";
    }
}

/****Paging ***/
// $Page = 1;
$RecordsPerPage = 25;
if(isset($_REQUEST['HdnPage']) && is_numeric($_REQUEST['HdnPage'])) {
   $Page = $_REQUEST['HdnPage'];
   $_SESSION["team"]["HdnPage"] = $Page;
}
/*End of paging*/

include_once('header.php'); ?>
<link href="assets/custom/css/teamlist.css" rel="stylesheet" type="text/css" />
<style type="text/css">

table.dataTable.no-footer {
    border-bottom: 0px solid #111; 
}
table.dataTable{
    border-collapse: collapse;
}
</style>
<div class="page-content-wrapper">
        <div class="page-content">
            <?php if (isset($_GET['msg'])) { ?>
            <div class="alert alert-block fade in <?php echo $alert_class; ?>">
                <button type="button" class="close" data-dismiss="alert"></button>
                <p> <?php echo $alert_message; ?> </p>
            </div>
            <?php } ?>
            <div class="row searchheder">                
                <form method="post" id="searchteamform">
                    <div class="col-md-12 col-sm-12 col-xs-12 searchbarstyle" >
                        <div class="col-md-4 col-sm-4 col-xs-12 select_season">
                            <div class="form-group ">
                                <input type="hidden" name="sportid" id="sportid" value="<?php echo $SportId ?>">
                                <input type="hidden" name="customerid" id="customerid" value="<?php echo $LoginCustId ?>">
                                <input type="hidden" name="tablename" id="tablename" value="<?php echo $tablename ?>">
                                <?php
                                if($_SESSION['logincheck'] != 'master') {
                                    $searchClass="searchbycustomteam";                                
                                    $SeasonRes = $conn->prepare("SELECT season,name from customer_season JOIN $tablename on customer_season.id=$tablename.season  where  customer_season.custid in (:cid) and $tablename.season != '' group by season ORDER BY customer_season.season_order DESC");
                                    $SeasonResArr = array(":cid"=>$LoginCustId);
                                    $SeasonRes->execute($SeasonResArr);
                                    $SeasonResCnt = $SeasonRes->rowCount(); ?> 

                                    <select class="form-control border-radius" id="searchbyseasonid" name="seasonid">
                                    <?php if ($SeasonResCnt > 0) {?>
                                        <option value="">Select season</option>
                                        <?php
                                        $FetchSeason = $SeasonRes->fetchAll(PDO::FETCH_ASSOC);
                                        foreach ($FetchSeason as $SeasonRow) { ?>                                    
                                        <option <?php echo ($SeasonRow['season'] == $HiddenSesid) ? "selected" :"" ;?> value="<?php echo $SeasonRow['season']; ?>"><?php echo $SeasonRow['name']; ?></option>
                                        <?php }
                                    } else {?>
                                        <option value="">Select season</option>
                                    <?php } ?>
                                    </select>
                                <?php } else {
                                    $searchClass = "searchbyabateam";
                                    $Dres = $conn->prepare("select * from customer_division where custid in ($customerid) order by name ASC");
                                    $Dres->execute();
                                    $Cntdres = $Dres->rowCount(); ?>
                                    <select class="form-control border-radius" id="searchbydivisionid" name="divisionid">
                                    <option value="">Select Division</option>
                                    <?php 
                                    if ($Cntdres > 0) {
                                        $FetchDivision = $Dres->fetchAll(PDO::FETCH_ASSOC);
                                        foreach ($FetchDivision as $drow) { ?>
                                            <option <?php echo ($drow['id'] == $HiddenDivid) ? "selected" :"" ;?> value="<?php echo $drow['id']; ?>"  ><?php echo $drow['name']; ?></option>
                                        <?php }
                                    } else { ?>
                                        <option value="">Select Division</option>
                                    <?php }                                 
                                }
                                ?>
                                </select>
                            </div>
                        </div>
    					
                        <div class="col-md-3 col-sm-3 col-xs-12 col-lg-4 removerightpadding search_txt">
                            <!-- <form class="search-form search-form-expanded" > -->
                                <div class="form-group">
                                    <input type="text" id="searchtext" class="form-control border-radius" placeholder="Search by Team name" name="query" value="<?php echo $HiddenSearchtext ?>">
                                </div>
                            <!-- </form> -->
                        </div>
    					<div class="col-md-2 col-sm-2 col-xs-12 removerightpadding">
                            <!-- <form class="search-form search-form-expanded" > -->
                               <div class="form-group ">
    								<div class="form-group">
    									<select class="form-control  player_form border-radius" name="active_status" id="searchbystatus"> 
    										<option value="all">All Team</option>
    										<option value="active">Active</option>
    										<option value="Inactive">InActive</option>
    									</select>
    								   <?php if($hdn_status!=""){$active = $hdn_status;} else {$active = "active";} ?>
    									<script>$("#searchbystatus").val("<?php echo $active;?>")</script> 
    								</div>
    							</div> 
                            <!-- </form> -->
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-12 col-lg-2 searchrightpadding">
                            <div class="form-group">
                                <input type="button" class="btn searchbtnyellow reset-left1 <?php echo $searchClass;?>" style="border-radius:5px !important;line-height: 1.5;" value="Search" name="search">
                                <input type="submit" id="resetbtn" class="btn resetbtnred reset-left" style="margin-left:5px" value="Reset" name="reset">
                            </div>
                        </div>
                    </div>                
                </form>
            </div>            
            <div class="row">
                <div class="col-md-12">                    
                    <div class="portlet-body customerlist-tbl-pr clearfix" style="clear: both;">
                        <div class="widget-header"> 
                            <h3>
                            <i class="icon-settings font-red-sunglo"></i>
                            LIST OF TEAMS                       
                            </h3>
                            <div class="pull-right">                                
                                <input type="button" class="btn btn-small addcustomerbtn adddarkbtnlist" onclick="document.location='manage_team.php'" value="Add Team" style="margin-right:14px;border-radius: 4px !important;font-size:13px;">
                                
                            </div>
                        </div>
                        <div class="loadingsection">
                            <img src="images/loading-publish.gif" alt="loadingimage" id="loadingimage">
                        </div>

                        <div class="portlet-body">
                            <div class="table-responsive" id="ajaxteamlist" >
                                <form id="team_list" name="team_list" method="post" >
                                <input type="hidden" name="HdnPage" id="HdnPage" value="<?php echo $Page; ?>">
                                <input type="hidden" name="HdnMode" id="HdnMode" value="<?php echo $Page; ?>">
                                <input type="hidden" name="RecordsPerPage" id="RecordsPerPage" value="<?php echo $RecordsPerPage; ?>">
                                <input type="hidden" name="hdndivid" id="hdndivid" value="<?php echo $HiddenDivid ?>">
                                <input type="hidden" name="hdnsesid" id="hdnsesid" value="<?php echo $HiddenSesid ?>">
                                <input type="hidden" name="hdnsearchteam" id="hdnsearchteam" value="<?php echo $HiddenSearchtext ?>">
								<input type="hidden" name="hdn_status" id="hdn_status" value="<?php echo $hdn_status;?>">
                                <input type="hidden" name="SortingColumnNum" id="SortingColumnNum" value="<?php echo $SortingColumnNum;?>">
                                <input type="hidden" name="SortingColumnOrder" id="SortingColumnOrder" value="<?php echo $SortingColumnOrder;?>">
                             
                                <table class="table table-striped table-bordered table-hover dataTable no-footer dataTable customerlist-tbl" id="sample_1" sytle="border: 0px solid #CCC;border-collapse: collapse;">
                                    <thead>
                                    <tr>
                                        <th nowrap > Team&nbsp;ID </th>
                                        <th nowrap> Team Name </th>
                                        <th nowrap> Abbr </th>
                                        <th nowrap> Division </th>
                                        <th nowrap > Venue </th>
										<th nowrap style="width: 30px;"> Video </th>
                                        <th nowrap> Team&nbsp;Logo </th>
                                        <th nowrap> Action </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                            $dbQry = $res;
                                            $getResQry      =   $conn->prepare($dbQry);
                                            $getResQry->execute();
                                            $getResCnt      =   $getResQry->rowCount();
                                            if ($getResCnt > 0) {
                                                $TotalPages=ceil($getResCnt/$RecordsPerPage);
                                                $Start=($Page-1)*$RecordsPerPage;
                                                $sno=$Start+1;                                        
                                                $dbQry.=" limit $Start,$RecordsPerPage";
                                                $getResQry      =   $conn->prepare($dbQry);
                                                $getResQry->execute();
                                                $getResCnt      =   $getResQry->rowCount();

                                                if($getResCnt>0){
                                                    $getResRows     =   $getResQry->fetchAll(PDO::FETCH_ASSOC);
                                                    $s=1;
                                                    foreach($getResRows as $team){
                                                    $isSuspended=$team['isSuspended'];
                                                    if($isSuspended=="1"){
                                                        $background="background-color:#D3D3D3 !important;";

                                                    } else {
                                                        $background="";
                                                    }
                                                            
                                        ?>
                                        <tr class="odd gradeX" style="<?php echo $background; ?>">
                                            <td nowrap  style="<?php echo $background; ?>"><?php echo $team['id']; ?></td>
                                            <td nowrap style="<?php echo $background; ?>"><?php echo $team['team_name']; ?>
                                            <td nowrap style="<?php echo $background; ?>"><?php echo $team['abbrevation']; ?></td>
                                            <td nowrap style="<?php echo $background; ?>">
                                            <?php 
                                            $divid= $team['division'];
                                            $orgQry = $conn->prepare("select * from customer_division where id=:divid");        
                                            $QryArr = array(":divid"=>$divid);        
                                            $orgQry->execute($QryArr);                                        
                                            $DivName = $orgQry->fetch(PDO::FETCH_ASSOC);
                                            echo $DivName['name'];
                                            ?></td>
                                            <td nowrap style="<?php echo $background; ?>" ><?php echo $team['stadium']; ?></td>
											<td nowrap  style="<?php echo $background; ?>width: 30px;"><?php 
											$teamid = $team['id'];
											$custvideoQry=$conn->prepare("select * from customer_tv_station where team_id=:team_id and customer_id in (:customer_id)");
											$custQryArr = array(":team_id"=>$teamid,":customer_id"=>$LoginCustId); 
											$custvideoQry->execute($custQryArr);                                        
                                            $Video = $custvideoQry->fetch(PDO::FETCH_ASSOC);
											$videoactive = $Video['active'];
											if ($videoactive == '1') {
											    $videostatus = 'Y';
											} else {
											    $videostatus='N';
											}
											echo $videostatus;
											?></td>
                                            <td nowrap style="<?php echo $background; ?>">
                                            <?php if($team['team_image']!=""){ ?>
                                                <img src="uploads/teams/<?php echo $team['team_image']; ?>" alt="" width="25" height="25" />
                                            <?php } ?>
                                            </td>
                                            <td nowrap style="<?php echo $background; ?>">
                                                <table class="table-hover">
                                                    <tr style="<?php echo $background; ?>text-align:center;">
                                                        <td nowrap class="teamaction" style="<?php echo $background; ?>">
                                                        <a href="manage_team.php?tid=<?php echo base64_encode($team['id']); ?>"  class="roundbtngreenedit edit_btnteam btn-circle btn-icon-only"><i class="icon-note trash_btn"></i>
                                                            
                                                        </a>
                                                        </td>
                                                        <td nowrap class="teamaction" style="<?php echo $background; ?>">   
                                                        <a  class="roundbtnreddelete btn-circle btn-icon-only" onclick="return deleteTeam('<?php echo $team['id']; ?>','<?php echo $SportName; ?>');"><i class="icon-trash trash_btn"></i>
                                                            
                                                        </a>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                        </tr>
                                        <?php
                                            $s++;
                                           }
                                        } else {
                                           // echo "<td colspan='8' style='text-align:center;line-height:1.7em;'>No Team(s) found.</td>";
                                        }
                                        } else {
                                            // echo "<tr><td colspan='8' style='text-align:center;line-height:1.7em;'>No Team(s) found.</td></tr>";
                                        }
                                        ?>
                                        
                                    </tbody>
                                </table>
                                <?php
                                    if($TotalPages > 1){

                                    echo "<tr><td style='text-align:center;' colspan='8' valign='middle' class='pagination'>";
                                    $FormName = "team_list";
                                    require_once ("paging.php");
                                    echo "</td></tr>";

                                    }
                               ?>
                            </div>
                        </div>
                    
                    </div><!--- portlet-body customerlist-tbl-pr clearfix !-->
                </div><!--- col-md-12 !-->
            </div><!--- row !-->
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->                
</div>
<!-- END CONTAINER -->

<?php include_once('footer.php'); ?>
<script src="assets/custom/js/teamlist.js" ></script>