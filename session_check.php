<?php
session_start();
if (!array_key_exists("loginid", $_SESSION) && empty($_SESSION['loginid']))  {
    header('Location:login.php');
    exit;
}