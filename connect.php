<?php 
global $conn;
error_reporting(0);
define("LOCAL_SERVER_NAME","localhost");
define("DEMO_LIVE_SERVER_NAME","805stats.com"); 
define("LIVE_SERVER_NAME","805stats.com"); 
define("MEDIA_SERVER_URL","http://35.162.166.183:8182/api/v1"); 
define("MEDIA_SERVER","35.162.166.183"); 

global $conn;
//PDO connection starts here
try {
    
	if(substr_count($_SERVER['SERVER_NAME'], LOCAL_SERVER_NAME)>0)
	{
		// Database Informations
		define('DB_HOST_NAME',"localhost");
		define('DATABASE',"stats_dashboard");
		define('USERNAME',"root");
		define('PASSWORD',"");
		define("SYSTEM_ROOT_PATH",$_SERVER["DOCUMENT_ROOT"]."/805statsadminpanel");
		define("HTTP_ROOT","http://".$_SERVER["SERVER_NAME"]."/805statsadminpanel");
		define("SYSTEM_IMAGE_PATH","http://localhost/805statsadminpanel");
	} 

	else if(substr_count($_SERVER['SERVER_NAME'],DEMO_LIVE_SERVER_NAME)>0) {
		// Database Informations
		define('DB_HOST_NAME',"localhost");
		define('DATABASE',"stats_dashboard");
		define('USERNAME',"stats_dash");
		define('PASSWORD',"Admin@1234");
		define("SYSTEM_ROOT_PATH",$_SERVER["DOCUMENT_ROOT"]."/panel/");
		define("HTTP_ROOT","http://".$_SERVER['SERVER_NAME']."/panel");
		define("SYSTEM_IMAGE_PATH","http://".$_SERVER["HTTP_HOST"]."/panel");
	}
	/*else if(substr_count($_SERVER['SERVER_NAME'],LIVE_SERVER_NAME)>0) 
	{

		// Database Informations
		define('DB_HOST_NAME',"localhost");
		define('DATABASE',"stats_dashboard");
		define('USERNAME',"stats_admin");
		define('PASSWORD',"Karol@0826");
		define("SYSTEM_ROOT_PATH",$_SERVER["DOCUMENT_ROOT"]."/805stats.com/panel/");
		define("HTTP_ROOT","http://".$_SERVER["HTTP_HOST"]."/panel");
	}*/

	// Mysql PDO connection
	$conn=new PDO("mysql:host=".DB_HOST_NAME.";dbname=".DATABASE,USERNAME,PASSWORD);
	$conn->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING );

}catch(PDOException $e){
   echo "Connection failed: " . $e->getMessage();
}



if($_SESSION['logincheck'] == 'master'){		
	$customerid   = $_SESSION['loginid'].",".$_SESSION['childrens'];
	$LoginCustId  = $_SESSION['loginid'];
	$MasterCustId = $_SESSION['loginid'];

}else if($_SESSION['logincheck']=='children'){	

	$customerid  = $_SESSION['parent'];	
	$ExeQry = $conn->prepare("select * from customer_info where id=$customerid");
	$ExeQry->execute();
	$QryCnt = $ExeQry->rowCount();
	if ($QryCnt > 0) {
        $FetchRow = $ExeQry->fetch(PDO::FETCH_ASSOC);
        $_SESSION['childrens'] = $FetchRow['children'];
		$_SESSION['site_url'] = $FetchRow['site_url'];
	}
	$customerid   = $customerid.",".$_SESSION['childrens'];
	$LoginCustId  = $_SESSION['loginid'];
	$MasterCustId = $_SESSION['parent'];

}else{	
	$customerid   = $_SESSION['loginid'];
	$LoginCustId  = $_SESSION['loginid'];
	$MasterCustId = $_SESSION['loginid'];
}
