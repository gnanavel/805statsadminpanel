<?php
include_once('session_check.php'); 
include_once('connect.php');

if ( isset($_REQUEST['email']) && $_REQUEST['postcheck'] == "customerlogincheck" ) {
    $checkEmail = $_REQUEST["email"];
    $customerid = $_REQUEST['customerid'];

    $custIdCondn = (!empty($customerid))?" AND id NOT IN ($customerid)" : "";
    $checkQry = $conn->prepare("SELECT * from customer_info where email=:checkEmail $custIdCondn");
    $checkArr = array(":checkEmail"=>$checkEmail);    
    $checkQry->execute($checkArr);
    $cntEmail = $checkQry->rowCount();
    if ($cntEmail >0) {
        $fetchEmail = $checkQry->fetch(PDO::FETCH_ASSOC);
        $status = "exists";
    } else {
        $status = "notexists";
    } 
    echo $status;
    
}
exit;