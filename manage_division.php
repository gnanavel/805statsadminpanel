<?php 
include_once('session_check.php'); 
include_once("connect.php");
include_once('usertype_check.php');
include_once('common_functions.php');

if(isset($_POST['division_name']) && !empty($_POST['division_name'])){
    
    $division_name = $_POST['division_name'];    
    $division_rulelist = $_POST['division_rulelist'];
    $concat_customer_name = $division_name. " - " .$division_rulelist; 
    $divsion_id = $_POST['cust_divsion_id'];
    if (empty($divsion_id)) {
        $InsertDivision = $conn->prepare("INSERT INTO customer_division(name, custid) VALUES ('$concat_customer_name', '$LoginCustId')");
        $InsertDivision->execute();
        header('Location:manage_division.php?msg=1');
        exit;
    } else {
        $update_results=array(":division_name"=>$concat_customer_name,":division_id"=>$divsion_id);
        // print_r($update_results);exit;
        $updateQry="update customer_division set name=:division_name  where id=:division_id";        
        $prepupdateQry=$conn->prepare($updateQry);
        $updateRes=$prepupdateQry->execute($update_results);
        header('Location:manage_division.php?msg=2');
        exit;
    }    
    
}

if (isset($_SESSION["sportid"])) {
    
    $sportid = $_SESSION["sportid"];
    $sportname = $_SESSION["sportname"];
}
$hdn_team_id = "";
if(isset($_POST["hnd_team_id"])){
	$hdn_team_id = $_POST["hnd_team_id"];
}	

$condition = "";
$limit="";
$divisionname = "";
$srchArr   = array();
if(isset($_POST['searchbtnpost'])){
   $divisionname= $_POST['divisionname'];   
   $condition  .=" and name like '%$divisionname%' ";
   $srchArr[":srch_name"]    = "%".$divisionname."%";
}
if(isset($_POST['hnd_team_id'])){
   $divisionname= $_POST['hnd_team_id'];   
   $condition  .=" and name like '%$divisionname%' ";
   $srchArr[":srch_name"]    = "%".$divisionname."%";
}


$alert_message = '';
$alert_class = '';

if (isset($_GET['msg'])) { 
    if ($_GET['msg'] == 1) {
        $alert_message = "Added new division successfully";
        $alert_class = "alert-success";
    } else if($_GET['msg'] == 2) {
        $alert_message = "Updated new  division successfully";
        $alert_class = "alert-success";
    } else if($_GET['msg'] == 3) {
        $alert_message = "Division deleted successfully";
        $alert_class = "alert-success";
    }
}

/****Paging ***/
$Page = 1;$RecordsPerPage = 25;
if(isset($_REQUEST['HdnPage']) && is_numeric($_REQUEST['HdnPage']))
    $Page = $_REQUEST['HdnPage'];
/*End of paging*/
include_once('header.php');
?>
<link href="assets/custom/css/divisionlist.css" rel="stylesheet" type="text/css" />
    <div class="page-content-wrapper">
        <div class="page-content">

            <div class="row">
                <div class="col-md-12">
                    <form action="" method="post" id="searchplayedform">
                        <input type="hidden" name="customerid" id="customerid" value="<?php echo $cid ?>">
                        <input type="hidden" name="sportid" id="sportid" value="<?php echo $sportid ?>">

                        <div class="portlet light col-sm-12 col-md-12 col-xs-12 col-lg-12" style="padding: 15px 15px 0px;background: #FFF;border: 1px solid #CCC;-moz-border-radius: 5px;-webkit-border-radius: 5px;border-radius: 5px !Important;    float: left;margin-bottom:10px;">
                            <div class="portlet-title " style="border-bottom: 0px solid #eee;">                            
                                <div class="col-sm-9 col-md-9 col-xs-12 col-lg-7" style="padding:0px">
                                    <div class="col-md-7 col-sm-6 col-xs-12 searchboxstyle" style="padding-right:0px">
                                        <div class="form-group">
                                            <input class="form-control border-radius" type="text" name="divisionname"  placeholder="Division Name" id="manage_division_search" value="<?php echo $divisionname; ?>"> 
                                        </div>
                                    </div>
								<div class="col-md-2 col-sm-3 col-xs-3 resetbtnstyle" id="" style="text-align: center;padding: 0px ;">
                                        <div class="form-group">
                                          <input type="button" id="searchbtn" name="searchbtnpost" class="btn searchbtnyellow" value="Search" style="border-radius:5px !important;line-height: 1.5;"> 
                                        </div>
							</div>
								<div class="col-md-2 col-sm-3 col-xs-3 resetbtnstyle" id="" style="text-align: left;padding: 1px 0px 0px 0px ;">
                                        <div class="form-group">
                                            <a class="btn resetbtnred resetbtn">Reset</a> 
                                        </div>
										 <?php if($hdn_team_id!=""){
											$divisionids=$hdn_team_id;}
										 ?>
                                        
										</div>                                    
                                    
                                </div>
                            </div>
                        </div>
                    </form>
                 </div><!--Col-md-12 -->
            </div> <!--row -->

            <?php if (isset($_GET['msg'])) { ?>
            <div class="alert alert-block fade in <?php echo $alert_class; ?>">
                <button type="button" class="close" data-dismiss="alert"></button>
                <p> <?php echo $alert_message; ?> </p>
            </div>
            <?php } ?>            
          
            <div class="row">
                <div class="col-md-12">                    
                    
                    <div class="portlet-body customerlist-tbl-pr clearfix" style="clear: both;">

                        <div class="widget-header"> 
                            <h3>
                            <i class="icon-settings font-red-sunglo"></i>
                             LIST OF DIVISIONS                     
                            </h3>
                            <div class="pull-right mobile_right">
								<button type="button" class="btn btn-small addcustomerbtn" data-toggle="modal" style='margin-right:10px;border-radius: 4px !important;' id="adddivision">Add New</button>
								<input type="hidden" name="sports_name" id="sports_name" value="<?php echo $sportname?>" >
                            </div>
                        </div>
						<div class="loadingsection">
							 <img src="images/loading-publish.gif" alt="loadingimage" id="loadingimage">
					</div>
                    <div class="customerlistparent table-responsive">
                        <form id="customer_list" name="customer_list" method="post" action="">
                        <input type="hidden" name="HdnPage" id="HdnPage" value="<?php echo $Page; ?>">
                        <input type="hidden" name="HdnMode" id="HdnMode" value="<?php echo $Page; ?>">
                        <input type="hidden" name="RecordsPerPage" id="RecordsPerPage" value="<?php echo $RecordsPerPage; ?>">
						 <input type="hidden" name="hnd_team_id" id="hnd_team_id" value="<?php echo $hdn_team_id ?>">
                        
                            <table class="table table-striped table-bordered table-hover dataTable no-footer dataTable" style="border: 1px solid #CCC;border-collapse: collapse;" id="divisionlistingtable" >
                                <thead>
                                    <tr>
                                        <th style="width:150px;"> Division Id </th>
                                        <th> Division Name </th>
                                        <th> Actions </th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php 
								$i=1;
								if($_SESSION['logincheck']=='master'){		
									$selectCustId = $customerid  ;
								}else{
									$selectCustId = $LoginCustId;
								}
                                $dbQry = "select * from customer_division where custid in ($selectCustId)".$condition;
								$getResQry      =   $conn->prepare($dbQry);
                                // $QryArr = array(":ids"=>$ids);
                                $getResQry->execute();
                                $getResCnt      =   $getResQry->rowCount();
                                
                                if ($getResCnt > 0) {
                                    $TotalPages=ceil($getResCnt/$RecordsPerPage);
                                    $Start=($Page-1)*$RecordsPerPage;
                                    $sno=$Start+1;                                        
                                    $dbQry.=" limit $Start,$RecordsPerPage";
									$getResQry      =   $conn->prepare($dbQry);
                                    $getResQry->execute();
                                    $getResCnt      =   $getResQry->rowCount();

                                    if($getResCnt>0){
                                        $getResRows     =   $getResQry->fetchAll();
                                        
                                        $s=1;
										$customer_division_name='';
										$db_division_name =  '';
										$db_division_rule =  '';
                                        foreach ($getResRows as $division) {
											$sportname=$sportname?$sportname:$ls;
											?> 
                                            <tr>
                                                <td><?php echo $division['id']; ?></td>
                                                <td nowrap><?php echo $division['name'] ?></td>
                                                <td>
                                                    
													<?php 
													$division_id=$division['id'];
													$division_cust_id=$division['custid'];
													$DivisionQry = $conn->prepare("select * from customer_division where id='$division_id' and custid='$division_cust_id'");
													$DivisionQry->execute(); 
													$CntDivision = $DivisionQry->rowCount();
												   $DivisionResRow = $DivisionQry->fetchAll(PDO::FETCH_ASSOC);
														
													foreach($DivisionResRow as $DivisionRes) {
														
														$customer_division_name =  explode(" - ",$DivisionRes['name']);	$db_division_name =  $customer_division_name[0];
														$db_division_rule =  $customer_division_name[1];
													}
												   
											        ?>
												   <a href="#" id="edit_division" data-id="<?php echo  $division['id'];?>" data-name="<?php echo $db_division_name ?>"
												   data-role="<?php echo $db_division_rule;?>"  data-sports="<?php echo $sportname;?>" data-toggle="modal" class=" roundbtngreenedit btn-circle btn-icon-only edit_popup"  customerid="<?php echo $division['id']; ?>"><i class="icon-note trash_btn"></i>
													</a>
									                         
                                                </td>
                                            </tr>
                                        <?php $i++; 
											$s++;}
                                        } else {
                                           echo "<tr><td colspan='3' style='text-align:center;'>No Divisions found.</td></tr>";
                                        }
                                } else {
                                    echo "<tr><td colspan='3' style='text-align:center;'>No Divisions found.</td></tr>";
                                }?>
                                </tbody>
                            </table>
                            
                                <?php
                                if($TotalPages > 1){

                                echo "<tr><td style='text-align:center;' colspan='3' valign='middle' class='pagination'>";
                                $FormName = "customer_list";
                                require_once ("paging.php");
                                echo "</td></tr>";

                                }
                               ?>
                        </div>
                        </form>
                    </div><!--- portlet-body customerlist-tbl-pr clearfix !-->
                </div><!--- col-md-12 !-->
            </div><!--- row !-->
        </div>
    </div>
</div>

<div id="DivisionModal" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">							
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal">&times;</button>
		<h4 class="modal-title">Division Information<span style="margin-right: 10%;" class="mode-color" id="mode">Edit</span></h4>
	  </div>
	  <div class="modal-body">
	  <form name="divisionfrm" id="divisionfrm" method="POST" class="form-horizontal" novalidate="novalidate" action="">
        <input type="hidden" name="cust_divsion_id" id="cust_divsion_id" value="">
		<!-- <input type="hidden" name="editdivisionnew" id="editdivisionnew" value="Edit"> -->
		<!-- <input type="hidden" name="cust_divsion_id" id="cust_divsion_id" value=""> -->
		<!-- <input type="hidden" name="division_customer_id" id="division_customer_id" value="<?php echo $LoginCustId;?>"> -->
		<input type="hidden" name="role_name" id="role_name" value="">
		<input type="hidden" name="role_sports" id="role_sports" value="">
		<div class="col-md-10" style='margin:auto;float:none;'>
			<div class="form-group col-md-12 ">
				<label>Division Name<span class="error">*</span></label>
				<input class="form-control requiredcs border-radius" type="text" name="division_name" id="division_name" placeholder="Division Name" value="<?php echo $db_division_name;?>"/> 				
			</div>	
			<div class="form-group col-md-12 ">
				<label>Division Rule<span class="error">*</span></label>
				<select name="division_rulelist" class="form-control seldivinrule requiredunique border-radius" id="division_rulelist">
						<option value="">---Select---</option>
						<?php if($sportname=='basketball')
						{ ?>
						<option value="FIBA" <?php echo $db_division_rule == "FIBA" ? "selected":""?>>FIBA</option>
						<option value="HS" <?php echo $db_division_rule == "HS" ? "selected":""?>>HS</option>
						<option value="NBA" <?php echo $db_division_rule == "NBA" ? "selected":""?>>NBA</option>
						<option value="NCAA" <?php echo $db_division_rule == "NCAA" ? "selected":""?>>NCAA</option>
						<option value="NCAAW" <?php echo $db_division_rule == "NCAAW" ? "selected":""?>>NCAAW</option>
						<?php } ?>		
						<?php if($sportname=='baseball' || $sportname=='softball')
						{?>
							<option value="HS_BA" <?php echo $db_division_rule == "HS_BA" ? "selected":""?>>HS_BA</option>
							<option value="HS_SB" <?php echo $db_division_rule == "HS_SB" ? "selected":""?>>HS_SB</option>
							<option value="MLB" <?php echo $db_division_rule == "MLB" ? "selected":""?>>MLB</option>
							<option value="NCAA_BA" <?php echo $db_division_rule == "NCAA_BA" ? "selected":""?>>NCAA_BA</option>
							<option value="NCAA_SB" <?php echo $db_division_rule == "NCAA_SB" ? "selected":""?>>NCAA_SB</option>
						<?php } ?>

						<?php if($sportname=='football')
						{ ?>
							<option value="Arena" <?php echo $db_division_rule == "Arena" ? "selected":""?>>Arena</option>
							<option value="HS" <?php echo $db_division_rule == "HS" ? "selected":""?>>HS</option>
							<option value="Indoor" <?php echo $db_division_rule == "Indoor" ? "selected":""?>>Indoor</option>
							<option value="NCAA" <?php echo $db_division_rule == "NCAA" ? "selected":""?>>NCAA</option>
							<option value="NFL" <?php echo $db_division_rule == "NFL" ? "selected":""?>>NFL</option>
						<?php } ?>
				</select>
			</div>	
			<div class="form-group col-md-12 popupbtn">										
				<input class="btn addnewdivibtn btnpopupgreen" type="button" value="Update" id="mode_btn">
				<button class="btn cancelbtn btnpopupred" style="margin-left:15px;" type="button" data-dismiss="modal">Cancel</button>
			</div>	
		</div>
		</form>
	  </div>					 
	</div>
  </div>
</div>

<?php include_once('footer.php'); ?>
<script src="assets/custom/js/managedivision.js" ></script>




   