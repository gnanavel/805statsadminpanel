<?php 
include_once('session_check.php');
include_once('connect.php');

if (isset($_POST['divisionid'])) {
    $divisionid		= $_POST['divisionid'];
	$conferenceid	= $_POST['conferenceid'];
	$seasonid	    = $_POST['seasionid'];
	
	$QryArr			= array(":division_id"=>$divisionid,":conference_id"=>$conferenceid,":season_id"=>$seasonid,":customer_id"=>$MasterCustId);
	
//	delete from customer_conference_division where division_id=75 and conference_id=2 and season_id=41 and customer_id=105
	$delconfqry = $conn->prepare("delete from customer_conference_division where division_id=:division_id and conference_id=:conference_id and season_id=:season_id and customer_id=:customer_id");
	$delconfqry->execute($QryArr);

	$delteamqry = $conn->prepare("delete from customer_division_team where division_id=:division_id and conference_id=:conference_id and season_id=:season_id and customer_id=:customer_id");
    $delteamqry->execute($QryArr);


	$delplayerqry = $conn->prepare("delete from customer_team_player where division_id=:division_id and conference_id=:conference_id and season_id=:season_id and customer_id=:customer_id");
	$delplayerqry->execute($QryArr);

	echo "success";
	exit;
}
