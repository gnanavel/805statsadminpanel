<?php
include_once('session_check.php');
include_once('connect.php');

if(isset($_GET['seasonnamenew']) && !empty($_GET['seasonnamenew'])){		
	$test = '';
	$SeasonArr  = array();
	$seasonnamenew  = $_GET['seasonnamenew'];	
	$existsseason   = $_GET['existsseason'];
	$createdate  = date('Y-m-d H:i:s');

	$stmt		 = $conn->prepare("INSERT INTO customer_season (season_order,custid, name) select max(season_order)+1,$LoginCustId,'$seasonnamenew' from customer_season where custid=$LoginCustId");
    $stmt->execute();	
	$SeasonId = $conn->lastInsertId();

	if(isset($_GET['season'])){
		if(is_array($_GET['season'])){
			$SeasonArr  = array_filter($_GET['season']);
		}
	}
	
	if(count($SeasonArr)>0){
		foreach($SeasonArr as $Key=>$Value){
			
			if(is_array($Value)){		
				$ConfArr  = array_values( $Value);
				
				$ConfArrVal = explode("###",$ConfArr[0]);				
				$ConferenId  = $ConfArrVal[1];
				
				$stmt1		 = $conn->prepare("INSERT INTO customer_season_conference (season_id, conference_id,customer_id,status,created_date) VALUES (:season_id,:conference_id, :customer_id,:status,:created_date)");
				$stmt1->execute(array(':season_id'=>$SeasonId,':conference_id'=>$ConferenId,':customer_id' => $MasterCustId,':status' => 1, ':created_date'=>$createdate));

				foreach($Value as $Key1=>$Value1){
					$DivisionArr = explode("###",$Value1);
					$DivisionId  = $DivisionArr[0];
					$ConferenId  = $DivisionArr[1];					

					$stmt2		 = $conn->prepare("INSERT INTO customer_conference_division (season_id, conference_id,customer_id,division_id,status,created_date) VALUES (:season_id,:conference_id, :customer_id,:division_id,:status,:created_date)");
					$stmt2->execute(array(':season_id'=>$SeasonId,':conference_id'=>$ConferenId,':customer_id' => $MasterCustId,':division_id'=> $DivisionId,':status' =>1, ':created_date'=>$createdate));
					
					if(!empty($existsseason)){
						$stmt3		 = $conn->prepare("INSERT INTO customer_division_team (customer_id,season_id,conference_id,division_id,team_id,team_order,status,created_date,modified_date) SELECT customer_id,$SeasonId,conference_id,division_id,team_id,team_order,status,now(),now() from customer_division_team where customer_id=:customer_id and season_id=:season_id and conference_id=:conference_id and division_id=:division_id");			
						
						$QryCond  = array(':customer_id'=>$MasterCustId, ':season_id'=>$existsseason,':conference_id'=>$ConferenId,':division_id'=> $DivisionId);
						
						$stmt4		 = $conn->prepare("INSERT INTO customer_team_player (customer_id,season_id,conference_id,division_id,team_id,player_id,player_order,status,created_date,modified_date,isdelete) SELECT customer_id,$SeasonId,conference_id,division_id,team_id,player_id,player_order,status,now(),now(),isdelete from customer_team_player where customer_id=:customer_id and season_id=:season_id and conference_id=:conference_id and division_id=:division_id");				

						$stmt3->execute($QryCond);
						$stmt4->execute($QryCond);
					}
				}

			}else{			
				$ConferenceArr = explode("###",$Value);
				$ConferenId    = $ConferenceArr[0];
					
				$stmt3		   = $conn->prepare("INSERT INTO customer_season_conference (season_id, conference_id,customer_id,status,created_date) VALUES (:season_id,:conference_id, :customer_id,:status,:created_date)");
				$stmt3->execute(array(':season_id'=>$SeasonId,':conference_id'=>$ConferenId,':customer_id' =>$MasterCustId,':status'=> 1,':created_date'=>$createdate));						
			}
		}
	}	
	
	$Qry		= $conn->prepare("select * from customer_season where custid in($customerid) order by season_order DESC");	
	$Qry->execute();
	$QryCntSeason = $Qry->rowCount();

	$DivisionWrapHtml= $AddNewSeasonTree='';
	$Inc =0;
	if ($QryCntSeason > 0) {
		while ($row = $Qry->fetch(PDO::FETCH_ASSOC)){
			
			$QryExe = $conn->prepare("select * from customer_season_conference as seasonconf LEFT JOIN customer_conference as custconf ON  seasonconf.conference_id=custconf.id where season_id=:season_id order by  seasonconf.conf_order ASC");
			$Qryarr = array(":season_id"=>$row['id']);
			$QryExe->execute($Qryarr);
			$QryCntSeasonconf	= $QryExe->rowCount();											
			$SeletedArrConf		= array();
			$SeletedArrDiv		= array();
			$AddNewSeason = $AddSeasontbl = $Conferencetbl= '';
			$TreeLine=45;
			if ($QryCntSeasonconf > 0) {
				while ($rowSeason = $QryExe->fetch(PDO::FETCH_ASSOC)){												
					$QryExeDiv = $conn->prepare("select * from customer_conference_division as seasonconfdiv LEFT JOIN customer_division as custconf ON  seasonconfdiv.division_id=custconf.id where seasonconfdiv.conference_id=:conference_id and season_id=:season_id order by seasonconfdiv.div_order ASC");
					$QryarrCon = array(":conference_id"=>$rowSeason['conference_id'],":season_id"=>$row['id']);
					$QryExeDiv->execute($QryarrCon);
					$QryCntSeasonconf = $QryExeDiv->rowCount();
					$Divisiontbl = $AddNewSeason='';
					$TreeLine=48+($QryCntSeasonconf*44)-21;

					while ($rowSeasonDiv = $QryExeDiv->fetch(PDO::FETCH_ASSOC)){													
						$Selected = ($rowSeasonDiv['status'])?'checked':'';
						$Divisiontbl .= "<div class='divisions' data-divid='".$rowSeasonDiv['id']."'><table class='table innerdivtable'><tr><td class='divisionbtns'><span class='divisioncircle circle'>D</span><label class='mt-checkbox'><input type='checkbox' name='division[]'  class='division-checked' value='".$rowSeasonDiv['id']."' $Selected> ".$rowSeasonDiv['name']."<span></span></label><a class='btn btn-circle btn-icon-only btn-default red deletebtndiv tooltips' href='javascript:;' data-container='body' data-placement='top' data-original-title='Delete division' data-divisionid='".$rowSeasonDiv['id']."' data-conferenceid='".$rowSeason['id']."' data-seasionid='".$row['id']."' ><i class='icon-trash'></i></a><a href='add_divisionteam.php?divisionid=".$rowSeasonDiv['id']."&conferenceid=".$rowSeason['id']."&seasonid=".$row['id']."' class='btn btn-circle btn-icon-only btn-default green adddivisionbtn tooltips'  data-conferencename='".$rowSeason['conference_name']."' data-container='body' data-placement='top' data-original-title='Add Team'><i class='fa fa-plus'></i></a></td></tr></table></div>";	
						$SeletedArrDiv[] = $rowSeasonDiv['id'];
						
						$AddNewSeason .= "<table class='table innerdivtable'><tr><td class='divisionbtns'><span class='divisioncircle circle'>D</span><label class='mt-checkbox'><input type='checkbox' class='division-checked'name='season[conference".$rowSeason['id']."][division".$rowSeasonDiv['id']."]' value='".$rowSeasonDiv['id']."###".$rowSeason['id']."' $Selected> ".$rowSeasonDiv['name']."<span></span></label></td></tr></table>";		
					}

					$Selected = ($rowSeason['status'])?'checked':'';
					$Conferencetbl .= "<div class='conferencetbltoggle' data-conf='".$rowSeason['id']."'><table class='table innertable' id='innertblid".$rowSeason['id']."'><tr><td class='conferencebtns '><span class='conferencecircle circle'>C</span><label class='mt-checkbox '><input type='checkbox' name='' class='conference-checked' value='".$rowSeason['id']."' $Selected> ".$rowSeason['conference_name']."<span></span></label><a class='btn btn-circle btn-icon-only btn-default red deletebtnconf tooltips' href='javascript:;' data-container='body' data-placement='top' data-original-title='Delete conference' data-conferenceid='".$rowSeason['id']."' data-seasonid='".$row['id']."'><i class='icon-trash'></i></a><a class='btn btn-circle btn-icon-only btn-default blue managedivisionbtn tooltips' href='javascript:;' data-container='body' data-placement='top' data-original-title='Manage division'  data-conferenceid='".$rowSeason['id']."' data-seasonid='".$row['id']."'><i class='icon-wrench'></i></a><a class='btn btn-circle btn-icon-only btn-default green adddivisionbtn tooltips' href='javascript:;' data-container='body' data-placement='top' data-original-title='Add division' data-toggle='modal' data-target='#DivisionModal' data-conferenceid='".$rowSeason['id']."' data-seasonid='".$row['id']."' data-conferencename='".$rowSeason['conference_name']."'><i class='fa fa-plus'></i></a></td></tr></table><div class='divisiontbltoggle'>".$Divisiontbl."</div></div>";	

					$AddSeasontbl .= "<table class='table innertable' id='innertblid".$rowSeason['id']."'><tr><td class='conferencebtns '><span class='conferencecircle circle'>C</span><label class='mt-checkbox'><input type='checkbox' name='season[conference".$rowSeason['id']."]' value='".$rowSeason['id']."###".$row['id']."' $Selected class='conferencechkbox conference-checked'> ".$rowSeason['conference_name']."<span></span></label></td></tr></table>".$AddNewSeason;

					$SeletedArrConf[] = $rowSeason['id'];
					$SeletedArrDiv		= array();
				}				
			}			


			$SeasonTree = "<table class='table  ms_seasontble  ms_seasontble msseasontbleborder_".$row['id']."' data-seasion='".$row['id']."'><tr><td><span class='seasoncircle circle'>S</span><label class='mt-checkbox'><input type='checkbox' name='' value='".$row['id']."' checked='checked' disabled> ".$row['name']."<span></span></label><a class='btn btn-circle btn-icon-only btn-default red deleteseasonbtn tooltips' href='javascript:;' data-container='body' data-placement='top' data-original-title='Delete season' data-seasonid='".$row['id']."'><i class='icon-trash' ></i></a><a class='btn btn-circle btn-icon-only btn-default blue    manageconferencemodel tooltips' href='javascript:;'  data-seasonid='".$row['id']."' data-container='body' data-placement='top' data-original-title='Manage conference'><i class='icon-wrench'></i></a><a class='btn btn-circle btn-icon-only btn-default green addconferencebtn tooltips' href='javascript:;' data-toggle='modal' data-target='#ConferenceModal' data-seasonid='".$row['id']."' data-seasonname='".$row['name']."' data-container='body' data-placement='top' data-original-title='Add conference'><i class='fa fa-plus'></i></a><div class='parent_confernce'>$Conferencetbl</div></td></tr></table>";


			$AddNewSeasonTree .= "<div class='mt-radio-list clearfix'><label class='mt-radio'><input type='radio' name='seasonnamesnewid' value='".$row['id']."' class='seasonnamesnew'>".$row['name']."<span></span></label></div><table class='table  ms_seasontble popupmodelseason'><tr><td><form method='POST' class='selectedseasontree' novalidate='novalidate'><span class='seasoncircle circle'>S</span><label class='mt-checkbox'><input type='checkbox' name='seasonnames[]' value='".$row['id']."' checked disabled> ".$row['name']."<span></span></label>$AddSeasontbl</form></td></tr></table>";		
			
			if($Inc==0){				
				$DispCond    = "";
			}else{				
				$DispCond    = "display:none;";
			}

			if($TreeLine==27){
				$TreeLine=46;
			}
			
			$BorderStyleCss = '<div id="msseasontbleborder_'.$row['id'].'" style="display:none;"><style>table.msseasontbleborder_'.$row['id'].'>tbody>tr>td:after{bottom: '.$TreeLine.'px;}</style></div>';

			echo '<div class="col-md-12 col-sm-12 seasonwrapcont" id="seasonmaincont_'.$row['id'].'">'.$BorderStyleCss.'<div class="portlet box grey seasontbltogglewrap">
				<div class="portlet-title">
					<div class="caption tools" style="width: 98%;">
						<a href="javascript:;" class="expand" style="color:#000;background-image:none;display: block;width: 100%;"> '.$row['name'].'</a>
					</div>												
					<div class="tools">
						<a href="javascript:;" class="expand" style=""></a>
					</div>
				</div>
				<div class="portlet-body seasontbltoggle" style="'.$DispCond.'">'.$SeasonTree.'	
				<div id="conferewrap_'.$row['id'].'" class="hide">'.$conference.'</div>'.$DivisionWrapHtml.'
				</div>
			</div></div>';
			$conference='';
			$Inc++;
		} 				
	}
	exit;
}

?>	
