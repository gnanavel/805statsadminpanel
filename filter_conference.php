<?php 
include_once('session_check.php'); 
include("connect.php");

if(isset($_REQUEST["conferencename"])){
	$RecordsPerPage=$_REQUEST["PerPage"];
	//$HdnMode=$_REQUEST["HdnMode"];
	//$HdnPage=$_REQUEST["HdnPage"];
	$Page=1;
	//$Page=1;
	$conferencename     =  $_REQUEST['conferencename'];

	if (isset($_SESSION["sportid"])) {	    
	    $sportid = $_SESSION["sportid"];
	    $sportname = $_SESSION["sportname"];
	}

	?>
	<form id="frm_confrerence_list" name="frm_confrerence_list" method="post" action="">
	<input type="hidden" name="HdnPage" id="HdnPage" value="<?php echo $Page; ?>">
	<input type="hidden" name="HdnMode" id="HdnMode" value="<?php echo $Page; ?>">
	<input type="hidden" name="RecordsPerPage" id="RecordsPerPage" value="<?php echo $RecordsPerPage; ?>">
	<input type="hidden" name="hdnsearch" id="hdnsearch" value="<?php echo $conferencename; ?>">
	<table class="table table-striped table-bordered table-hover dataTable no-footer dataTable" id="sample_1">
	<thead>
		<tr>
			<th style="width:150px;"> Conference Id </th>
	        <th> Conference Name </th>      
	        <th> Actions </th>
		</tr>
	</thead>
	<tbody>
	<?php

	if ($_SESSION['logincheck'] == 'master') {
        $customer_id = $customerid;
    } else {
        $customer_id = $LoginCustId;
    }	
	$confCondtn = "";
	if (!empty($conferencename)) {
		$confCondtn = "and conference_name like '%$conferencename%'";
	}
		
	$res = "select * from customer_conference where customer_id in ($customer_id) $confCondtn";
    $getResQry      =   $conn->prepare($res);
    $getResQry->execute();
    $getResCnt      =   $getResQry->rowCount();
    	$TotalPages = '';
	    if($getResCnt>0){
	        $TotalPages=ceil($getResCnt/$RecordsPerPage);
	        $Start=($Page-1)*$RecordsPerPage;
	        $sno=$Start+1;
	            
	        $res.=" limit $Start,$RecordsPerPage";
	                
	        $getResQry      =   $conn->prepare($res);
	        $getResQry->execute($QryArr);
	        $getResCnt      =   $getResQry->rowCount();
		    if($getResCnt>0){
		        $getResRows     =   $getResQry->fetchAll();
		        $s=1;
	        foreach($getResRows as $conference){
			?>
				<tr>
	                <td><?php echo $conference['id'] ?></td>
	                <td nowrap><?php echo $conference['conference_name'] ?></td>
					<td>
						<?php 
						$division_id = $conference['id'];
						$ConfQry = $conn->prepare("select * from customer_conference where id='$division_id' and customer_id in ($customer_id)");
						$ConfQry->execute(); 
						$CntConf = $ConfQry->rowCount();
					    $ConfResRow = $ConfQry->fetchAll(PDO::FETCH_ASSOC);
							
						foreach($ConfResRow as $ConfRes) {
							
							$customer_conf_name =  explode(" - ",$ConfRes['name']);	$db_division_name =  $customer_conf_name[0];
							$db_division_rule =  $customer_conf_name[1];
						}				   
					   ?>				   
						<a href="#" id="edit_division" data-id="<?php  echo base64_encode($conference['id']); ?>" data-name="<?php echo $conference['conference_name'];?>"
	                    data-sport="<?php echo $sportname; ?>" data-toggle="modal" class="roundbtngreenedit btn-circle btn-icon-only edit_popup"  data-cid="<?php echo $customer_id;?>" customerid="<?php echo $conference['id']; ?>"><i class="icon-note trash_btn"></i>
	                    </a>												
									
	                </td>
	            </tr>

			<?php
			$s++;
			}
		} else {
	        echo "<tr><td colspan='3' style='text-align:center;'>No Conference(s) found.</td></tr>";
	    }
    } else {
        echo "<tr><td  colspan='3' style='text-align:center;'>No Conference(s) found.</td></tr>";
    }
		
}?>
 </tbody>
 </table>
<?php
	if($TotalPages > 1){

	echo "<tr><td style='text-align:center;' colspan='3' valign='middle' class='pagination'>";
	$FormName = "frm_confrerence_list";
	require_once ("paging.php");
	echo "</td></tr>";

	}
?>
</form>





