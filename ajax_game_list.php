<?php 
include_once('session_check.php'); 
include_once("connect.php");
include_once('common_functions.php');
include_once('usertype_check.php');

if (isset($_REQUEST['Searchbyteam'])) {

	$season = $_REQUEST['searchbyseason'];
    $Searchbyteam  = $_REQUEST['Searchbyteam'];
    $SearchbyteamID  = $_REQUEST['Searchbyteamid'];
    $monthyear = $_REQUEST['monthyear'];
    $gameDate = ($_REQUEST['gamedate'])?$_REQUEST['gamedate']:"";

    if (isset($_SESSION["sportid"])) {
        $sportid = $_SESSION["sportid"];
        $sportname = $_SESSION["sportname"]; 
    }

    $SeleCustId = $LoginCustId;
    if ($_SESSION['logincheck'] == 'master') {
        $SeleCustId = $customerid;            
    }

    $teamlogin_id = '';
    $team_id = '';
    if ($_SESSION['team_manager_id']) {
        $teamlogin_id = $_SESSION['team_manager_id'];
        $team_id = " AND (visitor_team_id=$teamlogin_id OR home_team_id=$teamlogin_id)";
    }    
    $teaminfoCondn = "";
    if (!empty($Searchbyteam))
       $teaminfoCondn.= " AND team_name like '%{$Searchbyteam}%'";
    if (!empty($SearchbyteamID))
       $teaminfoCondn.= " AND id='$SearchbyteamID'";

    // if (!empty($Searchbyteam) && empty($SearchbyteamID)) {
    //     $teaminfoCondn = " team_name like '%{$Searchbyteam}%'";
    // } else if (empty($Searchbyteam) && !empty($SearchbyteamID)) {
    //     $teaminfoCondn = " id='$SearchbyteamID'";
    // } else if (!empty($SearchbyteamID) && !empty($Searchbyteam)) {
    //     $teaminfoCondn = " team_name like '%{$Searchbyteam}%' OR id='$SearchbyteamID'";
    // } else {
    //     $teaminfoCondn = "";
    // }

    $implodeTeamId = "";
    $teamCondtn = " AND (visitor_team_id in ($implodeTeamId) OR home_team_id in ($implodeTeamId))"; 

    $resTeam = $conn->prepare("select * from teams_info where id<>'' $teaminfoCondn and customer_id in ($SeleCustId)");
    $resTeam->execute();
    $CntresTeam = $resTeam->rowCount();        
    if ($CntresTeam > 0) {
        $FetchRows = $resTeam->fetchAll(PDO::FETCH_ASSOC);
        foreach ($FetchRows as $rowSeason) {
            $teamid[] =  $rowSeason['id'];
        }
        $implodeTeamId = implode(",", $teamid);
        $teamCondtn = " AND (visitor_team_id in ($implodeTeamId) OR home_team_id in ($implodeTeamId))";
    } 

    $DateCondn = "";
    if(!empty($gameDate)) {      
        $DateCondn = " and date_format(STR_TO_DATE(date, '%m/%d/%Y'),'%M, %Y') = '$gameDate'";
    } else {
        $DateCondn = " and date_format(STR_TO_DATE(date, '%m/%d/%Y'),'%M, %Y') = '$monthyear'";
    }

    $seasonCondn = !empty($season) ? "and season='$season'" : "";   
    $sportIdcondn = " and (sport_id='$sportid')";     

    $res = $conn->prepare("select * from games_info where (home_customer_id in ($SeleCustId) or visitor_customer_id in ($SeleCustId)) $teamCondtn $sportIdcondn   $seasonCondn $team_id $DateCondn ORDER BY STR_TO_DATE(date, '%m/%d/%Y'),time");
	// echo "select * from games_info where (home_customer_id in ($SeleCustId) or visitor_customer_id in ($SeleCustId)) $teamCondtn $sportIdcondn   $seasonCondn $team_id $DateCondn ORDER BY STR_TO_DATE(date, '%m/%d/%Y'),time";
    $res->execute();
	$resCnt = $res->rowCount();

	if ($resCnt > 0) {
	    $GameRes = $res->fetchAll(PDO::FETCH_ASSOC);
	    $GameDetail = array();
	    $j = 0;
	    foreach ($GameRes as $GameRow) {
	        $GameYear = date("Y", strtotime($GameRow['date']));
	        $GameMonth = date("m", strtotime($GameRow['date']));
	        $MatchDetail = $GameRow['visitor_team_id']. " vs ".$GameRow['home_team_id'];
	        $GameDetail[$GameYear][$GameMonth][$j] = $GameRow;
	        $j++;
	    }  
	
		?>		
        <div id="myCarousel" class="carousel slide" data-ride="carousel" data-interval="false">
            
            <div class="carousel-inner" role="listbox">
            <?php 
            foreach ($GameDetail as $gameDetailYear => $GameDetailYearVal) {                        
                foreach ($GameDetailYearVal as $GameDetailMonth => $GameDetailMonthInfo) {
                $first_key = key($GameDetailMonthInfo); 
                $month = date("F", mktime(0,0,0,$GameDetailMonth+1,0,0));
                ?>
                <div class="item <?php if ($first_key == 0) {echo "active"; }?>" month-name="<?php echo $month.", ".$gameDetailYear; ?>" >
                <input type="hidden" id="hiddenmonth" value="<?php echo date('F', strtotime($GameDetailMonth)); ?>">
                <?php 
                    foreach ($GameDetailMonthInfo as $Keys => $Values) { 
                        $VisitName = json_decode(getTeamName($Values['visitor_team_id']), true);
                        $HomeName = json_decode(getTeamName($Values['home_team_id']), true);
                        $DivisionName = json_decode(getDivisionName($Values['division']), true);
                        $GameType = json_decode(getGameType($Values['isLeagueGame']), true);
                        $TournamentName = ($Values['isLeagueGame']) ? $Values['isLeagueGame'] : " " ;

                        $enableModify = "display:none;";
                        if ($_SESSION["usertype"] == "team_manager") {
                            $start_date = new DateTime(date("Y-m-d H:i:s", strtotime($Values["date"]." ".$Values["time"])));
                            $end_date = new DateTime(date("Y-m-d H:i:s"));
                            $interval = $start_date->diff($end_date);
                            if ($interval->y == 0 && $interval->m == 0 && $interval->d <= 30) {
                                $diffDay = $interval->d;
                                $enableModify = "";
                            }
                        }

                        // $SportQry = $conn->prepare("SELECT subsports.sport_id,sports.sport_name FROM customer_subscribed_sports as subsports left join sports on subsports.sport_id=sports.sportcode where customer_id=:customer_id");
                        // $SportQryArr = array(":customer_id"=>$cid);
                        // $SportQry->execute($SportQryArr);
                        // $FetchSportName = $SportQry->fetch(PDO::FETCH_ASSOC);
                        // $sportname = $FetchSportName["sport_name"];
                        ?>

                            <div class="panel-body rc-padding <?php echo $Values['date'];?>">
                                <div class="row rm-margin opencloseportletcaption">                                
                                    <div class="col-md-12 rm-padding">
                                        <div class="portlet box grey portletdown">
                                            <div class="portlet-title">
                                                <div class="caption captionstyle tools">
                                                    <a href="javascript:;" class="expand" id="shortgamedetail">
                                                        <?php echo $VisitName." vs ".$HomeName." "; ?><small><?php echo "&nbsp;&nbsp;&nbsp;".date("F d, Y", strtotime($Values['date'])); echo " ".$Values['time']; ?></small>
                                                    </a>
                                                </div>
                                                <div class="tools">
                                                    <a href="javascript:;" class="expand" data-original-title="" title=""> </a>
                                                </div>
                                           </div>
                                            <div class="portlet-body portlet-custom expand opencloseportlet">
                                                <div class="slimScrollDiv" >
                                                    <div class="scroller removerightpaddingtable"  data-always-visible="1" data-rail-visible="1" data-rail-color="blue" data-handle-color="red" data-initialized="1">
                                                        <div class="col-md-12 rm-padding table-responsive">
                                                            <table class="table table-hover table-bordered gameinfotable">
                                                                <tr>
                                                                    <th nowrap ><strong>Game Id</strong></th>
                                                                    <th nowrap><strong>Game Name</strong></th>
                                                                    <th nowrap><strong>Visitor Team</strong></th>
                                                                    <th nowrap><strong>Home Team</strong></th>
                                                                    <th nowrap><strong>Division</strong></th>
                                                                    <th nowrap><strong>Game Type</strong></th>
                                                                    <th nowrap><strong>Location</strong></th>
                                                                    <th nowrap><strong>Tournament</strong></th>
                                                                    <th nowrap><strong>Action</strong></th>
                                                                </tr>
                                                                <tr>
                                                                    <td nowrap><?php echo $Values['id']; ?></td>
                                                                    <td nowrap><?php echo $Values['game_name']; ?></td>
                                                                    <td nowrap><?php echo $VisitName; ?></td>
                                                                    <td nowrap><?php echo $HomeName; ?></td>
                                                                    <td nowrap><?php echo $DivisionName; ?></td>
                                                                    <td nowrap><?php echo $GameType; ?></td>
                                                                    <td nowrap><?php echo $Values['game_location']; ?></td>
                                                                    <td nowrap><?php echo $Values['tournament']; ?></td>                                                                    
                                                                    <td nowrap>
                                                                        <div class="actions">
                                                                            <a title="Edit game" href="manage_game.php?gid=<?php echo base64_encode($Values['id']); ?>" class="roundbtngreenedit btn-circle btn-icon-only green_btn">                     
                                                                            <i class="icon-note trash_btn"></i></a>&nbsp;&nbsp;
                                                                            <?php 
                                                                            $gameid = $Values['id']; 
                                                                            $CheckGamedetailQry = $conn->prepare("SELECT id FROM game_details WHERE xml_game_id=:xml_game_id");
                                                                            $CheckGamedetailQryArr = array(":xml_game_id"=>$gameid);
                                                                            $CheckGamedetailQry->execute($CheckGamedetailQryArr);
                                                                            $CntGamedetails = $CheckGamedetailQry->rowCount();
                                                                            if ($CntGamedetails == 0) { ?>
                                                                                <a title="Delete game" class="roundbtnreddelete btn-circle btn-icon-only red_btn" onclick="return deleteGame('<?php echo base64_encode($Values['id']); ?>','<?php echo $sportname; ?>');">
                                                                                <i class="icon-trash trash_btn" ></i> </a>&nbsp;&nbsp;
                                                                            <?php } 
                                                                            if ($sportname == 'basketball') {
                                                                                if ($_SESSION["usertype"] != "team_manager") {
                                                                            ?>
                                                                            <a href="game_stats.php?gid=<?php echo base64_encode($Values['id']); ?>" title="Modify game" class="roundbtnyellow btn-circle btn-icon-only modifystatsbtn yellow_btn">
                                                                                <i class="icon-doc" ></i>
                                                                            </a>&nbsp;&nbsp;
                                                                            <?php } else { ?>
                                                                                 <a style="<?php echo $enableModify; ?>" href="game_stats.php?gid=<?php echo base64_encode($Values['id']); ?>" title="Modify game" class="roundbtnyellow btn-circle btn-icon-only modifystatsbtn yellow_btn modifystatsbtn">
                                                                                <i class="icon-doc" ></i> 
                                                                                </a>&nbsp;&nbsp;

                                                                            <?php } 
                                                                            } 
                                                                            ?>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                
                                                            </table>
                                                        </div>                                                
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>                                
                                </div>
                            </div>
                    <?php } ?>
                </div><?php
                
                }
            }
            ?>
            </div>
            <!-- <a class="left carousel-control carouselstle" href="#myCarousel" role="button" data-slide="prev">
                <i class='fa fa-chevron-left prev'></i>
                <span class="sr-only">Previous</span>

            </a>
            <a class="right carousel-control carouselstle" href="#myCarousel" role="button" data-slide="next">
                <i class='fa fa-chevron-right next'></i>
                <span class="sr-only">Next</span>
            </a> -->
        </div>
<?php } else {   ?>
<div id="myCarousel" class="carousel slide" data-ride="carousel" data-interval="false">

    <!-- <div class="col-md-12 mycarousalnew">
        <div class="col-md-4 col-sm-4 col-xs-4 logames ">
            <i class="fa fa-list font-red-sunglo"></i><span class="caption-subject font-red-mint bold uppercase "> List of games</span>                        
        </div>
        <div class="col-md-4 col-sm-4 col-xs-4 monthrcds">        
            <div class="gamemonth"><strong ><?php echo $gameMonth.", ".$gamesplit[1];?></strong>
            <input type="hidden" id="datepicker" value=""><i class="fa fa-calendar" id="datepicker-button"></i>
            </div>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-4 logamesbtn">
            <a href="manage_game.php" class="btn btn-small addcustomerbtn">
            Add Game </a>
            <a href="add_bulk_game.php?sport=<?php echo $SportName; ?>" style="margin-right:10px;" class="btn btn-small addcustomerbtn" >
            Add Bulk Game </a>
        </div>    
    </div> -->
    <!-- <div class="col-md-12 col-sm-12 col-xs-12 ">
        <div class="col-md-4 col-sm-4 col-xs-4 logames ">
        </div>
        <div class="col-md-4 col-sm-4 col-xs-4 monthrcds">
            <div class="gamemonth">
            <strong class=""></strong>
            <input type="hidden" id="datepicker" value=""><i class="fa fa-calendar" id="datepicker-button"></i>
            </div>                            
        </div>
        <div class="col-md-4 col-sm-4 col-xs-4 logamesbtn">
        </div>
    </div> -->
    <div class="carousel-inner" role="listbox">
        
        <div class="item active" month-name="<?php echo (!empty($gameDate))? $gameDate : date("F, Y") ; ?>">
            <div class="panel-body rc-padding carousel-inner ajaxcarousel-inner">
                <div class="row">                                
                    <div class="col-md-12">
                        <div class="portlet box grey portletdown">
                            <div class="portlet-title">
                                <div class="caption captionstyle nogamescaption" style="color:#000;background-image:none;display: block !important;width: 100%">
                                    No Game(s) found</div>
                                                    
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- <a class="left carousel-control carouselstle" href="#myCarousel" role="button" data-slide="prev">
        <i class='fa fa-chevron-left prev'></i>
        <span class="sr-only">Previous</span>

    </a>
    <a class="right carousel-control carouselstle" href="#myCarousel" data-container="body" role="button" data-slide="next">
        <i class='fa fa-chevron-right next'></i>
        <span class="sr-only">Next</span>
    </a> -->
    <?php }  }?>    
</div>
<script>

// var gamemonth = '';
// var initialloadmonth = '';
// $(document).ready(function() {

//     initialloadmonth = $('.item.active').attr('month-name');
    
//     if (initialloadmonth != "")
//         $('.gamemonth strong').text(initialloadmonth);
//     $(document).on( "click", ".right" , function() {
//         var nextMonthName = $('.item.active').next().attr('month-name');        
//         $('.gamemonth strong').text(nextMonthName);
//         if (nextMonthName == undefined) {
//             $('.gamemonth strong').text($('div.item').first().attr('month-name'));
//         }
//     });

//     $(document).on( "click", ".left" , function() {       
//         var prevMonthName = $('.item.active').prev().attr('month-name');
//         $('.gamemonth strong').text(prevMonthName); 
//         if (prevMonthName == undefined) {
//             $('.gamemonth strong').text($('div.item').last().attr('month-name'));
//         }    
//     });
 
// });
</script>



