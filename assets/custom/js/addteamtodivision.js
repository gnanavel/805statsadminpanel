$(document).ready(function() {
	$('#undo_redo').multiselect({
		sort:false,
        search: {
            left: '<input type="text" name="q" class="form-control searchteambox" placeholder="Search Team" /><label>Select Team</label>',
            right: '<p class="clearfix" style="    margin-bottom: 3px;"><a href="manage_season.php"><button type="button" class="btn uppercase pull-left backbtnred">Back</button></a><button type="button" class="btn uppercase addplayerbtntop" style="float: right;">Assign Players</button></p><p class="clearfix" style="    margin-bottom: 0px;"><label>Selected Team</label></p>',
        },
		//rightSelected:true,
		afterMoveToRight: function($left, $right, $options) { }
    });


$(document).on('click','#addteambtnid', function(evt) {
	if (!$("#addteamform").validate()) { 
		return false;
	} 	
	$("#addteamform").submit();		
});


$.validator.addMethod('requiredcs',function(value){
	if(value==''){
		return false;
	}else{
		return true;
	}
},""
);


$("#addteamform").validate({
	 rules: {
		 conferencelist:{requiredcs :true},
		 divisionlist:{requiredcs:true},
		 "selectedteam[]":{requiredcs:true},
		 },
		messages: {
		 conferencelist:{requiredcs:"Please select conference"},
		 divisionlist:{requiredcs:"Please select division"},
		 "selectedteam[]":{requiredcs:"Please add team"},
	   },
		submitHandler: function (form) {			
			var $form = $(form);
			var FormArr  = $form.serialize();	
			$("#undo_redo_to option").each(function()
			{
				$(this).prop('selected', true);
			});

			var seasonlist       =  $('#seasonlist').val();	
			var conferencelist   =  $('#conferencelist').val();
			var divisionlist     =  $('#divisionlist').val();
			var selectedteam     =  $('#undo_redo_to').val();
			if(selectedteam==""){
				return false;
			}
           
			$(".rightsidewrap").hide();
			$(".loadingwrap").show();
				var result=$.ajax({
				 type:"POST",			
				 url:"assignteamtoseason.php",
				 data:{"seasonlist":seasonlist,"conferencelist":conferencelist,"divisionlist":divisionlist,"selectedteam":selectedteam},
				 success: function(data) {
				 	//alert(result.responseText);
				    $(".rightsidewrap").show();
					$(".loadingwrap").hide();
					$( ".page-content").empty().append(result.responseText);
					return false;
				 },
				 error: function(data) {
					alert("something wrong");
					
				 },
				}); 		 
            }

	});

});

 $(document).on('click','.addplayerbtntop', function() {
 	var selOptEle = $("select#undo_redo_to option").length;
 	if (selOptEle > 1)
 		window.location.href="add_divisionplayer.php";
 	else {
       alert("please add team");
       return false;
 	}
 
     
 });

$(document).on('change','#seasonlist', function(evt) {
	var $this          =  $(this);	
	var SelectAttrid   =  $this.attr("id");
	var post_type      =  '';
	var SeasonId       =  '';	
	var ConferenceId   =  '';
	var DivisionId     =  '';	

	
	post_type      =  "seasonlist";
	SeasonId       =  $('#seasonlist').val();	
		
	
	$.ajax({
		 type:"POST",
		 async:false,
		 url:"selectseasonlist.php",
		 data:{"post_type":post_type,"divisionid":DivisionId,"conferenceid":ConferenceId,"seasonid":SeasonId},
		 dataType:"text",
		 success: function(data) {		  
		   $('#conferencelist').empty().append(data);
			$('#divisionlist').empty().append("<option value=''>Select</option>");
			return false;
		 },
		 error: function(data) {
			alert("something wrong");
			
		 },
	 }); 	
		
});



$(document).on('change','#conferencelist', function(evt) {
	var $this          =  $(this);	
	var SelectAttrid   =  $this.attr("id");
	var post_type      =  '';
	var SeasonId       =  '';	
	var ConferenceId   =  '';
	var DivisionId     =  '';	

	post_type      =  "conferencelist";
	SeasonId       =  $('#seasonlist').val();
	ConferenceId   =  $('#conferencelist').val();

	var result=$.ajax({
		 type:"POST",
		 async:false,
		 url:"selectseasonlist.php",
		 data:{"post_type":post_type,"divisionid":DivisionId,"conferenceid":ConferenceId,"seasonid":SeasonId},
		 dataType:"text",
		 success: function(data) {		 
		   $('#divisionlist').empty().append(data);	
		   $('#divisionlist').trigger('change');
			return false;
		 },
		 error: function(data) {
			alert("something wrong");
			
		 },
	 }); 		
});



$(document).on('change','#divisionlist', function(evt) {
	var $this          =  $(this);	
	var SelectAttrid   =  $this.attr("id");
	var post_type      =  '';
	var SeasonId       =  '';	
	var ConferenceId   =  '';
	var DivisionId     =  '';	

	post_type      =  "divisionlist";
	SeasonId       =  $('#seasonlist').val();	
	ConferenceId   =  $('#conferencelist').val();
	DivisionId     =  $('#divisionlist').val();
	
	$.ajax({
		 type:"POST",
		 async:false,
		 url:"selectseasonlist.php",
		 data:{"post_type":post_type,"divisionid":DivisionId,"conferenceid":ConferenceId,"seasonid":SeasonId},
		 dataType:"text",
		 success: function(data) {	  
		   $('.addteammainwrap').empty().append(data);		
			return false;
		 },
		 error: function(data) {
			alert("something wrong");			
		 },
	 }); 
});