//Function used to reload conference
$(document).on('click','.managedivpopbtn', function(evt) {
	var $this = $(this);
	var anyBoxesChecked = false;
	var SeasonIdChk = $("#seasioniddivid").val();
    $(this).closest('#managedivformid').find('input[type="checkbox"]').each(function() {
		 if ($(this).is(":checked")) {
            anyBoxesChecked = true;
        }
    });
 
    if (anyBoxesChecked == false) {
        alert("Please check at least one checkbox");
        return false;
    } 
	

	var FormArr  = $this.closest("#managedivformid").serialize();
	var SeasionId = $("#seasioniddivid").val();
	$this.closest("#managedivformid").hide();
	$this.closest(".modal-body").find("#loadingdivison").show();
	$( "#seasonmaincont_"+SeasionId).load( "update_confrence.php?"+FormArr, function( response, status, xhr ) {
		DragAndDropOnLoad();
		//$('#msseasontbleborder_'+SeasonIdChk).empty();

		$("#loadingdivison").hide();
		$( "#managedivinmsg" ).show();
		
	});
});


$(document).ready(function () {

$.validator.addMethod('uniquediv',function(value)
 {
  var divisionname=value;  
  var post_type="divisionnamecheck";
  var result=$.ajax({
		 type:"POST",
		 async:false,
		 url:"nameexistscheck.php",
		 data:{"post_type":post_type,"divisionname":divisionname},
		 dataType:"text",
	 });    
	  if(result.responseText=="not exists"){
	   return true;
	  }
	  else{ 
		return false;
	  }
  },""
);

$.validator.addMethod('uniqueconf',function(value)
 {
  var conferencename=value;  
  var post_type="conferencenamecheck";
  var result=$.ajax({
		 type:"POST",
		 async:false,
		 url:"nameexistscheck.php",
		 data:{"post_type":post_type,"conferencename":conferencename},
		 dataType:"text",
	 }); 
		
	  if(result.responseText=="not exists"){
	   return true;
	  }
	  else{ 
		return false;
	  }
  },""
);

$.validator.addMethod('requiredcs',function(value){
	if(value==''){
		return false;
	}else{
		return true;
	}
},""
);
var res = '';
$.validator.addMethod('uniquedivision',function(value){
		var post_type="division_name_check";
		var divisionname=$("#divisionname").val();
		var divisionrulelist=$('#divisionrule').val();
		 var response; 
        $.ajax({
            method: "POST",
            url: "manageseasondivision-ajax.php",
			async:false,
            data:{"post_type":post_type,"division_name":divisionname,"division_rulelist":divisionrulelist},      
            }).success(function(response) {
				res = response;
                
            });
            if(res == "not-exists"){	
			  return true;
			}else{
			  return false;
			} 
    });

$("#divisionfrm").validate({
	 rules: {
		division:{requiredcs:true},
		divisionrule:{requiredcs:true,uniquedivision: true},	
		 },
		messages: {
		 division:{requiredcs:"Please enter division name"},
		 divisionrule:{requiredcs:"Please select division rule",uniquedivision:"Division name already exists"},			 
	   },
		submitHandler: function (form) {			
			var $form = $(form);
			var FormArr  = $form.serialize();	
			var SeasionId = $("#seasoniddiv").val();
			$form.hide();
			$("#DivisionModal").find("#loadingadddivisn").show();
			$( "#seasonmaincont_"+SeasionId).load( "add_newdivision.php?"+FormArr, function( response, status, xhr ) {	
				if(status=="success"){
					$("#loadingadddivisn").hide();
					$("#divisionstsmsg").show();
					$("#divisionstsmsg tr td").empty().append("Division details saved successfully.");
				}else{
					$("#loadingadddivisn").hide();
					$("#divisionstsmsg").show();
					$("#divisionstsmsg tr td").empty().append("Division details not saved.");
				}
				DragAndDropOnLoad();
				//$( "#DivisionModal" ).modal("hide");
				
			});
	}
 });	
	
	 
		



$('.addnewdivibtn').on('click', function(evt) {
	if (!$("#divisionfrm").validate()) { 
		return false;
	} 	
	$("#divisionfrm").submit();		
});

$('.addnewconferencebtn').on('click', function(evt) {
	if (!$("#conferencefrm").validate()) { 
		return false;
	}
	
	$("#conferencefrm").submit();		
});

$("#conferencefrm").validate({
	 rules: {
		 conference:{requiredcs:true,uniqueconf: true},		 
		 },
		messages: {
		 conference:{requiredcs:"Please enter conference name",uniqueconf:"Conference name already exists"},	 
	   },
		submitHandler: function (form) {			
			var $form = $(form);
			var FormArr  = $form.serialize();	
			
			var SeasionId = $("#seasonid").val();
			$form.hide();
			$("#ConferenceModal").find("#loadingaddconfer").show();
			$( "#seasonmaincont_"+SeasionId).load( "add_newconference.php?"+FormArr, function( response, status, xhr ) {			if(status=="success"){
					$("#loadingaddconfer").hide();
					$("#conferencestsmsg").show();
					$("#conferencestsmsg tr td").empty().append("Conference details saved successfully.");
				}else{
					$("#loadingaddconfer").hide();
					$("#conferencestsmsg").show();
					$("#conferencestsmsg tr td").empty().append("Conference details not saved.");
				}
				DragAndDropOnLoad();
				
			});

		 }
	});

});
//Function used to reload season 
$(document).on('click','.manageconfpopbtn', function(evt) {
	var $this = $(this);
	var anyBoxesChecked = false;

    $this.closest('#manageconfformid').find('input[type="checkbox"]').each(function() {
		 if ($(this).is(":checked")) {
            anyBoxesChecked = true;
        }
    });
 
    if (anyBoxesChecked == false) {
        alert("Please check at least one checkbox");
        return false;
    } 
	var FormArr  = $this.closest("#manageconfformid").serialize();
	var SeasionId = $("#seasionidconfid").val();
	$this.closest("#manageconfformid").hide();
	$this.closest(".modal-body").find("#loadingconference").show();
	$( "#seasonmaincont_"+SeasionId).load( "update_season.php?"+FormArr, function( response, status, xhr ) {		
		$("#loadingconference").hide();
		$( "#manageconfmsg" ).show();
		DragAndDropOnLoad();
		
	});
});

   $(document).on('click','.managedivisionbtn', function(evt) {
	  
	   $("#managedivModal").find("#managedivformid").show();
	   $("#loadingdivison,#managedivinmsg").hide();
	   $("#loadingdivisionselected").show();

	   var seasonid = $(this).attr("data-seasonid");
	   var ConferenceId = $(this).attr("data-conferenceid");
		$.ajax({
			  url: "managedivisionselected-ajax.php",
			  type: "POST",
			  data: {seasonid:seasonid,conferenceid : ConferenceId},
			  success: function(data){
				
			   var response =$.parseJSON(data);			  
			   $("#seasioniddivid").val(seasonid);
			   $("#seasionidconfdivid").val(ConferenceId);
			   if(response.status=="Failed"){			
				   	$("#loadingdivisionselected").hide();
				    $("#managedivModal").find("#managedivformid").hide();
					alert('No Division Found.');
					return false;
			   }else{
					var HtmlWrap = response;
					$("#managedivformcont").empty().append(HtmlWrap);
					$("#loadingdivisionselected").hide();
					$("#managedivModal").modal('show');
			   }
			  }
			});
	  
	});
  
	$(document).on('click','.manageconferencemodel', function(evt) {
		
		$("#ManageConferenceModal").find("#manageconfformid").show();
		$("#loadingconference,#manageconfmsg").hide();
		$("#loadingconferenceselected").show();

	   var seasonid = $(this).attr("data-seasonid");
	 
		$.ajax({
			url: "manageconferenceselected-ajax.php",
			  type: "POST",
			  data: {seasonid:seasonid},
			  success: function(data){
				 var response =$.parseJSON(data);
				   $("#seasionidconfid").val(seasonid);
				    if(response.status=="Failed"){
						$("#loadingconferenceselected").hide();
						$("#ManageConferenceModal").find("#manageconfformid").hide();
						alert('No Conference Found.');
					return false;
					}else{
						var HtmlWrap = response;
						$("#manageconfformcont").empty().append(HtmlWrap);
						$("#loadingconferenceselected").hide();
						$("#ManageConferenceModal").modal('show');
					}
			}
		});
		
	});

	$(document).on("click", ".addconferencebtn", function(event){
		var SeasonId = $(this).attr("data-seasonid");
		var Seasonname=$(this).attr("data-seasonname");
		$('#seasonid').val(SeasonId);
		if(Seasonname !=''){
		$('#ConferenceModal').find('#season-name-conference').text('' + Seasonname + ' ');
		}
		$("#loadingaddconfer,#conferencestsmsg").hide();
		$("#conferencefrm").show();
		$("#conferencename").val('');
		//$('.inactivedivision').attr('checked', true);
	});
	$(document).on("click", ".adddivisionbtn", function(event){
		$("#loadingadddivisn,#divisionstsmsg").hide();		
		$("#divisionfrm").show();
		$("#divisionname").val('');
		$('.activedivision').attr('checked', 'checked');
		$('.seldivinrule').val('');
		

		var ConferenceId = $(this).attr("data-conferenceid");
		var SeasonId = $(this).attr("data-seasonid");
		var Conferencename=$(this).attr("data-conferencename");
		if(Conferencename!=''){
		$('#DivisionModal').find('#conference-name-division').text('' + Conferencename + ' ');
		}
		$('#conferenceid').val(ConferenceId);
		$('#seasoniddiv').val(SeasonId);
	});

	
	$(document).on("click", ".deletebtnconf", function(event){
		var $this = $(this);

		if(confirm("Are you sure want to delete this conference?")){
			var ConferenceId = $(this).attr("data-conferenceid");
			var SeasonId	 = $(this).attr("data-seasonid");
			
			$.ajax({
			  url: "deleteconference.php",
			  type: "POST",
			  data: {seasonid:SeasonId,conferenceid : ConferenceId},
			  success: function(data){				
				
				var MasParentWrap = $this.closest(".parent_confernce").children().length;
				var ParentWrap = $this.closest(".parent_confernce");
				//dont not change this remove to above. It will affect drag and drop style
				$this.closest(".conferencetbltoggle").remove();

				var ChildCnt = ParentWrap.find('.divisiontbltoggle').children().length;			
					
				if(ChildCnt>1){
					var test = ChildCnt ;
					var BottomCss =  (48+(test*44)-21);						
					
					$('#msseasontbleborder_'+SeasonId).empty().append('<style>table.msseasontbleborder_'+SeasonId+'>tbody>tr>td:after{bottom: '+BottomCss+'px;}</style>');	
				}else {
					$('#msseasontbleborder_'+SeasonId).empty().append('<style>table.msseasontbleborder_'+SeasonId+'>tbody>tr>td:after{bottom: 47px;}</style>');
				}

			  }
			});
		}else{
			return false;
		}
	});
	$(document).on("click", ".deletebtndiv", function(event){
		var $this = $(this);
		 
		var DivisionId = $this.closest('.divisions').attr("data-divid");
		var ConferenceId = $this.closest('.conferencetbltoggle').attr("data-conf");
		var SeasionId = $this.closest('.ms_seasontble').attr("data-seasion");

		var MasParentWrap = $(this).closest(".parent_confernce").children().length;
		var ParentWrap = $(this).closest(".conferencetbltoggle");		
		var ChildCnt = ParentWrap.find('.divisiontbltoggle').children().length;
				
		if(confirm("Are you sure want to delete this division?")){			
			$.ajax({
			  url: "deletedivision.php",
			  type: "POST",
			  data: {conferenceid:ConferenceId,divisionid : DivisionId,seasionid:SeasionId},
			  success: function(data){
				if(data=="success"){
					$this.closest(".divisions").remove();
					$('.tooltip ').hide();
					//$("#divisionwrap_"+ConferenceId).find('input:checkbox[value="' + DivisionId + '"]').attr('checked', false);
					if(ParentWrap.is(':last-child') && (ChildCnt>1)){		
						
						if(ChildCnt>1){
							var test = ChildCnt ;
							var BottomCss =  ((48+(test-1)*44)-21);							
							$('#msseasontbleborder_'+SeasionId).empty().append('<style>table.msseasontbleborder_'+SeasionId+'>tbody>tr>td:after{bottom: '+BottomCss+'px;}</style>');	
						}
					}else if((MasParentWrap>1)&&(ParentWrap.is(':last-child'))){
						$('#msseasontbleborder_'+SeasionId).empty().append('<style>table.msseasontbleborder_'+SeasionId+'>tbody>tr>td:after{bottom: 47px;}</style>');
					}
					
				}
			  }
			});
		}else{
			return false;
		}
	});



	$(document).on("click", ".deleteseasonbtn", function(event){
		var $this = $(this);
		if(confirm("Are you sure want to delete this season?")){
			var SeasonId = $(this).attr("data-seasonid");
			$.ajax({
			  url: "deleteseason.php",
			  type: "POST",
			  data: {seasonid:SeasonId},
			  success: function(data){
				if(data=='success'){
					var EmptySeasonHtml ='<div class="col-md-12 col-sm-12 seasonwrapcont donotdraganddrap text-center"><div class="portlet box grey seasontbltogglewrap"><div class="portlet-title"><div class="caption tools" style="width: 98%;"><a href="javascript:;" class="collapse" style="color:#000;background-image:none;display: block;width: 100%;">No season(s) found</a></div></div></div></div>';

					var EmptyChk = $('.seasonmainwrapper .seasonwrapcont').length;
					
					if(EmptyChk==1){
						$this.closest('.seasonmainwrapper').append(EmptySeasonHtml);
					}
					$this.closest(".seasonwrapcont").remove();
				}
			  }
			});
		}else{
			return false;
		}
	});


$(document).on('change','.seasonnamesnew', function(evt) {
	var $this = $(this);
	if($this.is(":checked")) {		
		//$(".slidetogglewrap").slideUp();		
		$this.closest(".mt-radio-list").next(".slidetogglewrap").slideToggle();
		
	}
});


$.validator.addMethod('uniqueseason',function(value)
 {
  var seasonname=value;  
  var post_type="seasonnamechk";
  var result=$.ajax({
		 type:"POST",
		 async:false,
		 url:"nameexistscheck.php",
		 data:{"post_type":post_type,"seasonname":seasonname},
		 dataType:"text",
	 }); 
	
	  if(result.responseText=="not exists"){
	   return true;
	  }
	  else{ 
		return false;
	  }
  },""
);




$("#addseasonfrm").validate({
	 rules: {
		 seasonnamenew:{requiredcs:true,uniqueseason: true},		 
		 },
		messages: {
		 seasonnamenew:{requiredcs:"Please enter season name",uniqueseason:"Season name already exists"},	 
	   },
		submitHandler: function (form) {	
			 
			var SeasonId      = SeasonQry = FormArr = '';
			var seasonnamenew = $("#seasonnamenew").val();
			var Existsseason    = '';
			
			SeasonQry  = "&seasonnamenew="+encodeURIComponent(seasonnamenew);
			$('.seasonnamesnew').each(function () {
				if($(this).is(":checked")){		
					Existsseason  = $(this).val();
					FormArr = $(this).closest(".mt-radio-list").next(".slidetogglewrap").find(".selectedseasontree").serialize();	
					SeasonQry  += "&existsseason="+Existsseason;
				}
			});			
			
			$("#addseasonfrm,#addseasonformcont").hide();
			$("#seasonModal").find("#addseasonbtnshow").hide();
			$("#seasonModal").find("#loadingseason").show();

			$( '.seasonmainwrapper').load( "add_newseason.php?"+FormArr+SeasonQry, function( response, status, xhr ) {	
				
				if(xhr.statusText=="OK"){
					
					$('.seasonmainwrapper').html(xhr.responseText);
					$("#addseasonbtnshow").hide();
					$("#addseasonformcont,#addseasonfrm").hide();
					$("#seasonstsnmsg").show();
					$("#seasonModal").find("#loadingseason").hide();
					$("#seasonstsnmsg tr td").empty().append("Season details saved successfully.");		
					DragAndDropOnLoad();
					return false;
				}
			});

		 }
	});


$(document).on('click','.addnewseasonbtn', function(evt) {
	if (!$("#addseasonfrm").validate()) { 
		return false;
	} 	
	$("#addseasonfrm").submit();		
});

$(document).on('click','.addseasonbtntop', function(evt) {
	$("#loadingseason,#seasonstsnmsg,.popupmodelseason,#seasonnamenew-error").hide();
	$("#addseasonformcont,#addseasonfrm").show();
	$("#addseasonfrm").hide();
	$("#loadingseasonaddform").show();
	$("#addseasonbtnshow").hide();
	$("#seasonnamenew").val("");
	 var post_type="addseasoncheck";
	$.ajax({
			url: "addseasonselected-ajax.php",
			  type: "POST",
			  data: {post_type:post_type},
			  success: function(data){
				$("#loadingseasonaddform").hide();
				var response =$.parseJSON(data);
				if(response=='Failed'){
					$("#addseasonfrm").show();
					$("#addseasonbtnshow").show();
				}else{
				var HtmlCont  = response;
				$("#addseasonfrm").show();
				$("#addseasonformcont").empty().append(HtmlCont);
				$("#addseasonbtnshow").show();
				}
			}
		});


	//var HtmlCont  = $("#addseasonformcont").html();
	//$("#addseasonformcont").empty().append(HtmlCont);

});
$(document).on("change",".conference-checked", function() {
	var seasonid=$(this).closest('.ms_seasontble').attr('data-seasion');
	var conferenceid=$(this).val();
	var divisionArray = new Array();
	$(this).closest(".conferencetbltoggle").find("input[name='division[]']").each(function() {
		divisionArray.push($(this).val());
	});
	if(this.checked){
		var status='1';
	}else{
		var status='0';
	}
	
	var post_type="conferencechecked";
		$.ajax({
			url: "customerseasonchecked-ajax.php",
			type:"POST",
			data:{post_type:post_type,seasonid:seasonid,conferenceid:conferenceid,divisionArray:divisionArray,status:status},
				success: function(data){
					if(data=='success'){
					}
			}
		});
	
});
$(document).on("change",".division-checked", function() {
	var seasonid=$(this).closest('.ms_seasontble').attr('data-seasion');
	var conferenceid=$(this).closest('.conferencetbltoggle').attr('data-conf');
	var divisionid=$(this).val();
	var divisionArray = new Array();
	$(this).closest(".conferencetbltoggle").find("input[name='division[]']").each(function() {
		divisionArray.push($(this).val());
	});
	if(this.checked){
		var status='1';
	}else{
		var status='0';
	}
	var post_type="divisionchecked";
		$.ajax({
			url: "customerseasonchecked-ajax.php",
			type:"POST",
			data:{post_type:post_type,seasonid:seasonid,conferenceid:conferenceid,divisionid:divisionid,status:status,divisionArray:divisionArray},
				success: function(data){
					if(data=='success'){
						}
			}
		});
});


$(document).on("change",".conferencechkbox", function() {
	var ischecked= $(this).is(':checked');
	if(!ischecked){
	    $(this).closest(".innertable").nextUntil("table.innertable").find('input[type="checkbox"]').removeAttr('checked');
	}else{
		$(this).closest(".innertable").nextUntil("table.innertable").find('input[type="checkbox"]').prop('checked' , true);
	}
}); 
$(document).on("change",".conference-checked", function() {
	var ischecked= $(this).is(':checked');
	if(!ischecked){
	    $(this).closest(".innertable").nextUntil("table.innertable").find('input[type="checkbox"]').removeAttr('checked');
	}else{
		
		$(this).closest(".innertable").nextUntil("table.innertable").find('input[type="checkbox"]').prop('checked' , true);
	}
}); 

$(document).on("change",".division-checked", function() {
		var ischecked= $(this).is(':checked');
		if(ischecked){
		$(this).closest(".conferencetbltoggle").find('.innertable').find('input[type="checkbox"]').prop('checked' , true);
		}

});
$(document).on("change",".add-division-selected", function() {
		var ischecked= $(this).is(':checked');
		if(ischecked){
		$(this).closest('.popupmodelconference').find('.innertable').find('input[type="checkbox"]').prop('checked' , true);
		}

});

/************* Drag and drop script***/

function DragAndDropOnLoad(){
    
    $( ".seasonmainwrapper " ).sortable({
       revert       : true,
       //connectWith  : ".sortable, .seasonwrapcont ",
	   containment: $('.bottommainwrap'),
	   connectWith  : ".sortable ",
       cancel: ".donotdraganddrap ",
	   forcePlaceholderSize: true,
       stop         : function(event,ui){ /* do whatever here */ },
	   update: function( event, ui ) {
			var UpdateSeason =[];
			$(this).find(".seasonwrapcont").each(function(i) {				
				var SeasonId  = $(this).attr("data-seasonid");
				UpdateSeason[i] = SeasonId;	

			});		
			
			$.ajax({
			url: "update_orders.php",
			type: "POST",
			data:{'post_type':'seasonorderchange','seasonid':UpdateSeason}, //UpdateDiv,
			success: function(data){
				//console.log(data);
			}
			});
	}
    }).disableSelection();

    $(".parent_confernce").sortable ({
        revert       : true,
        connectWith  : ".sortable",
		//containment: $(".seasontbltoggle"),//"parent",
		placeholder: "conferenceplaceholder",
        stop         : function(event,ui){ 
			//$(this).closest('.parent_confernce').css({'padding-bottom':'0px'});
		},
		forcePlaceholderSize: true,
		start: function( event, ui) {
			var PlaceHolderHeight = ui.item.height();
			//$thisDiv = $(this);
			$(".conferenceplaceholder").css({'height':PlaceHolderHeight+'px'});
			//$(this).closest('.parent_confernce').css({'padding-bottom':PlaceHolderHeight+'px'});
		},
		update: function( event, ui ) {
			//var ParentCnt = $(this).closest('.parent_confernce').children();
			var SeasionId = $(this).closest('.ms_seasontble').attr('data-seasion');
			//var MasParentWrap = $(this).closest(".parent_confernce").children().length;
			//var ParentWrap = $(this).closest(".conferencetbltoggle");		

			var ChildCnt = $(this).closest('.seasontbltoggle').find('.parent_confernce').children().last().find('.divisions').length;

			if(ChildCnt>0){		
				
				var test = ChildCnt ;
				var BottomCss =  ((48+(test-1)*44)+21);							
				$('#msseasontbleborder_'+SeasionId).empty().append('<style>table.msseasontbleborder_'+SeasionId+'>tbody>tr>td:after{bottom: '+BottomCss+'px;}</style>');					
				
			}else{
				$('#msseasontbleborder_'+SeasionId).empty().append('<style>table.msseasontbleborder_'+SeasionId+'>tbody>tr>td:after{bottom: 47px;}</style>');
			}
			var $thisParentDiv  = $(this).closest('.ms_seasontble');
			var SeasonId     = $thisParentDiv.attr('data-seasion');
			var UpdateConf =[];
			//$(this).closest('.parent_confernce').css({'padding-bottom':'0px'});
			$(this).find(".conferencetbltoggle").each(function(i) {				
				var ConfId  = $(this).attr("data-conf");
				UpdateConf[i] = ConfId;	
				
			});			
			$.ajax({
			  url: "update_orders.php",
			  type: "POST",
			  data:{'post_type':'conforderchange','conferenceid':UpdateConf,'seasonid':SeasonId}, //UpdateDiv,
			  success: function(data){
				  //ms_seasontble  
				//var Test =  $thisParentDiv.html();
				//$thisParentDiv.empty().append(Test);
				//console.log(data);
			  }
			});
		}
    }).disableSelection();

	var OldConfId = '';
	var MovedDiv  = '';
    $(".divisiontbltoggle").sortable ({
        revert       : true,
		//containment: $('.parent_confernce'),
		// do not delete this. It will be for future use
        connectWith  : ".conferencetbltoggle .divisiontbltoggle", 
		//connectWith  : $('.divisiontbltoggle').parent(),
        dropOnEmpty: true,
		start: function( event, ui ) {			
			OldConfId = $(ui.item).closest(".conferencetbltoggle").attr("data-conf");			
		},
		update: function( event, ui ) {			
			if (this === ui.item.parent()[0]) {
				var ConferenceId =  $(ui.item).closest(".conferencetbltoggle").attr('data-conf');
				var SeasonId     = $(this).closest(".ms_seasontble").attr('data-seasion');
				var UpdateDiv =[];						
				
				var ChildCnt = $(this).closest('.seasontbltoggle').find('.parent_confernce').children().last().find('.divisions').length;

				if(ChildCnt>0){		
					
					var test = ChildCnt ;
					var BottomCss =  ((48+(test-1)*44)+21);							
					$('#msseasontbleborder_'+SeasonId).empty().append('<style>table.msseasontbleborder_'+SeasonId+'>tbody>tr>td:after{bottom: '+BottomCss+'px;}</style>');					
					
				}else{
					$('#msseasontbleborder_'+SeasonId).empty().append('<style>table.msseasontbleborder_'+SeasonId+'>tbody>tr>td:after{bottom: 47px;}</style>');
				}

				if(OldConfId==ConferenceId){
					$(this).find(".divisions").each(function(i) {				
						var DivisionId  = $(this).attr("data-divid");
						UpdateDiv[i] = DivisionId;						
					});						
					$.ajax({
					  url: "update_orders.php",
					  type: "POST",
					  data:{'post_type':'divisionorderchange','divisionid':UpdateDiv,'conferenceid':ConferenceId,'seasonid':SeasonId}, //UpdateDiv,
					  success: function(data){
						//console.log(data);
					  }
					});				

				}else{
					$(this).find(".divisions").each(function(i) {				
						var DivisionId  = $(this).attr("data-divid");
						UpdateDiv[i] = DivisionId;						
					});
					MovedDiv = $(ui.item).attr("data-divid");				
					
					var NewHrefLink = 'add_divisionteam.php?divisionid='+MovedDiv+'&conferenceid='+ConferenceId+'&seasonid='+SeasonId;					
					$(ui.item).find('.adddivisionbtn ').attr('href',NewHrefLink);
					
					$.ajax({
					  url: "update_orders.php",
					  type: "POST",
					  data:{'post_type':'divisionorderchangeconftoconf','divisionid':UpdateDiv,'newconferenceid':ConferenceId,'seasonid':SeasonId,'oldconferenceid':OldConfId,'moveddivision':MovedDiv}, //UpdateDiv,
					  success: function(data){
						//console.log(data);
					  }
					});
				}
			}
		}
    }).disableSelection();
    
}


$(document).ready(function(){
	DragAndDropOnLoad();
});