
$(document).ready(function(){

	function readURL(input) {

	    if (input.files && input.files[0]) {
	        var reader = new FileReader();
	        
	        reader.onload = function (e) {	        	
	            $('#preview').attr('src', e.target.result);
	        }
	        
	        reader.readAsDataURL(input.files[0]);
	    }
	}

	$("#backgroundimage").change(function(){
	    readURL(this);
	});

});

$(document).ready(function(){

    $("#settingsform").validate({       
        rules: {
            gen_slider_page:{
                required:true,
             },
            gen_standing_page:{
                required:true,
             }, 
            gen_schedule_page:{
                required:true,
                maxlength:5,
             },
            // gen_content_bgcolor:{
            //     required:true,
            // },
            // gen_content_fontcolor:{
            //     required:true,
            // },
            // menu_bgcolor:{
            //     required:true,
            // },
            // menu_hovercolor:{
            //     required: true,                
            // },
            // menu_fontcolor:{
            //     required: true,                
            // },
            // slider_heading_bgcolor:{
            //     required: true,                
            // },
            // slider_content_bgcolor:{
            //     required: true,                
            // },
            // slider_fontcolor:{
            //     required: true,                
            // },  
        }, 
        messages: {
            gen_slider_page: {
                required: "Please select schedule type",               
            },
            gen_standing_page: {
                required: "Please select stanging page type",                
            },
            gen_schedule_page: {
                required: "Please select schedule type",                
            },
            // gen_content_bgcolor: {
            //     required: "Please select background color",
            // },
            // gen_content_fontcolor: {
            //     required: "Please select font color",                
            // },
            // menu_bgcolor: {
            //     required: "Please select background color",                
            // },
            // menu_hovercolor: {
            //     required: "Please select hover color",                
            // },
            // menu_fontcolor: {
            //     required: "Please select font color",                
            // },
            // slider_heading_bgcolor: {
            //     required: "Please select background color",                
            // },
            // slider_content_bgcolor: {
            //     required: "Please select background color",                
            // },
            // slider_fontcolor: {
            //     required: "Please select font color",                
            // },

        },

    });

});

$(document).ready(function() {

    $( ".updatebtn" ).on( "click", function() {
        var hiddenBackgroundImage = $('#hiddenbgimg').val();
        if (hiddenBackgroundImage.length != '') {
            backgroundImagefun(false);
        }
    });

    var chackedvalue = $('input[name=backgroundradio]:checked').attr('id');
    if (chackedvalue == 'radio1_1') {
        var hiddenBcgImage = $('#hiddenbgimg').val();
        if (hiddenBcgImage.length != '') {
            backgroundImagefun(false);
        } else {
            backgroundImagefun(true);
        }
        // $("#preview").show();
    } else {
        // $("#preview").hide();
    }

    $( "input[name=backgroundradio]" ).on( "click", function() {
        var getId = $(this).attr('id'); 
        var hiddenBgImage = $('#hiddenbgimg').val();           
        if (getId == 'radio1_1') { 

            // $("#preview").show();           
            if (hiddenBgImage.length != '') {
                backgroundImagefun(false);
            } else {
                backgroundImagefun(true);
            }
        } else {            
            // $("#preview").hide();
            backgroundImagefun(false);
        }
    }); 

    //Adding validation for Background image while you check on Background image
    function backgroundImagefun(requiretype)
    {
        $( "#backgroundimage" ).rules( "add", {
              required: requiretype,
              messages: {
                required: "Please select background image"
              }
        });
    }
});


       