var SortingColumnNum = "";
var SortingColumnOrder = "";
$(document).ready(function() {

    $(document).on("click",".searchbycustomteam",function(){
        ajaxTeamList();
    });
    $(document).on("click",".searchbyabateam",function(){
       ajaxTeamList();
    });

    $(document).on("click","#sample_1 > thead > tr th:not(.sorting_disabled)",function(){
        
       SortingColumnNum = $(this).index();
       $("#SortingColumnNum").val($(this).index());
       SortingColumnOrder = $("th:eq("+SortingColumnNum+" )").attr("aria-sort");
        if (SortingColumnOrder == "ascending") {
            SortingColumnOrder = "asc";
        } else {
            SortingColumnOrder = "desc";
        }
        // alert(SortingColumnNum);
        SortingColumnNum = $("#SortingColumnOrder").val(SortingColumnOrder);

    });

    SortingColumnNum = $("#SortingColumnNum").val() ? $("#SortingColumnNum").val() : 1 ;
    SortingColumnOrder = $("#SortingColumnOrder").val() ? $("#SortingColumnOrder").val() : "asc" ;

    $('#sample_1').DataTable({
        "retrieve": true,
        "paging": false,
        "bInfo": false,
       "bFilter":false,
        "bLengthChange":false,
        "bPaginate":false,
        "aaSorting": [[SortingColumnNum,SortingColumnOrder]],
        "language": {
            "zeroRecords": "No Team(s) found.",
            "infoEmpty": "No Team(s) found."
        },            
         "aoColumnDefs": [ { "bSortable": false, "aTargets": [6, 7] } ], 
    }); 

});


function ajaxTeamList() {       

    $(".loadingsection").show();
    $('#ajaxteamlist').hide();

    SortingColumnNum = $("#sample_1 > thead > tr th").index($("#sample_1 > thead > tr th.sorting_asc"));
    if (SortingColumnNum <= 0 ) {
        SortingColumnNum = $("#sample_1 > thead > tr th").index($("#sample_1 > thead > tr th.sorting_desc"));
    } 
    SortingColumnOrder = $("th:eq("+SortingColumnNum+" )").attr("aria-sort");
    if (SortingColumnOrder == "ascending") {
        SortingColumnOrder = "asc";
    } else {
        SortingColumnOrder = "desc";
    }    

    var HdnPage        = $("#HdnPage").val();
    var HdnMode        = $("#HdnMode").val();
    var RecordsPerPage = $("#RecordsPerPage").val();
    var tablename = $("#tablename").val();
    var season = $('#searchbyseasonid').val();
	var status = $('#searchbystatus').val();
    $("#hdnsesid").val(season); 

    var season = "";
    if ( $( "#searchbyseasonid" ).length )
        season = $('#searchbyseasonid').val();
    $("#hdnsesid").val(season); 

    var division = "";
    if ( $( "#searchbydivisionid" ).length )
        division = $('#searchbydivisionid').val();
    $("#hdndivid").val(division);

    $("#hdnsearchteam").val($("#searchtext").val());

    var searchteam = '';
    searchteam  = $("#searchtext").val();
    $("#hdnsearchteam").val(searchteam);
    

    $.ajax({
        url:"filter_teams.php",  
        method:'GET',
        data:{season: season, division: division, ls: tablename, searchbyteam:searchteam, HdnPage: HdnPage, HdnMode: HdnMode, PerPage: RecordsPerPage, status: status, SortingColumnNum: SortingColumnNum, SortingColumnOrder: SortingColumnOrder},
        success:function(data) {
            $(".loadingsection").hide();
            $('#ajaxteamlist').show();
            $('#ajaxteamlist').html('');
            $('#ajaxteamlist').html(data);
            loading_sorting();
        }
    });
}

function loading_sorting() {  

    SortingColumnNum = $("#SortingColumnNum").val() ? $("#SortingColumnNum").val() : 1 ;
    SortingColumnOrder = $("#SortingColumnOrder").val() ? $("#SortingColumnOrder").val() : "asc" ;    
    $('#sample_1').DataTable({
         "retrieve": true,
        "paging": false,
        "bInfo": false,
       "bFilter":false,
        "bLengthChange":false,
        "bPaginate":false,
        "aaSorting": [[SortingColumnNum, SortingColumnOrder]],
        "language": {
            "zeroRecords": "No Team(s) found.",
            "infoEmpty": "No Team(s) found."
        },
        "aoColumnDefs": [ { "bSortable": false, "aTargets": [6, 7] } ], 
    });
}

function deleteTeam(id,sportname){
	var answer = confirm('Are you sure you want to delete?');
	if(answer){ 
		window.location = "delete_team.php?action=delete&did="+id+"&sportname="+sportname;
	} 
	// else {
	// 	alert("Cancelled the delete!")
	// }
}


