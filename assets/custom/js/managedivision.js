$(document).ready(function() {
	 // $( ".resetbtn" ).click(function() {

    divSorting();

    $('.dt-buttons').hide();
	$('#loadingadds').hide();
	$('#divisionstsmsg').hide();

    $(document).on("click","#adddivision",function(){
        
        var popupname = $(this).text();
        $('#division_name').val('');        
        $("#division_rulelist").prop("selectedIndex", 0);
        $('#mode').empty().append('Add');
        $('#mode_btn').val('Submit');        
        $('#divisionfrm .form-group label.error').remove();
        $('#divisionfrm .form-group input, #divisionfrm .form-group select').removeClass('error');
        $('#DivisionModal').modal({show: 'true'}); 
        
    });

    //EditDivisionModal
	$(document).on("click", ".edit_popup", function () {       	
        var customerid = $(this).attr('customerid');
		$('#cust_divsion_id').val(customerid);
		var data_id=$(this).attr('data-id');
		var data_name=$(this).attr('data-name');
		var data_role=$(this).attr('data-role');
		var data_sports=$(this).attr('data-sports');

        $('#mode').empty().append('Edit');
        $('#mode_btn').val('Update');

		$('#division_name').val(data_name);
		$('#role_name').val(data_role);
		$('#role_sports').val(data_sports);
		$("#division_rulelist").val(data_role);
        $('#divisionfrm .form-group label.error').remove();
        $('#divisionfrm .form-group input, #divisionfrm .form-group select').removeClass('error');
		$( "#DivisionModal" ).modal("show");	

    });
  
	$(document).on("click","#searchbtn",function(){
		var OrganName = $("#manage_division_search").val();
		if (OrganName!='') {
    		$(".loadingsection").show();
            $('.customerlistparent').hide();
            managedivisionsearch();
    		return false;
		} else {
				alert("Please enter the division name");
		return false;
		}

	});
    

    $('.addnewdivibtn').on('click', function(evt) {
    	if (!$("#divisionfrm").validate()) { 
    		return false;
    	} 	
    	$("#divisionfrm").submit();		
    });

});

//  Add & Edit modal popup
$(document).ready(function() {

    $("#divisionfrm").validate({
    	 rules: {
    		 division_name:{
                required:true,
                
            },
    		 division_rulelist:{
                required:true,
                requiredunique: true,
            },
    	},
    	messages: {
    	   division_name:{
                required:"Please enter division name",
                
            },
    	   division_rulelist:{
                required:"Please select division rule",
                requiredunique:"Division name already exists",
            },
       },
    		
    });

	var response; 
	$.validator.addMethod('requiredunique',function(value){
        var post_type="division_name_check";
		var divisionname=$("#division_name").val();
		var divisionrulelist=$('#division_rulelist').val();
		var division_id=$('input[name="cust_divsion_id"]').val();
	   $.ajax({
        method: "POST",
        url: "managedivision-ajax.php",
        async:false,
        data:{"post_type":post_type,"division_name":divisionname,"division_rulelist":divisionrulelist,"division_id":division_id},
  
        }).success(function(msg) {
            response = msg;                  
            
        });

        if(response == "not-exists")
            return true;
        else
            return false;    
    });

});

function managedivisionsearch(){
    var OrganName = $("#manage_division_search").val();     
    var HdnPage        = $("#HdnPage").val();
    var HdnMode        = $("#HdnMode").val();
    var RecordsPerPage = $("#RecordsPerPage").val();
    
     $.ajax({
            
        url:"filter_divisions.php",  
        method:'POST',
        data:{searchbyorganization: OrganName, HdnPage: HdnPage, HdnMode: HdnMode, PerPage: RecordsPerPage},
        success:function(data) {

            $('.customerlistparent').html(''); 
            $('.customerlistparent').html(data);
            $(".loadingsection").hide();
            $('.customerlistparent').show();
            divSorting();
        }
      }); 
}

function divSorting() {
    $('#divisionlistingtable').DataTable({
            "retrieve": true,
            "paging": false,
            "bInfo": false,
           "bFilter":false,
            "bLengthChange":false,
            "bPaginate":false,
            "aaSorting": [[0,'desc']],
            "aoColumnDefs": [ { "bSortable": false, "aTargets": [2] } ], 
    });
}

$(document).on("click",".resetbtn",function(){
    window.location = "manage_division.php";
    // $( "#manage_division_search" ).val('');
    // Managedivision();
});