$(document).ready(function() {

	$('#rulelist').prop("disabled", true);
	$("#cancelbtn").click(function(){
	var sportnam = $("#sportn").val();
		// window.location = "game_list.php?sport="+sportnam;
		window.location = "game_list.php";
	});	

	$('#divisionlist').change(function(){
		if ((this.value) == 'addnew') {
			$("#adddivtext").val('');
			$('#DivisionModal').modal('show');  
			$('#rulelist').prop('disabled', false);        
	    }
	});

	$("#adddivbtn").click(function(){
	   	var newvalue=  $("#adddivtext").val();
		if(newvalue !=""){  
		    $('#divisionlist').append($('<option/>', { 
				value: newvalue,
				text : newvalue,
				selected:'selected' 
			}));

		$("#divisionlist option[value='']").removeAttr("selected","selected");
		} else {
            $("#adddivtext").keyup(function(){
                var divvalue=  $("#adddivtext").val();
                if (divvalue != "") {
                    $("#diverror").hide();
                    return true;
                } else {
                    $("#divisionlist option[value='']").attr("selected","selected");
                }
            });
            $("#diverror").show();
            $("#divisionlist option[value='']").attr("selected","selected");
            return false;
	    }    
	   
	  	$('#DivisionModal').modal('hide');
		$('#rulelist').prop('disabled', false); 
	});
	$(".closedivmodal").click(function(){
		$('#divisionlist').prop('selectedIndex',0);
		$('#DivisionModal').modal('hide');
		$('#rulelist').prop("disabled", true);
	});


	$('#seasonlist').change(function(){
		if ((this.value) == 'addnew') {
			$("#addssntext").val('');
			$('#SeasonModal').modal('show');         
		}
	});

	$("#addssnbtn").click(function(){
	   	var newvalue=  $("#addssntext").val();
		if(newvalue !=""){  
		    $('#seasonlist').append($('<option/>', { 
		        value: newvalue,
		        text : newvalue,
				selected:'selected' 
			}));
		    $("#seasonlist option[value='']").removeAttr("selected","selected");
		} else {
			$("#addssntext").keyup(function(){
                var divvalue=  $("#addssntext").val();
                if (divvalue != "") {
                    $("#seasonerror").hide();
                    return true;
                } else {
                    $("#seasonlist option[value='']").attr("selected","selected");
                }
            });
            $("#seasonerror").show();
            $("#seasonlist option[value='']").attr("selected","selected");
            return false;
		}   
	     
	  	$('#SeasonModal').modal('hide');
	});
	$(".closeseasonmodal").click(function(){
		$('#seasonlist').prop('selectedIndex',0);
		$('#SeasonModal').modal('hide');
	});

	$('#SeasonModal').on('hidden.bs.modal', function () {
        var conferencevalue = $("#addssntext").val();
        if (conferencevalue == "") {
            $('#seasonlist').prop('selectedIndex',0);
        }
    })

    $('#DivisionModal').on('hidden.bs.modal', function () {
        var divisionvalue = $("#adddivtext").val();
        if (divisionvalue == "") {
            $('#divisionlist').prop('selectedIndex',0);
        }
    })


});

$(document).on("click","#team-append .appendteams",function(){

	var selTeam = $(this).attr("teamname");
	var eleAppend = $(this).closest("div").parent().siblings("input").attr("id");
	var eleHide = $(this).closest("div").attr("id");
	var eleHideParent = $(this).closest("div").parent().attr("id");

	var isEleappend =  $("#"+eleAppend).val(selTeam);
	var isEleappendattr =  $("#"+eleAppend).attr("value", selTeam);
	var isEleHide = $("#"+eleHideParent+" #"+eleHide).hide();
	$("#"+eleAppend).focus();

});

$(document).ready(function(){
	$.validator.addMethod("notEqualTo", function(value, element) {
	   return $('#visitor').val() != $('#home').val()
	}, "");

	jQuery.validator.addMethod("childTeam",  
	    function(value, element, param) { 
	    	var teamManagerval = $("#teammanager").val();
	    	if (teamManagerval != "") {
	    		
		        var visitor = $("#visitor").val();
		        var home = $("#home").val();
		        
		        if (visitor !="" && home !="") {
		            if (visitor != teamManagerval && home != teamManagerval) {
		                return false;
		            } else {
		                return true;                
		            }
		        } else {
		            return true;
		        }
		    } else {
		    	return true;
		    }
	    }, ""
	);
});

$(document).on("keyup","#visitor, #home ",function(){
	
	var team_type = "";
	var thisAttr = $(this).attr("id");	
	var search_team_like = $(this).val();

	$.ajax({
		url: "find_teams.php",
		type: "POST",
		data: {search_team_like : search_team_like},
		success: function(data) {
			if (thisAttr == "visitor")
				$("#display_visitor_result").empty().append(data); 
			else
				$("#display_home_result").empty().append(data); 			
			    
		}
	});

});

$(document).ready(function(){
	$("#gameform").validate({
		rules: {
			 gamename:{
				required:true,				
			 },
			gamedate:{
				required:true,
				
			 },	
			gametime:{
				required:true,			
			 },	
			visitor:{
				required:true,
				notEqualTo: true,
				childTeam: true,
			 },
			home:{
				required:true,
				notEqualTo: true,
				childTeam: true,
			},

			divisionlist:{
				required:true,			
			 },
			rulelist:{
				required: true,               
			 },
			seasonlist:{
				required: true,
			},
				
	    }, 
		messages: {
			gamename:{
			   required: "Please enter game name",			  
			},
			gamedate:{
			   required: "Please enter date",			  
			},
			gametime:{
			   required: "Please enter time",		 
			},
			visitor:{
			   required: "Please enter visitor name",
			   notEqualTo:"Teams should not be same",
			   childTeam:"Must select your team",	
			},
			home:{
			   required: "Please enter home",
			   notEqualTo:"Teams should not be same",
			   childTeam:"Must select your team",
			},
			divisionlist:{
			   required: "Please select division",		 
			},
			rulelist:{
			   required: "Please select rule",            
			},
			seasonlist:{
			 	required: "Please select season",  
			},
						
	   },
	   errorPlacement: function (error, element) { // render error placement for each input type
            if (element.attr("name") == "gamedate") { // for uniform radio buttons, insert the after the given container
                error.insertAfter(".minicalender");
            } else if (element.attr("name") == "gametime") { // for uniform checkboxes, insert the after the given container
                error.insertAfter(".minitime");
            } else {
                error.insertAfter(element); // for other inputs, just perform default behavior
            }
        },

	});

});


$(document).ready(function(){
	$("#eventform").validate({
		rules: {
			 ename:{
				required:true,				
			 },
			date:{
				required:true,
				
			 },	
			time:{
				required:true,			
			 },	
			descp:{
				required:true,			
			 },
			
	    }, 
		messages: {
			ename:{
			   required: "Please enter value",			  
			},
			gamedate:{
			   required: "Please enter date",			  
			},
			time:{
			   required: "Please enter time",		 
			},
			descp:{
			   required: "Please enter  description",		 
			},
						
	   },
	   errorPlacement: function (error, element) { // render error placement for each input type
            if (element.attr("name") == "gamedate") { // for uniform radio buttons, insert the after the given container
                error.insertAfter(".minicalender");
            } else if (element.attr("name") == "gametime") { // for uniform checkboxes, insert the after the given container
                error.insertAfter(".minitime");
            } else {
                error.insertAfter(element); // for other inputs, just perform default behavior
            }
        },

	});


});
