jQuery(document).ready(function() {

    $(":file").filestyle({buttonBefore: true});

    jQuery.validator.addMethod( "extension", function( value, element, param ) {
        param = typeof param === "string" ? param.replace( /,/g, "|" ) : "doc|docx|pdf|xls|csv|zip";
        return this.optional( element ) || value.match( new RegExp( "\\.(" + param + ")$", "i" ) );
    }, jQuery.validator.format( "Please enter a value with a valid extension." ) );

    $("#frmmailsend").validate({
        rules: {       
            mail_send: {
              required: true,
              email: true
            },
            upload_file:{
              required: true,
              extension: true,
            }
        },
        messages: {     
            mail_send: {
              required: "Please enter email address",
              email: "Please enter a valid email address"
            },
             upload_file:{
              required:"Please choose a file",
               extension: "Please choose a valid file",
            }
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "upload_file") { // for uniform radio buttons, insert the after the given container
                error.insertAfter(".bootstrap-filestyle ");
            } else {
                error.insertAfter(element);
            }
        },
    });
});