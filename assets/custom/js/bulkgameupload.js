
$(document).on('click','.deleteplayerbtn',function(){
	var $this  = $(this);	
	if($('#bultplayentrycont form').length>1){
	 $('#bultplayentrycont form:last-child').remove();
	}else{
		alert('Minimum one row is required');
		return false;
	}
});

$(document).on('click','.submitallplayerbtn',function(){	
	
	var $thisbtn = $(this);

	var RegExpression = /^[a-zA-Z\s]*$/;  
	var AddPlayerChk = true;
	var FormCount  = $('#bultplayentrycont .multipleplayerformgrp').length;
	var Inc= 0;	
	var allvalid=true;
	
	$('#bultplayentrycont .multipleplayerformgrp').each(function(){	
		var formchk  = true;
		var $thisform = $(this);

		$thisform.css('border','0px solid red');
		$thisform.find('input').css('border','1px solid #d6d6d6');
		$thisform.find('select').css('border','1px solid #d6d6d6');		
		$thisform.find(".playerimg").css('border','1px solid #d6d6d6');	

		var gamename     	= $thisform.find("#gamename").val();
		var gamedate     	= $thisform.find("#gamedate").val();
		var gametime     	= $thisform.find("#gametime").val();
		var visitor       	= $thisform.find("#visitor").val();
		var home          	= $thisform.find("#home").val();
		var divisionlist    = $thisform.find("#divisionlist").val();
		var seasonlist      = $thisform.find("#seasonlist").val();
		var teamManagerval = $("#teammanager").val(); 

		var thisFormClass = $thisform.attr("class");	
		var FormClass = thisFormClass.split(' ');
		var thisformEleClass = FormClass[2];

		if (($("#bultplayentrycont ."+thisformEleClass+' .rulelist').is(':disabled')) == false) {
			var rulelist = $("#bultplayentrycont ."+thisformEleClass+" .rulelist").val();
			if(rulelist == '') {
				$("#bultplayentrycont ."+thisformEleClass+" .rulelist").focus();
				$("#bultplayentrycont ."+thisformEleClass+" .rulelist").css('border','1px solid red');
				allvalid = false;
				formchk  = false;
			}
			 
		}		
		
		if(gamename==''){
			$thisform.find("#gamename").focus();
			$thisform.find("#gamename").css('border','1px solid red');
			allvalid=false;
			formchk  = false;
		}
		if(gamedate==''){
			
			$thisform.find("#gamedate").css('border','1px solid red');
			allvalid=false;
			formchk  = false;
		}
	
		if(gametime==''){
			
			$thisform.find("#gametime").css('border','1px solid red');
			allvalid=false;
		}
		if(visitor==''){
			$thisform.find("#visitor").focus();
			$thisform.find("#visitor").css('border','1px solid red');
			allvalid=false;
			formchk  = false;
		}
		if(home==''){
			$thisform.find("#home").focus();
			$thisform.find("#home").css('border','1px solid red');
			allvalid=false;
			formchk  = false;
		}
		if (teamManagerval != "") {

			if (visitor !="" && home !="") {
	            if (visitor != teamManagerval && home != teamManagerval) {
	            	$thisform.find("#visitor").css('border','1px solid red');
					$thisform.find("#home").css('border','1px solid red');
	                allvalid=false;
					formchk  = false;
				}
			}
	            // } else {
	            //     return true;                
	            // }
	        // } else {
	        //     return true;
	        // }
		}
		if (visitor == home){
			$thisform.find("#visitor").css('border','1px solid red');
			$thisform.find("#home").css('border','1px solid red');
			allvalid=false;
			formchk  = false;
		}
		if(divisionlist==''){
			$thisform.find("#divisionlist").focus();
			$thisform.find("#divisionlist").css('border','1px solid red');
			allvalid=false;
			formchk  = false;
		}
		
		if(seasonlist==''){
			$thisform.find("#seasonlist").focus();
			$thisform.find("#seasonlist").css('border','1px solid red');
			allvalid=false;
			formchk  = false;
		}

		if(!formchk)
		{
			$thisform.css('border','1px solid red');
		}else{
			$thisform.css('border','0px solid red');
		}

	});	
	if(!allvalid)
	{
		return false;
	} 

	// $thisbtn.hide();

	$('#bultplayentrycont .multipleplayerformgrp:not(.alreadyvalidate)').each(function(){	

		var $thisform = $(this);
		var formData = new FormData($(this)[0]);
		var sportid = $("#sportid").val();
		var sportname = $("#sportname").val();
		 $.ajax({
			type : "POST",
			url : "addgamebulkentry.php?sport="+sportname,
			data : formData,	
			async:false,
			contentType: false,
			processData: false,
			success : function(response) {
				var response = $.parseJSON(response);
				if (response.gamestatus=='gameexists') {					
					var GamesLength  =  response.gamedetails;
					var GamesHtml    = '';
					var GameCode = '';
					for(var i=0;i<GamesLength.length;i++ ){
						var GameVisitorname = GamesLength[i].visitor;
						var GameHomename  = GamesLength[i].home;
						var GameCode         = GamesLength[i].GameID;
						var GameDate         = GamesLength[i].gamedate;
						var GameTime         = GamesLength[i].time;

					}
					$thisform.find('.uploadstatus').empty().append('<img src="images/editplayer.png" style="width: 16px; height: 16px; display: inline;" title="Duplicate game" gamedate="'+GameDate+'" gametime="'+GameTime+'" visitor="'+GameVisitorname+'" home="'+GameHomename+'" class="uploadstatusimg exitsmodelshow" gameid="'+GameCode+'">'+GamesHtml);	
					$thisform.css('border','1px solid red');
					$thisform.addClass('exitsgameform');
					PlayerExistChk= true;
				} else if (response.gamestatus=='success') {
					$thisform.addClass('alreadyvalidate');
					 $thisform.find('input, textarea, button, select').attr('disabled','disabled');
					$thisform.find('.uploadstatus').empty().append('<img src="images/yes.gif" style="width: 16px; height: 16px; display: inline;" class="uploadstatusimg">');	
				}				
			},
			error: function(jqXHR, textStatus, errorThrown){
				 alert(textStatus, errorThrown);
			}
		});	
	});
		
});

$(document).on('click','.exitsmodelshow',function(){
	$('#ExistGame').modal('show');
	var vistiorTeamName = $(this).attr("visitor");
	var homeTeamName = $(this).attr("home");
	var gameDate = $(this).attr("gamedate");
	var gameTime = $(this).attr("gametime");
	var gameID = $(this).attr("gameid");

	var modalBody = '<div class="modal-header"><p class="gameexists alert-danger">This game already exists with game ID: '+gameID+'</p></div><div class="modal-body"><table class="table"><tr><td><strong>'+vistiorTeamName+'</strong> vs <strong>'+homeTeamName+'</strong></td></tr><tr><td>'+gameDate+" "+gameTime+'</td></tr></table></div><div class="modal-footer"><button type="button" class="btn btn-default" data-dismiss="modal">Close</button></div>';
	$("#ExistGame .modal-content").empty().append(modalBody);
});

$(document).ready(function() {
	$('#rulelist').prop("disabled", true);
});

var parenFormattr = "";	
var splitFormClass = "";
var formEleClass = "";
$(document).on("change",".divisionlist",function(){

	$("#adddivtext").val('');
	parenFormattr = $(this).closest(".multipleplayerformgrp").attr("class");	
	splitFormClass = parenFormattr.split(' ');
	formEleClass = splitFormClass[2];
	
    if ((this.value) == 'addnew') {
		$('#DivisionModal').modal('show');
		$("."+formEleClass+' .rulelist').prop('disabled', false);        
	} else {
		$("."+formEleClass+' .rulelist').prop('disabled', true);
	}
});

// $(".closedivmodal").click(function(){
$(document).on("click",".closedivmodal",function(){

	// $("."+formEleClass+" .divisionlist option[value='']").attr("selected","selected");
	$("."+formEleClass+ ' .divisionlist').prop('selectedIndex',0);
	$('#DivisionModal').modal('hide');
	$("."+formEleClass+' .rulelist').prop('disabled', true);
});

$(document).on('click','#adddivbtn',function(){

    var newvalue=  $("#adddivtext").val();
	if(newvalue !=""){  
	    $("."+formEleClass+' .divisionlist').append($('<option/>', { 
			value: newvalue,
			text : newvalue,
			selected:'selected' 
		}));
		$("."+formEleClass+" .divisionlist option[value='']").removeAttr("selected","selected");
	} else {
		$("#adddivtext").keyup(function(){
            var divvalue=  $("#adddivtext").val();
            if (divvalue != "") {
                $("#diverror").hide();
                return true;
            } else {
                $("#divisionlist option[value='']").attr("selected","selected");
            }
        });
        $("#diverror").show();
        $("."+formEleClass+" .divisionlist option[value='']").attr("selected","selected");
        return false;
    } 
  	$('#DivisionModal').modal('hide');
	$("."+formEleClass+' .rulelist').prop('disabled', false); 
});

	$(document).on("change",".seasonlist",function(){

		$("#addssntext").val('');
		parenFormattr = $(this).closest(".multipleplayerformgrp").attr("class");	
		splitFormClass = parenFormattr.split(' ');
		formEleClass = splitFormClass[2];

		if ((this.value) == 'addnew') {
			$('#SeasonModal').modal('show');         
		}
	});

	$(document).on('click','#addssnbtn',function(){
	   	var newvalue=  $("#addssntext").val();
		if(newvalue !=""){  
		    $("."+formEleClass+' .seasonlist').append($('<option/>', { 
		        value: newvalue,
		        text : newvalue,
				selected:'selected' 
			}));
		    $("."+formEleClass+" .seasonlist option[value='']").removeAttr("selected","selected");
		} else {
		    $("#addssntext").keyup(function(){
                var seasonvalue=  $("#addssntext").val();
                if (seasonvalue != "") {
                    $("#seasonerror").hide();
                    return true;
                } else {
                    $("."+formEleClass+" .seasonlist option[value='']").attr("selected","selected");
                }
            });
            $("#seasonerror").show();
            $("."+formEleClass+" .seasonlist option[value='']").attr("selected","selected");
            return false;
		} 
	  	$('#SeasonModal').modal('hide');
	});

	$(document).on("click",".closeseasonmodal",function(){

		// $("."+formEleClass+" .seasonlist option[value='']").attr("selected","selected");
		$("."+formEleClass+ ' .seasonlist').prop('selectedIndex',0);
		$('#SeasonModal').modal('hide');
	});

	$('#SeasonModal').on('hidden.bs.modal', function () {
        var conferencevalue = $("#addssntext").val();
        if (conferencevalue == "") {
            $("."+formEleClass+' .seasonlist').prop('selectedIndex',0);
        }
    })

    $('#DivisionModal').on('hidden.bs.modal', function () {
        var divisionvalue = $("#adddivtext").val();
        if (divisionvalue == "") {
            $("."+formEleClass+ ' .divisionlist').prop('selectedIndex',0);
        }
    })

	// Add class in form element
	if (!($(".multipleplayerformgrp").hasClass("bultplayentrycont_1"))) {
		$(".multipleplayerformgrp").addClass("bultplayentrycont_1");
	}
	$(document).on('click','.addmoreplayerbtn',function(){
		var $this  = $(this);
		var AddPlayerHTML  = $('#addmoreplayercont').html();
		var FormEntryLength = $('#bultplayentrycont').length+1;
		var countAppend = $('#bultplayentrycont form').length+1;

		$('#bultplayentrycont').append(AddPlayerHTML);
		$('#bultplayentrycont').find("form").last().removeClass("bultplayentrycont_1");
		$('#bultplayentrycont').find("form").last().addClass("bultplayentrycont_"+countAppend);

		if ($(".bultplayentrycont_"+countAppend+" .dategame").attr("login") == "team_manager") {
			$(".bultplayentrycont_"+countAppend+" .dategame").datepicker({
				// minDate: 0,
				startDate: '+0d',
				format: "mm/dd/yyyy",
			    autoclose: true,
			});
		} else {
			$(".bultplayentrycont_"+countAppend+" .dategame").datepicker({
				format: "mm/dd/yyyy",
			    autoclose: true,
			});
		}	
		$('.timepicker').timepicker({defaultTime: false,});			
		
	});

	$(document).on('keyup','.hometeam',function(){
		var parenFormClass = $(this).closest(".multipleplayerformgrp").attr("class");	
		var splitClass = parenFormClass.split(' ');
		var formElementClass = splitClass[2];
		var search_this = $(this).val();
		var sportid = $("#sportid").val();
		$("."+formElementClass+" .display_home_result").show();

		$.post("find_teams.php", {search_team_like : search_this}, function(data) {
			$("."+formElementClass+" .display_home_result").html(data);
			if (($(data).hasClass("noteams"))) {
				$(".noteams").parent().parent().css("margin-top","14px");
			} else {
				$("."+formElementClass+" .hometeamformgroup").css("margin-top","0px");
			}
		});
	});

	$(document).on('keyup','.visitorteam',function(){
		var parenFormClass = $(this).closest(".multipleplayerformgrp").attr("class");
		var splitClass = parenFormClass.split(' ');
		var formElementClass = splitClass[2];
		var search_this = $(this).val();
		var sportid = $("#sportid").val();
		$("."+formElementClass+" .display_visitorteam_result").show();
		
		$.post("find_teams.php", {search_team_like : search_this}, function(data) {
			$("."+formElementClass+" .display_visitorteam_result").html(data);
			if (($(data).hasClass("noteams"))) {
				$(".noteams").parent().parent().css("margin-top","14px");
			} else {
				$("."+formElementClass+" .visitorteamformgroup").css("margin-top","0px");
			}
		});
	});

	$(document).on("click","#team-append .appendteams",function(){

		var parenFormClass = $(this).closest(".multipleplayerformgrp").attr("class");	
		var splitClass = parenFormClass.split(' ');
		var formElementClass = splitClass[2];

		var selTeam = $(this).attr("teamname");
		var eleAppendclass = $(this).closest("div").parent().siblings("input").attr("class").split(' ');
		var eleAppend = eleAppendclass[2];		
		var eleHideclass = $(this).closest("div").parent().attr("class").split(' ');
		var eleHide = eleHideclass[1];
		var eleHideParent = $(this).closest("div").parent().attr("id");
		
	    $.when( appendSelected(formElementClass, selTeam, eleAppend, eleHide), checkTeamNames(formElementClass, eleAppend)).done( function(result1, result2) {
			
	    });	

	});

	function checkTeamNames(formElementClass, eleAppend) {

		var dfd = jQuery.Deferred();
		var visitorteam = $("."+formElementClass).find("#visitor").attr("team-name");
		var hometeam = $("."+formElementClass).find("#home").attr("team-name");
		
		if (visitorteam !='' && hometeam !='') {
		
			$("."+formElementClass).find("#visitor").css('border','1px solid #c2cad8');
			$("."+formElementClass).find(".visitor").focus();
			$("."+formElementClass).find("#home").css('border','1px solid #c2cad8');
			$("."+formElementClass).find(".home").focus();

		}
		return dfd.promise();
	}
	function appendSelected(formElementClass, selTeam, eleAppend, eleHide) {
		
		var dfd = jQuery.Deferred();  
		var isEleappend =  $("."+formElementClass+" ."+eleAppend).val(selTeam);
		var isEleappendattr =  $("."+formElementClass+" ."+eleAppend).attr("team-name", selTeam);
		var isEleHide = $("."+formElementClass+" ."+eleHide).hide();
		dfd.resolve( "Appended Seleceted Team" );
		
		return dfd.promise(); 
	}
	