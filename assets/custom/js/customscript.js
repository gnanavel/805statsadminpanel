function deletee(id) {
	if(id == "donotdelete"){
		alert("You can't delete stream with video(s)");
		return false;
	}
	var x=confirm("Are you sure want to delete?");
	if (x==true)
	{
	  var xhttp = new XMLHttpRequest();
	  xhttp.onreadystatechange = function() {
		if (xhttp.readyState == 4 && xhttp.status == 200) {
		 window.location.href = window.location.href.replace( /[\?#].*|$/, "?msg=9" );
		}
	  }
	  xhttp.open("GET", "deletevideo.php?id="+id, true);
	  xhttp.send();
	}
}
function disp_video(id,action) {

	var x=confirm("Are you sure want to "+action+"?");
	if (x==true)
	{
	  var xhttp = new XMLHttpRequest();
	  xhttp.onreadystatechange = function() {
		if (xhttp.readyState == 4 && xhttp.status == 200) {
		  if(action=='show'){
			window.location.href = window.location.href.replace( /[\?#].*|$/, "?msg=5" );
		  }else {
			window.location.href = window.location.href.replace( /[\?#].*|$/, "?msg=6" );
		  }
		}
	  }
	  xhttp.open("GET", "video_hide.php?id="+id, true);
	  xhttp.send();
	}
}

function final(cid,id) {
	var x=confirm("ARE YOU SURE? ONLY PERFORM THIS WHEN THE VIDEO IS FINISHED");
	if (x==true)
	{
	  var xhttp = new XMLHttpRequest();
	  xhttp.onreadystatechange = function() {
		if (xhttp.readyState == 4 && xhttp.status == 200) {			
			window.location.href = window.location.href.replace( /[\?#].*|$/, "?msg=10" );
		}
	  }
	  xhttp.open("GET", "finalize.php?id="+id+"&cid="+cid, true);
	  xhttp.send();
	}
}
/*
$(document).ready(function(){
    $("#eu").click(function(){
        $("#eud").slideToggle();
		$("#pud").hide();
    });

    $("#tsbtn").click(function(){
        $("#tscode").slideToggle();
    });

	$("#pu").click(function(){
		$("#eud").hide()
		$("#pud").slideToggle();
    });
});
*/
/*$(document).ready(function(){
	$(".getencodeurlicon").click(function(){
		
		$(this).closest("tr").next("tr.encoder_wrap_row").show().find(".encoder_wrap").slideToggle('fast');
		$("#pud").hide();

	});
});*/

$(document).ready(function () {

   
	var globname='';
	var globrow='';
	$(".slidetogglemain").click(function(){
		var dataname = $(this).attr('data-name');
		var datarow = $(this).attr('data-row');
		var prevtdname ='';
		var tdname ='';
		tdname = dataname+"_"+datarow;
		if(globname!='') {
			prevtdname = globname+"_"+globrow;
			if(prevtdname!=tdname) {
				$($(".toggledropcont[data-id='"+prevtdname+"']")).slideUp( "slow", function() {
					$($(".toggledropcont[data-id='"+tdname+"']")).slideDown( "slow", function() {
						
					});
				});
			}
			else {
				$($(".toggledropcont[data-id='"+prevtdname+"']")).slideUp( "slow", function() {
					dataname='';datarow='';
				});
			}
		}
		else {
			$( $(".toggledropcont[data-id='"+tdname+"']")).slideToggle( "slow", function() {
			});
		}
		globname = dataname;
		globrow = datarow;	
	});
});



/**** create stream script start here ***/ 



function category(str) {
	if(str!='')
	{
	  var xhttp = new XMLHttpRequest();
	  xhttp.onreadystatechange = function() {
		if (xhttp.readyState == 4 && xhttp.status == 200) {
		  document.getElementById("category").innerHTML = xhttp.responseText;

	$("#date").mask("99/99/9999");
	$("#time").mask("99:99");
		}
	  }
	  xhttp.open("GET", "video_category.php?id="+str, true);
	  xhttp.send();
	}
}

/**** create stream script end here ***/ 


/**** upload video script start here ***/

function upload_video_category(str) {
		if(str!='')
		{  
		  var xhttp = new XMLHttpRequest();
		  xhttp.onreadystatechange = function() {
			if (xhttp.readyState == 4 && xhttp.status == 200) {
				document.getElementById("upload_video_cat").innerHTML = xhttp.responseText;
			}
		  }
		  xhttp.open("GET", "category_manual.php?id="+str, true);
		  xhttp.send();
		  	
		}
	}

	var bar = $('.bar');
	var percent = $('.percent');
	//var status = $('#status');
	
	$(document).ready(function(){
		
		$(document).on("submit", '.video_form', function(e) { 

			var ext = $('.input_video').val().split('.').pop().toLowerCase();			
			e.preventDefault();
			// submit the form 
			if( $('.input_video').val() == ""){
				alert("please upload file");
				return false;
			} else if($.inArray(ext, ['m4v','flv','mp4','mpg']) == -1) {
				alert('Invalid file, please upload video file.');
				return false;
			}
			//console.log($(this));
			
			$(this).ajaxSubmit({
				beforeSend: function() {
					var percentVal = '0%';
					$('.bar').width(percentVal);
					$('.percent').html(percentVal);
				},
				uploadProgress: function(event, position, total, percentComplete) {
					progress_div.style.display='block';
					var percentVal = percentComplete + '%';
					$('.bar').width(percentVal);	
					$('.progressbar').css({'margin-top': '-16px' });	
					console.log($('.progressbar'));
					$('.percent').css({'color': '#fff' });	
					$('.percent').html(percentVal);
					//console.log(percentVal);
				},
				complete: function(xhr) {
					progress_div.style.display='none';
					window.location.href = window.location.href.replace( /[\?#].*|$/, "?msg=8" );
					return false;
					$('#status').text("completed");
					//status.html(xhr.responseText);
				}
			});
		});
		// Event Validation
		$(document).on("submit", '#eventform', function(e) { 
			// submit the form 
			if( $('#ename').val() == ""){
				alert("please enter the  field");
				return false;
			}
			if( $('#date').val() == ""){
				alert("please enter the  date");
				return false;
			}
			if( $('#time').val() == ""){
				alert("please enter the  time");
				return false;
			}
			if( $('#descp').val() == ""){
				alert("please enter the  decscription");
				return false;
			}

			//console.log($(this));
			
			
		});

	});
/**** upload video script end here ***/


$(function() {
    $('.slidetogglemain').hover(function(){
        $(this).addClass('tooltipped tooltipped-s');
		$(this).attr('aria-label',$(this).attr("data-display"));
    }, function(){
        $(this).removeClass('tooltipped tooltipped-s');		
    });
});


$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip(); 
});

$(document).ready(function(){
    //$('.videolisttble').DataTable();
	
	$(".createstreambtn").click(function(){
		$(".uploadinnercont").slideUp();
		$(".tvstationinnercont").slideUp();	
		$(".searchinnercont").slideUp();
        $(".streaminnercont").slideToggle();
		
    });
	$(".uploadvideoicon").click(function(){
		$(".streaminnercont").slideUp();
		$(".tvstationinnercont").slideUp();		
		$(".searchinnercont").slideUp();
        $(".uploadinnercont").slideToggle();
		
    });

	$(".tvstationcodeicon").click(function(){
		$(".streaminnercont").slideUp();
		$(".uploadinnercont").slideUp();	
		$(".searchinnercont").slideUp();
        $(".tvstationinnercont").slideToggle();		
    });

	$(".searchteambtn").click(function(){		
		$(".streaminnercont").slideUp();
		$(".uploadinnercont").slideUp();		
        $(".tvstationinnercont").slideUp();	
		$(".searchinnercont").slideToggle();	
    });
	$(".closetogglesrchbtn").click(function(){
		$(".searchinnercont").slideUp();	
	});	
	$(".resetsearch").click(function(){
		$(".searchinnercont").slideUp();
		$(".teamnamesearch").val('');
		$(".searchdatepicker").val('');
		window.location.href = window.location.href;
	});	

	
});


function deletestreamvideo(cid,id,rid) {
	if(confirm("Are you sure you want to delete?")==true)
	{
		var xhttp = new XMLHttpRequest();
		xhttp.onreadystatechange = function() {
			if (xhttp.readyState == 4 && xhttp.status == 200) {				
				window.location.href = window.location.href.replace( /[\?#].*|$/, "?msg=8" );
			}
		};
		xhttp.open("GET", "deletestream.php?cid="+cid+"&id="+id+"&rid="+rid, true);
		xhttp.send();
	}
}


$(".fancybox").fancybox({
    openEffect  : 'none',
    closeEffect : 'none',
    iframe : {
        preload: false
    }
});

function delete_live_videos(id,videocode,recordid,filename) {
	if(confirm("Are you sure you want to delete?")==true)
	{
		var xhttp = new XMLHttpRequest();
		xhttp.onreadystatechange = function() {			
			//console.log(xhttp);			
			if (xhttp.readyState == 4 && xhttp.status == 200) {				
				window.location.href = window.location.href.replace( /[\?#].*|$/, "?msg=9" );
			}
		};
		xhttp.open("GET", "deleteaws.php?id="+id+"&videocode="+videocode+"&recordid="+recordid+"&filename="+filename, true);
		xhttp.send();
	}
}
$( function() {
	$("#searchintble").click(function(){
		
		if(($(".teamnamesearch").val()=='') && ($(".searchdatepicker").val()=='')){			
			alert("Please enter team name or date");
			$(".teamnamesearch").focus();
			return false;
		}else{
			$(".searchform").submit();
		}
	});

	$(".resetserchbtn").click(function(){
		$(".searchinnercont").slideUp();
		$(".teamnamesearch").val('');
		$(".searchdatepicker").val('');
		//location.reload();
		window.location.href = window.location.href;
	});
});
$( function() {
	$("#searchdatepicker").mask("99/9999");
} );

