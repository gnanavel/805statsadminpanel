$(document).ready(function() {

 $('input[id=first_name]').blur(function() {
            if ($(this).val() == '') {
              $('#first_name-error').show(); 
            }else{
			 $('#first_name-error').hide(); 
			}
     });
$('input[id=last_name]').blur(function() {
            if ($(this).val() == '') {
              $('#last_name-error').show(); 
            }else{
			 $('#last_name-error').hide();
			// $('#duplicatelast_name-error').hide();
			}
     });
$('input[id=uniform_no]').blur(function() {
            if ($(this).val() == '') {
              $('#uniform_no-error').show(); 
            }else{
			 $('#uniform_no-error').hide(); 
			}
     });
$('#sel_team').on('change', function() {
		if ($(this).val() == '') {
              $('#sel_team-error').show(); 
            }else{
			 $('#sel_team-error').hide(); 
			}
});
});
$(document).on('click',"#btn_close",function(){
	$('.updateplayerform').on('shown.bs.modal', function() {
		$('.updateplayerform').find('.modal-body').empty();
	});
});
$(document).on('click',".editsubmitform", function() {
	var RegExpression = /^[a-zA-Z\s']*$/;  
    var allvalid=true;	
	var fname=$("#first_name").val();
	var lname=$("#last_name").val();
	var uniform=$("#uniform_no").val();
	var team=$("#sel_team").val();
	var playerid=$("#edit_player_id").val();
	
		if(fname==''){
			$('#first_name-error').show(); 
			allvalid= false;
		}
		if(lname==''){
			$("#last_name-error").show();
			allvalid= false;
		} if(uniform==''){
			$("#uniform_no-error").show();
			allvalid= false;
		} if(team==''){
			$("#sel_team-error").show();
			allvalid= false;
		}
		if(!allvalid){
			return false;
		}
		 /*$.ajax({
            method: "POST",
           url:"updateplayer_exists.php",  
            method:'POST',
            data:{firstname: fname, lastname: lname,playerid:playerid},
			async:false,
            }).success(function(response) {
				//console.log(response);
				if(response=='notexits'){
				   $("#frm_manage_player").submit();	
					return true;
					
			   }else  if(response=='exits'){
					 $("#duplicatelast_name-error").show();  
					 return false;
			   }
		  });*/	
		
});

$(document).on('click',".addsubmitform", function() {
	var RegExpression = /^[a-zA-Z\s']*$/;  
    var allvalid=true;	
	var fname=$("#first_name").val();
	var lname=$("#last_name").val();
	var uniform=$("#uniform_no").val();
	var team=$("#sel_team").val();
		
		if(fname==''){
			$('#first_name-error').show(); 
			allvalid= false;
		}
		if(lname==''){
			$("#last_name-error").show();
			allvalid= false;
		} if(uniform==''){
			$("#uniform_no-error").show();
			allvalid= false;
		} if(team==''){
			$("#sel_team-error").show();
			allvalid= false;
		}

		if(!allvalid){
			return false;
		}	
		
		 $.ajax({
            method: "POST",
           url:"addplayer_exists.php",  
            method:'POST',
            data:{firstname: fname, lastname: lname},
      
            }).success(function(response) {
				//console.log(response);
				var response =$.parseJSON(response);
				if(response.playerstatus=='playerexits'){					
						var PlayersLength  =  response.playerdetails;
						var PlayersHtml    = '';
						var PlayerCode = '';
						for(var i=0;i<PlayersLength.length;i++ ){
							var PlayersFirstname = PlayersLength[i].Firstname;
							var PlayersLastname  = PlayersLength[i].Lastname;
							var PlayersImage     = PlayersLength[i].PlayerImg;
							var PlayerID         = PlayersLength[i].PlayerID;
						PlayersHtml +='<div class="col-md-12" style="border: 1px solid #CCC;padding-bottom: 25px;    padding-top: 10px;"><p>This Player already exist with Player ID:<span id="playerid"><b>'+PlayerID+'</b></span></p><img src="uploads/players/'+PlayersImage+'" alt="" width="50" height="50" style="float:left;"><div class="col-md-3" style="padding-top: 10px;">First Name:<b> '+PlayersFirstname+'</b></div><div class="col-md-3" style="padding-top: 10px;">Last Name:<b> '+PlayersLastname+'</b></div><div class="col-md-3"><button type="button" class="btn btn_purple updateplayer" id="updateplayer"  style="margin-top: 15px;">Update this player Info</button></div></div>';
							PlayerCode += PlayerID;
						}

						 PlayersHtml = '<div class="popupplayercont" style="display:block;">'+PlayersHtml+'<button type="button" class="btn btn-success addnewplayer" id="addnewplayer" style="margin-top: 15px;font-size: 13px;">Add as New Player</button><div class="loadingimgcont"><img src="images/playerloading.gif" style="margin:auto; width: 80px;" class="loadingimg"><div class="alert alert-success" id="updatemsg" style="display: none;">Details updated successfully</div></div>';
						 //console.log(PlayersHtml);
						 $('.updateplayerform').modal('show');
						 $('.updateplayerform').on('shown.bs.modal', function() {
							$('.updateplayerform').find('.modal-body').append(PlayersHtml);
						});	
						return false;
					}else{
						 console.log(response.playerstatus);
						 $("#frm_manage_player").submit();	
						return true;
					}
            });	
});



$(document).on('click','.updateplayer,.addnewplayer',function(){
	var $this    = $(this);
	var $thisaddchk = $this.attr('id');
	var PlayerID = $this.closest('.col-md-12').find('#playerid>b').text();
	$("#hnd_player_id").val(PlayerID);
	$("#hnd_addplayer").val($thisaddchk);
	var form_data	   =   $("#frm_manage_player").serialize();
	var player_id 	   =   $('input[name="hnd_player_id"]').val();
	var type 	   =   $('input[name="hnd_addplayer"]').val();
	var info_form_data =   new FormData();
	var info_file_data =   $('#player_image').prop('files')[0]; 
	info_form_data.append('info_file', info_file_data);
	info_form_data.append('form_data', form_data);
	info_form_data.append('player_id', player_id);
	info_form_data.append('type', type);
	$.ajax({
	        url:"updateplayer.php",  
            type: "post",
			cache: false,
			contentType: false,
			processData: false,
            data:info_form_data,
			async:false,
			success:function(response) {
			   if(response=='AddSuccess'){
			 window.location.href = "player_list.php?msg=1";
			   }else if(response=='EditSuccess'){
			 window.location.href = "player_list.php?msg=2";
			   }
				
		   }
	});
	

});

