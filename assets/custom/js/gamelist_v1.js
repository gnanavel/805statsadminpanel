
var arrMonth = "";
var defaultSeason = ($("#searchbyseasonid").val())?$("#searchbyseasonid").val() : "";
$.ajax({
    url: "game_season_month_v1.php",
    type: "POST",
    data: {season : defaultSeason},
    success: function(data){
        arrMonth = $.parseJSON(data);        
    }
});
var counter = 0;
var previous = 0;
var countGame;
$(function () { 
    
    $( "#searchbyseasonid" ).on( "change", function() {
        
        defaultSeason = $("#searchbyseasonid").val();

        $.ajax({
            url: "game_season_month.php",
            type: "POST",
            data: {season : defaultSeason},
            success: function(data){
                arrMonth = $.parseJSON(data); 
                console.log(arrMonth.length);
                counter = -1; 
                ajaxGameList($('#gamedate').val()); 

            }
        });

    });


    $(document).on( "click", ".next" , function() {
        

        $('#gamedate').val('');
        if (counter == 0)
            counter = 0;
        counter = (counter + 1) % arrMonth.length; 
        var countofmonth = arrMonth.length;
        
        ajaxGameList(arrMonth[counter]);
        $('#gamedate').val(arrMonth[counter]);
        

    });
    $(document).on( "click", ".prev" , function() {
        $('#gamedate').val('');
        
        if (counter <= 0) {
            counter = arrMonth.length;
        }        
        counter = counter-1;         
        var countofmonth = arrMonth.length;
        ajaxGameList(arrMonth[counter]);
        $('#gamedate').val(arrMonth[counter]);       
        
    });
   
});

var gamemonth = '';
var initialloadmonth = '';
$(document).ready(function() {

    initialloadmonth = $('.item.active').attr('month-name');
    
    if (initialloadmonth != "")
        $('.gamemonth strong').text(initialloadmonth);
    $(document).on( "click", ".right" , function() {
        var nextMonthName = $('.item.active').next().attr('month-name');        
        $('.gamemonth strong').text(nextMonthName);
        if (nextMonthName == undefined) {
            $('.gamemonth strong').text($('div.item').first().attr('month-name'));
        }
    });

    $(document).on( "click", ".left" , function() {       
        var prevMonthName = $('.item.active').prev().attr('month-name');
        $('.gamemonth strong').text(prevMonthName); 
        if (prevMonthName == undefined) {
            $('.gamemonth strong').text($('div.item').last().attr('month-name'));
        }    
    });
 
});

$( "#resetbtn" ).click(function() {        
    document.location='game_list.php';

});

$(document).on("click",".searchgamefilter",function(){
   ajaxGameList($('#gamedate').val());
});
$(document).on("click",".searchgameabafilter",function(){
   ajaxGameList($('#gamedate').val());
});

function ajaxGameList(month) {

    $('#game-carousal').hide();
    $(".loadingsection").show();
   
    // var Searchbydiv   = ($('#searchtext').val()) ? $('#searchtext').val() : "" ;
    var Searchbyteam   = ($('#searchtext').val()) ? $('#searchtext').val() : "" ;
    var Searchbyseason = ($('#searchbyseasonid').val()) ? $('#searchbyseasonid').val() : "";              
    // var CustomerID     = ($('#customerid').val()) ? $('#customerid').val() : "";
    var SportID        = ($('#sportid').val()) ?$('#sportid').val() : "";   
    var ls             = ($("#tableid").val()) ? $("#tableid").val() : "";
    var gamedate = ($("#gamedate").val()) ? $("#gamedate").val() : "";

    var monthyear = (month !='') ? month: "" ;
    
    $.ajax({
        url:"ajax_game_list_v1.php",  
        method:'GET',
        // async: false,
        data:"sportid="+SportID+"&searchbyseason="+Searchbyseason+"&Searchbyteam="+Searchbyteam+"&ls="+ls+"&gamedate="+gamedate+"&monthyear="+monthyear,
        success:function(data) {         
            $('#game-carousal').show(); 
            $(".loadingsection").hide();
            $("#myCarousel .carousel-inner .item.active").addClass("left");
            // setTimeout(function () {
                $('#myCarousel .carousel-inner').empty().append(data);
                $('.gamemonth strong').text(monthyear);
                $("#myCarousel .carousel-inner .item.active").removeClass("left");
            // },500);
        }
    });
} 

function deleteGame(id,sportname){

    var answer = confirm('Are you sure you want to delete?');
    if(answer) {
        window.location = "delete_game.php?action=delete&gid="+id+"&sportname="+sportname;
    } else {
        alert("Cancelled the delete!")
    }
}

function daysInMonth(month,year) {
    return new Date(year, month, 0).getDate();
}

var options = {
    selectedYear: 2016,
    startYear: 2008,
    finalYear: 2020,
    openOnFocus: false
};

$('#datepicker').monthpicker(options);
$('#datepicker-button').bind('click', function () {
    $('#datepicker').monthpicker('show');
});

$('#datepicker').monthpicker().bind('monthpicker-click-month', function (e, month) {

}).bind('monthpicker-change-year', function (e, year) {

}).bind('monthpicker-show', function () {

}).bind('monthpicker-hide', function () {
    
    var monYear = $(this).val();
    var monthYearsplit = monYear.split("/");
    var totalmonths = [ "January", "February", "March", "April", "May", "June", 
               "July", "August", "September", "October", "November", "December" ];
    var monthnum = monthYearsplit[0];
    var yearnum = monthYearsplit[1];
    var subMonthnum = totalmonths[monthnum-1];
    // var subYearnum = yearnum.substring(2,4);
    var ajaxmonthYear = subMonthnum+", "+yearnum;
    $('#gamedate').val(ajaxmonthYear);
    // $(".left.carousel-control").hide();
    if (monYear != '')
        ajaxGameList(ajaxmonthYear);

});

$(document).ready(function() {
	$(document).on("click", ".combineplayer", function () {

        //Hide success/failure messages and input field while open modal
        $(".cobmineform, #popupcombinebtn").show();       
        $(".cancelbtn").css("margin-left", "14px");
        $(".cobminestatus, .cobminestatusfailure, #duplicateplayeriderror, #playeriderror").hide();
        $("#playerid").val("");
        $("#duplicateplayerid").val("");


 		var game_id = $(this).attr('data-id');
        var season = $(this).attr("data-season");
        var date = $(this).attr("data-date");
        var gameinfoid = $(this).attr("data-gameinfoid");

        $('#combine_gameid').val(game_id);
        $('#combine_season').val(season);
        $('#combine_date').val(date);
        $('#combine_gameinfoid').val(gameinfoid);
        $("#game_view_id").text(game_id);

        $( "#CombineModal" ).modal("show");

	});

	var response; 
	$.validator.addMethod('uniquedivision',function(value){
		var game_id=$('input[name="combine_gameid"]').val();
        var post_type="checkpalyer";
		var playerid=$("#playerid").val();
		var duplicateplayerid=$("#duplicateplayerid").val();
        var gameinfo_id =$("#combine_gameinfoid").val();
		
        $.ajax({
            method: "POST",
            url: "checkgamewithplayer-ajax.php",
            async:false,
            data:{"post_type":post_type,"gameid":game_id,"playerid":playerid,"duplicateplayerid":duplicateplayerid, "gameinfoid": gameinfo_id},  
        }).success(function(msg) {
            response = msg;
        });

        if(response == "exists") {
            return true;
        } else {
            return false;    
        }
   });    

});



