<?php
include_once('session_check.php');
include_once('connect.php'); 
unset($_SESSION['divisionid']);
unset($_SESSION['seasonid']);
unset($_SESSION['conferenceid']);
/*Sports table*/
$SportId= $_SESSION['sportid'];
$Sports = array();
$SportListArr = array();
$SportsLists = $conn->prepare("select * from customer_subscribed_sports where customer_id=:customer_id");
$SportListArr = array(":customer_id"=>$customerid);
$SportsLists->execute($SportListArr);
$CntSportsLists = $SportsLists->rowCount();
if ($CntSportsLists > 0) {
    $SporstRes = $SportsLists->fetchAll(PDO::FETCH_ASSOC);
    foreach ($SporstRes as $SporstRow) {
        $Sports[]= $SporstRow['sport_id']; 
    }

    if ($Sports[0]=='4444') { $tablename='team_stats_bb'; } 
    if ($Sports[0]=='4442' || $Sports[0]=='4441') { $tablename='team_stats_ba'; } 
    if ($Sports[0]=='4443') { $tablename='team_stats_fb'; }
	}

if(isset($_POST['seasonid'])){
	$SeasonId      = $_POST['seasonid'];
	$PostType      = $_POST['post_type'];
	$_SESSION['seasonid']  = $SeasonId;
	$SeasonOptions = '';
	if($PostType == "seasonlist"){
		$_SESSION['conferenceid']	= '';		
		$_SESSION['divisionid']		= '';

		$QryExe = $conn->prepare("select * from customer_season_conference as seasonconf LEFT JOIN customer_conference as custconf ON  seasonconf.conference_id=custconf.id where season_id=:season_id");
		$Qryarr = array(":season_id"=>$SeasonId);
		$QryExe->execute($Qryarr);
		$QryCntSeasonconf	= $QryExe->rowCount();											
		
		if ($QryCntSeasonconf > 0) {
			$SeasonOptions     .= "<option value=''>Select conference</option>";
			while ($rowSeason   = $QryExe->fetch(PDO::FETCH_ASSOC)){				
				$SeasonOptions .= "<option value='".$rowSeason['conference_id']."'>".$rowSeason['conference_name']."</option>";
			}
		}else{
			$SeasonOptions   ="<option value=''>No conference found</option>";
		}
	}else if($PostType == "conferencelist"){
		$SeasonId					= $_POST['seasonid'];
		$conferenceid				= $_POST['conferenceid'];
		$_SESSION['conferenceid']	= $conferenceid;		
		$_SESSION['seasonid']		= $SeasonId;

		$QryExeDiv = $conn->prepare("select * from customer_conference_division as seasonconfdiv LEFT JOIN customer_division as custconf ON  seasonconfdiv.division_id=custconf.id where seasonconfdiv.conference_id=:conference_id and season_id=:season_id");
		$QryarrCon = array(":conference_id"=>$conferenceid,":season_id"=>$SeasonId);

		$QryExeDiv->execute($QryarrCon);
		$QryCntSeason = $QryExeDiv->rowCount();
		$DivisionWrapHtml= $AddNewSeasonTree='';
		$Inc =0;
		if ($QryCntSeason > 0) {
			//$SeasonOptions     .= "<option value=''>Select division</option>";
			$SeasonOptions     .= "";
			while ($row = $QryExeDiv->fetch(PDO::FETCH_ASSOC)){							
				$SeasonOptions     .= "<option value='".$row['id']."'>".$row['name']."</option>";
			}
		}else{
			$SeasonOptions     = "<option value=''>No division found</option>";
		}
	
	}else if($PostType == "divisionlist"){
		$conferenceid				= $_POST['conferenceid'];		
		$divisionid					= $_POST['divisionid'];
		$_SESSION['divisionid']		= $divisionid;	
		$_SESSION['conferenceid']	= $conferenceid;
		
		$QryExe1		= $conn->prepare("select * from customer_division_team where customer_id=:customer_id and season_id=:season_id and (conference_id!=:conference_id OR division_id!=:divisionid)");
		
		$Qryarr = array(":customer_id"=>$customerid,":season_id"=>$_SESSION['seasonid'],":conference_id"=>$_SESSION['conferenceid'],":divisionid"=>$_SESSION['divisionid']);

		$QryExe1->execute($Qryarr);
		$QryCntTeam = $QryExe1->rowCount();
		$TeamIdArr  = array();

		if ($QryCntTeam > 0) {
			while ($rowTeam = $QryExe1->fetch(PDO::FETCH_ASSOC)){	
				$TeamIdArr[] = $rowTeam['team_id'];
			}
		}
		
		?>
		                           
			<div class="row">                
				<div class="col-md-12">                              
					<div class="portlet-body form">
						<div class="form-body top-padding" style="padding-top:5px;"> 
							<!-- <h4 id="demo-undo-redo">Undo / Redo</h4> -->
							<div class="row">
								<div class="col-xs-5 col-md-5">									
									<select name="from[]" id="undo_redo" class="form-control border-radius " size="13" multiple="multiple">
									<?php		
									if ($_SESSION['master'] == 1) { 
											$children = array($_SESSION['childrens']);					
											 $ids = $_SESSION['loginid'].",".join(',',$children);
											 $res = "SELECT * FROM teams_info WHERE (customer_id IN ($ids) or customer_id IN ($ids))  and team_name!='' and (sport_id='$SportId') order by team_name"; 
									} else {
											$res = "select teams_info.* from teams_info  LEFT JOIN $tablename ON teams_info.id=$tablename.teamcode where teams_info.customer_id in ($customerid) and team_name!='' group by teams_info.id order by teams_info.team_name";
									}		
									$QryExe1		= $conn->prepare($res);									
									$QryExe1->execute();
									$QryCntSeason = $QryExe1->rowCount();
									$DivisionWrapHtml= $AddNewSeasonTree='';
									$Inc =0;
									$AssignedTeams ='';
									if ($QryCntSeason > 0) {
										while ($row = $QryExe1->fetch(PDO::FETCH_ASSOC)){									
											
											if(in_array($row['id'],$TeamIdArr)){ 												
													$AssignedTeams .= "<option value='".$row['id']."' disabled>".$row['team_name']."</option>";
											}else{																		
												echo "<option value='".$row['id']."'>".$row['team_name']."</option>";
											}
										}
										echo $AssignedTeams;
									}else{
										echo "<option value=''>No team found</option>";
									}
									?>	
									</select>
								</div>
								
								<div class="col-xs-2 col-md-2 centeredbtnswrap">									
									<button type="button" id="undo_redo_rightAll" class="btn btn-primary btn-block"><i class="glyphicon glyphicon-forward"></i></button>
									<button type="button" id="undo_redo_rightSelected" class="btn btn-default btn-block"><i class="glyphicon glyphicon-chevron-right"></i></button>
									<button type="button" id="undo_redo_leftSelected" class="btn btn-default btn-block"><i class="glyphicon glyphicon-chevron-left"></i></button>
									<button type="button" id="undo_redo_leftAll" class="btn btn-default btn-block"><i class="glyphicon glyphicon-backward"></i></button>									
								</div>
								
								<div class="col-xs-5 col-md-5 rightsidewrap" >									
									<select name="selectedteam[]" id="undo_redo_to" class="form-control border-radius requiredcs" size="13" multiple="multiple">
									<option value="" class="emptyselected"></option>
									<?php											

										$QryExeTeam = $conn->prepare("select * from customer_division_team as divteam LEFT JOIN teams_info as custteam ON  divteam.team_id=custteam.id where divteam.conference_id=:conference_id and divteam.season_id=:season_id and divteam.division_id=:division_id");
										$QryarrCon = array(":conference_id"=>$_SESSION['conferenceid'],":season_id"=>$_SESSION['seasonid'],":division_id"=>$_SESSION['divisionid']);

										$QryExeTeam->execute($QryarrCon);
										$QryCntSeason = $QryExeTeam->rowCount();										
										
										if ($QryCntSeason > 0) {
											while ($rowTeam = $QryExeTeam->fetch(PDO::FETCH_ASSOC)){
												if($rowTeam['team_name']!='')
												echo "<option value='".$rowTeam['id']."'>".$rowTeam['team_name']."</option>";
											}
										}										
									?>
									</select>
									<div class="row">
										<div class="col-sm-6">
											<button type="button" id="undo_redo_move_up" class="btn btn-block"><i class="glyphicon glyphicon-arrow-up"></i></button>
										</div>
										<div class="col-sm-6">
											<button type="button" id="undo_redo_move_down" class="btn btn-block col-sm-6"><i class="glyphicon glyphicon-arrow-down"></i></button>
										</div>
									</div>

								</div>
							</div>
						</div> 
						
						 <div class="">
								<button type="button" class="btn green-meadowsave" name="addsubmit" id="addteambtnid">Save</button>
								<a href="manage_season.php"><button type="button" class="btn red" id="cancelbtn">Cancel</button></a>
						 </div>   
					</div>					           
				</div> 
            </div>  
			
			<script>
			$(document).ready(function() {
				$('#undo_redo').multiselect({
					sort:false,
					search: {
						left: '<input type="text" name="q" class="form-control searchteambox" placeholder="Search Team" /><label>Select Team</label>',
						right: '<p class="clearfix" style="margin-top:0px;margin-bottom: 3px;"><a href="manage_season.php"><button type="button" class="btn uppercase pull-left backbtnred">Back</button></a><button type="button" class="btn uppercase addplayerbtntop" style="float: right;">Assign Players</button></p><p class="clearfix" style="margin-top:0px;margin-bottom: 0px;"><label>Selected Team</label></p>',
					},
					afterMoveToRight: function($left, $right, $options) { }
				});


			$(document).on('click','#addteambtnid', function(evt) {
				if (!$("#addteamform").validate()) { 
					return false;
				} 	
				$("#addteamform").submit();		
			});


			$.validator.addMethod('requiredcs',function(value){
				if(value==''){
					return false;
				}else{
					return true;
				}
			},""
			);


$("#addteamform").validate({
	 rules: {
		 conferencelist:{requiredcs :true},
		 divisionlist:{requiredcs:true},
		 "selectedteam[]":{requiredcs:true},
		 },
		messages: {
		 conferencelist:{requiredcs:"Please select conference"},
		 divisionlist:{requiredcs:"Please select division"},
		 "selectedteam[]":{requiredcs:"Please add team"},
	   },
		submitHandler: function (form) {			
			var $form = $(form);
			var FormArr  = $form.serialize();	
			$("#undo_redo_to option").each(function()
			{
				$(this).prop('selected', true);
			});

			var seasonlist       =  $('#seasonlist').val();	
			var conferencelist   =  $('#conferencelist').val();
			var divisionlist     =  $('#divisionlist').val();
			var selectedteam     =  $('#undo_redo_to').val();

			$(".rightsidewrap").hide();
			$(".loadingwrap").show();

			$.ajax({
				 type:"POST",			
				 url:"assignteamtoseason.php",
				 data:{"seasonlist":seasonlist,"conferencelist":conferencelist,"divisionlist":divisionlist,"selectedteam":selectedteam},
				 success: function(data) {
					$(".rightsidewrap").show();
					$(".loadingwrap").hide();
					$( ".page-content").empty().append(result.responseText);
					return false;
				 },
				 error: function(data) {
					alert("something wrong");
					
				 },
			}); 		 
		}
	});

});		
			</script>
   

		<?php
	}
	echo $SeasonOptions;
}
?>