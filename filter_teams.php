<?php 
include_once('session_check.php'); 
include_once('connect.php');

if(isset($_REQUEST["HdnMode"])){
    $SortingColumnNum = $_REQUEST["SortingColumnNum"];
    $SortingColumnOrder = $_REQUEST["SortingColumnOrder"];
	$RecordsPerPage = $_REQUEST["PerPage"];
	$HdnMode = $_REQUEST["HdnMode"];
	$HdnPage = $_REQUEST["HdnPage"];
	$Page = 1;
    $_SESSION["team"]["HdnPage"] = $Page;
}
if (isset($_REQUEST['season'])) {

	$season          =  !empty($_REQUEST['season']) ? $_REQUEST['season'] : "" ;
    $seasoncondn = (!empty($season)) ? " and $ls.season='$season'" : "";

    $division          =  !empty($_REQUEST['division']) ? $_REQUEST['division'] : "" ;
    $divcondn = (!empty($division)) ? " and division='$division'" : "";

	$ls              =  $_REQUEST['ls'];
	$searchbyteam    =  $_REQUEST['searchbyteam'];
	$searchstatus    =  $_REQUEST['status'];

    $sportid = stripslashes($_SESSION["sportid"]);
    $SportName = $_SESSION["sportname"];


    $_SESSION["team"]["hdnsesid"] = $season;
    $_SESSION["team"]["hdndivid"] = $division;
    $_SESSION["team"]["hdnsearchteam"] = $searchbyteam;
    $_SESSION["team"]["hdn_status"] = $searchstatus;
    $_SESSION["team"]["SortingColumnNum"] = $SortingColumnNum;
    $_SESSION["team"]["SortingColumnOrder"] = $SortingColumnOrder;

    if ($searchstatus == "active") {
        $status = "and teams_info.archive='0'";
        $masterstatus = "and archive='0'";
    }
    if($searchstatus == "Inactive"){
        $status = "and teams_info.archive='1'";
        $masterstatus = "and archive='1'";
    }

    if ($_SESSION['logincheck'] != 'master') {        

        $HiddenContn = "";
        if (!empty($season) && !empty($searchbyteam)) {   
            $HiddenContn = " and $ls.season='$season' and teams_info.team_name like '%$searchbyteam%'";
        } else if (!empty($season) && empty($searchbyteam)) {
            $HiddenContn = " and $ls.season='$season'";
        } else if (empty($season) && !empty($searchbyteam)) {
            $HiddenContn = " and teams_info.team_name like '%$searchbyteam%'";
        } 

        $dbQry = "select teams_info.* from teams_info LEFT JOIN $ls ON teams_info.id=$ls.teamcode where teams_info.customer_id in ($LoginCustId) $HiddenContn $status group by teams_info.id order by teams_info.team_name";
    } else {        

        $HiddenContn = "";
        if (!empty($division) && !empty($searchbyteam)) { 
            $HiddenContn = " and division='$division' and team_name like '%$searchbyteam%'"; 
        } else if (!empty($division) && empty($searchbyteam)) {
            $HiddenContn = " and division='$division'";
        } else if (empty($division) && !empty($searchbyteam)) {
            $HiddenContn = " and team_name like '%$searchbyteam%'"; 
        } 
        $dbQry = "SELECT * FROM teams_info WHERE customer_id IN ($customerid) $HiddenContn $masterstatus and (sport_id='$sportid')  order by team_name";

    }
// echo $dbQry;
?>
 <form id="team_list" name="team_list" method="post" action="">
    <input type="hidden" name="HdnPage" id="HdnPage" value="<?php echo $Page; ?>">
    <input type="hidden" name="HdnMode" id="HdnMode" value="<?php echo $Page; ?>">
    <input type="hidden" name="RecordsPerPage" id="RecordsPerPage" value="<?php echo $RecordsPerPage; ?>">
    <input type="hidden" name="hdndivid" id="hdndivid" value="<?php echo $division;?>">
    <input type="hidden" name="hdnsesid" id="hdnsesid" value="<?php echo $season ?>">
    <input type="hidden" name="hdnsearchteam" id="hdnsearchteam" value="<?php echo $searchbyteam ?>">
	<input type="hidden" name="hdn_status" id="hdn_status" value="<?php echo $searchstatus;?>">
    <input type="hidden" name="SortingColumnNum" id="SortingColumnNum" value="<?php echo $SortingColumnNum;?>">
    <input type="hidden" name="SortingColumnOrder" id="SortingColumnOrder" value="<?php echo $SortingColumnOrder;?>">
        <table class="table table-striped table-bordered table-hover dataTable no-footer dataTable customerlist-tbl" id="sample_1" sytle="border: 1px solid #CCC;border-collapse: collapse;">
            <thead>
                <tr>
                    <th nowrap> Team&nbsp;ID </th>
                    <th nowrap> Team Name </th>
                    <th nowrap> Abbr </th>
                    <th nowrap> Division </th>
                    <th nowrap> Venue </th>
					<th nowrap> Video </th>
                    <th nowrap> Team&nbsp;Logo </th>
                    <th nowrap> Action </th>
                </tr>
            </thead>
            <tbody>
            <?php
            
            $getResQry      =   $conn->prepare($dbQry);
            $getResQry->execute();
            $getResCnt      =   $getResQry->rowCount();
            $TotalPages = '';
            if ($getResCnt > 0) {
                $TotalPages=ceil($getResCnt/$RecordsPerPage);
                $Start=($Page-1)*$RecordsPerPage;
                $sno=$Start+1;                                        
                $dbQry.=" limit $Start,$RecordsPerPage";
                $getResQry      =   $conn->prepare($dbQry);
                $getResQry->execute();
                $getResCnt      =   $getResQry->rowCount();

                if($getResCnt>0){
                    $getResRows     =   $getResQry->fetchAll(PDO::FETCH_ASSOC);
                    $s=1;
                    foreach ($getResRows as $team) { 

                        $isSuspended=$team['isSuspended'];
                        if($isSuspended=="1"){
                            $background="background-color:#D3D3D3 !important;";

                        } else {
                            $background="";
                        }

                        ?> 

                        <tr style="<?php echo $background; ?>">
                            <td nowrap style="<?php echo $background; ?>"><?php echo $team['id'] ?></td>
                            <td nowrap style="<?php echo $background; ?>"><?php echo $team['team_name']; ?></td>
                            <td nowrap style="<?php echo $background; ?>"><?php echo $team['abbrevation'] ?></td>
                            <td nowrap style="<?php echo $background; ?>">
                            <?php 
                            $divid= $team['division'];
                            $orgQry = $conn->prepare("select * from customer_division where id=:divid");        
                            $QryArr = array(":divid"=>$divid);        
                            $orgQry->execute($QryArr);                                        
                            $DivName = $orgQry->fetch(PDO::FETCH_ASSOC);
                            echo $DivName['name'];
                            ?></td>
                            <td nowrap style="<?php echo $background; ?>"><?php echo $team['stadium'] ?></td>
							<td nowrap style="<?php echo $background; ?>"><?php 
											$teamid = $team['id'];
											$custvideoQry=$conn->prepare("select * from customer_tv_station where team_id=:team_id and customer_id in (:customer_id)");
											$custQryArr = array(":team_id"=>$teamid,":customer_id"=>$LoginCustId); 
											$custvideoQry->execute($custQryArr);                                        
                                            $Video = $custvideoQry->fetch(PDO::FETCH_ASSOC);
											$videoactive=$Video['active'];
											if ($videoactive=='1') {
									           $videostatus='Y';
											} else {
                                                $videostatus='N';
											}
											echo $videostatus;
											?></td>
                            <td nowrap style="<?php echo $background; ?>">
                            <?php if($team['team_image']!=""){ ?>
                                <img src="uploads/teams/<?php echo $team['team_image']; ?>" alt="" width="25" height="25" />
                            <?php } ?>
                            </td>
                            <td nowrap style="<?php echo $background; ?>">
                            <?php 
                            // $GameQryArr = array();
                            // $teamid = $team['id'];
                            // $GameQry = $conn->prepare("select * from teams_info where id=:teamid");
                            // $GameQryArr = array(":teamid"=>$teamid);
                            // $GameQry->execute($GameQryArr);
                            // $CntGame = $GameQry->rowCount();
                            // if ($CntGame > 0) {

                            //     $GameRows = $GameQry->fetch(PDO::FETCH_ASSOC);
                            //     $customer_id = $GameRows['customer_id'];
                            //     $sportid = $GameRows['sport_id'];
                            // }

                            // $sportnquery = $conn->prepare("select * from sports where sportcode='$sportid'");
                            // $sportnquery->execute();
                            // $Cntsportnquery = $sportnquery->rowCount();
                            // if ($Cntsportnquery > 0) {
                            //     $sportrows = $sportnquery->fetchAll(PDO::FETCH_ASSOC);
                            //     foreach ($sportrows as $sportnames) {
                            //         $teamsportname = $sportnames['sport_name']; 
                            //         $teamsportname = strtolower($teamsportname);
                            //     }
                            // } else {
                            //     $teamsportname = "";
                            // }
                            ?>
                                <table class="table-hover filter_team_list">
                                    <tr style="<?php echo $background; ?>text-align:center;" >
                                        <td nowrap style="<?php echo $background; ?>" class="teamaction">
                                        <a href="manage_team.php?tid=<?php echo base64_encode($team['id']); ?>" class="roundbtngreenedit btn-circle btn-icon-only edit_btnteam"><i class="icon-note trash_btn"></i> 
                                            
                                        </a>
                                        </td >
                                        <td style="<?php echo $background; ?>" class="teamaction">   
                                        <a  class="roundbtnreddelete btn-circle btn-icon-only" onclick="return deleteTeam('<?php echo $team['id']; ?>','<?php echo $SportName; ?>');"><i class="icon-trash trash_btn"></i> 
                                            
                                        </a>                                                   
                                        
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    <?php $s++;}
                    } 
                    else {
                        // echo "<tr><td colspan='8' style='text-align:center;line-height:1.7em;'>No Team(s) found.</td></tr>";
                    }
            } else
             {
                // echo "<tr><td colspan='8' style='text-align:center;line-height:1.7em;'>No Team(s) found.</td></tr>";
            }
            ?>
            </tbody>
        </table>
        
            <?php
            if($TotalPages > 1){

            echo "<tr><td style='text-align:center;' colspan='8' valign='middle' class='pagination'>";
            $FormName = "team_list";
            require_once ("paging.php");
            echo "</td></tr>";

            }
           ?>
    </div>
    </form>
<?php }?>
<script>
// $('#sample_1').DataTable({
//      "retrieve": true,
//     "paging": false,
//     "bInfo": false,
//    "bFilter":false,
//     "bLengthChange":false,
//     "bPaginate":false,
//     "aaSorting": [[<?php echo $SortingColumn; ?>,'asc']],
//     "language": {
//         "zeroRecords": "No Team(s) found.",
//         "infoEmpty": "No Team(s) found."
//     },
//     "aoColumnDefs": [ { "bSortable": false, "aTargets": [7] } ], 
// });
</script>
<!-- <script src="assets/custom/js/teamlist.js" ></script> -->
