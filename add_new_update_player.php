<?php 
include_once('session_check.php'); 
include_once('connect.php');
include_once('usertype_check.php');

if (isset($_REQUEST["firstname"])) {

    $mode = $_REQUEST["mode"];
    $teamid = $_REQUEST["hiddenteamid"];
    $hidden_team_name = $_REQUEST["hiddenteamname"];
    $firstname = $_REQUEST["firstname"];
    $lastname = $_REQUEST["lastname"];
    $uniform_no = $_REQUEST["uniformno"];
    $position = $_REQUEST["position"];
    $gender = $_REQUEST["gender"];
    $height = $_REQUEST["height"];
    $weight = $_REQUEST["weight"];
    $dob = $_REQUEST["dob"];
    $age = $_REQUEST["age"];
    $school = $_REQUEST["school"];
    $grade = $_REQUEST["grade"];
    $prev_school = $_REQUEST["prevschool"];    
    $created_date = date("Y-m-d H:i:s");
    $sportid = $_SESSION['sportid'];
    $sportname = $_SESSION['sportname'];

    if ($mode == "insert") {
        $insert_player = $conn->prepare("INSERT INTO player_info(customer_id, firstname, lastname, sex, age, school, prev_school, grade, uniform_no, team_id, position, height, weight, isActive, date_of_birth, sport_id, date_added) VALUES (:customer_id, :firstname, :lastname, :gender, :age, :school, :prev_school, :grade, :uniform_no, :team_id, :position, :height, :weight, :isActive, :dob, :sportid, :created_date)");    
        $insert_player_arr = array(":customer_id"=>$cid, ":firstname"=>$firstname, ":lastname"=>$lastname, ":gender"=>$gender, ":age"=>$age, ":school"=>$school, ":prev_school"=>$prev_school, ":grade"=>$grade, ":uniform_no"=>$uniform_no, ":team_id"=>$teamid, ":position"=>$position, ":height"=>$height, ":weight"=>$weight, ":isActive"=>"1", ":dob"=>$dob, ":sportid"=>$sportid, ":created_date"=>$created_date);
        $insertres = $insert_player->execute($insert_player_arr);
        $palyer_id = $conn->lastInsertId();
        if ($insertres) {

            $select_player = $conn->prepare("SELECT * from player_info where id=:id");
            $select_player_arr = array(":id"=>$palyer_id);
            $select_player->execute($select_player_arr);
            $fetch_player = $select_player->fetch(PDO::FETCH_ASSOC);

            $player_info_arr[0]["status"] = "success";
            $player_info_arr[0]["mode"] = "insert";
            $player_info_arr[0]["id"] = $fetch_player["id"];
            $player_info_arr[0]["lastname"] = $fetch_player["lastname"];
            $player_info_arr[0]["firstname"] = $fetch_player["firstname"];
            $player_info_arr[0]["teamname"] = $hidden_team_name;
        } else {
            $player_info_arr[0]["status"] = "Fail";
        }
    } else {

        $playerid = $_REQUEST["playerid"];
        $update_player = $conn->prepare("UPDATE player_info SET firstname=:firstname, lastname=:lastname, sex=:gender, age=:age, school=:school, prev_school=:prev_school, grade=:grade, uniform_no=:uniform_no, team_id=:team_id, position=:position, height=:height, weight=:weight, isActive=:isActive, date_of_birth=:dob, sport_id=:sportid, date_added=:created_date  WHERE id=:playerid");    
        $update_player_arr = array(":firstname"=>$firstname, ":lastname"=>$lastname, ":gender"=>$gender, ":age"=>$age, ":school"=>$school, ":prev_school"=>$prev_school, ":grade"=>$grade, ":uniform_no"=>$uniform_no, ":team_id"=>$teamid, ":position"=>$position, ":height"=>$height, ":weight"=>$weight, ":isActive"=>"1", ":dob"=>$dob, ":sportid"=>$sportid, ":created_date"=>$created_date, ":playerid"=>$playerid);
        $updateres = $update_player->execute($update_player_arr);
        if ($updateres) {

            $select_player = $conn->prepare("SELECT * from player_info where id=:id");
            $select_player_arr = array(":id"=>$playerid);
            $select_player->execute($select_player_arr);
            $fetch_player = $select_player->fetch(PDO::FETCH_ASSOC);

            $player_info_arr[0]["status"] = "success";
            $player_info_arr[0]["mode"] = "update";
            $player_info_arr[0]["id"] = $fetch_player["id"];
            $player_info_arr[0]["lastname"] = $fetch_player["lastname"];
            $player_info_arr[0]["firstname"] = $fetch_player["firstname"];
            $player_info_arr[0]["teamname"] = $hidden_team_name;
        } else {
            $player_info_arr[0]["status"] = "Fail";
        }

    }
    echo json_encode($player_info_arr);
    
     
}


